import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'superdonate_django.settings'
os.environ['DJANGOTEST_SERVER_LOCATION'] = 'remote'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

sys.path.append('/home/tbak/p/superdonate/www')
sys.path.append('/home/tbak/p/superdonate/www/superdonate_django')