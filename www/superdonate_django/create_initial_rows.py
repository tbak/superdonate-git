#!/usr/bin/env python
import django
from django.core.management import setup_environ
import os
import re

# Options parsing needs to be at the top of this file so that os.environ can be set when settings is imported.
if __name__ == "__main__":
    
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-r", "--remote", action="store_const", const="remote", dest="server_location", help="Use the remote applicaton twitter settings")
    parser.add_option("-l", "--local", action="store_const", const="local", default="local", dest="server_location", help="Use the local callback twitter settings")    

    (options, args) = parser.parse_args()
    os.environ['DJANGOTEST_SERVER_LOCATION'] = options.server_location

try:
    import settings # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

setup_environ(settings)
from django.contrib.auth.models import User

from main.models import Charity, UserProfile, Team

def create_anon_user_profile(charity, plura_id_int ):
    acp0 = UserProfile()
    acp0.user = User.objects.get(username__exact='charity' + str(plura_id_int))
    acp0.charity = charity
    acp0.save()
    
def create_charity( name, url, desc, plura_id_int, is_enabled=True ):
    c0 = Charity()    
    c0.name = name
    c0.url = url
    c0.description = desc
    c0.is_enabled = is_enabled
    c0.app_id = plura_id_int
    c0.save()
    
    create_anon_user_profile(c0, plura_id_int)

if __name__ == "__main__":
    
    """
    for i in range(0,20):
        ac1 = User()
        ac1 = User.objects.create_user( "charity" + str(i), "foo@bar.com", "pc288lcd" )
        ac1.save()
    """
    
    
    
    create_charity( "charity: water", "http://www.charitywater.org", "charity: water is a nonprofit organization stimulating greater global awareness about extreme poverty, educating the public, and provoking compassionate and intelligent giving. 100% of public donations go directly toward water projects on the ground, while administrative costs are covered by a separate set of donors, grants, and our board.", 0 )
    create_charity( "CARE", "http://www.care.org", "CARE DEL", 1, is_enabled=False )
    create_charity( "Oaktree Foundation", "http://www.theoaktree.org", "The Oaktree Foundation is an aid and development organisation run entirely by volunteers under 26. At Oaktree we raise awareness in the community by running programs and events, we lobby the government and we fundraise for our overseas projects. We have partnered with developing communities in 6 countries to provide over 40,000 young people with increased educational opportunities. We believe that education is the most powerful force we have to change the world.", 2 )
    create_charity( "Nature Conservancy", "http://www.nature.org", "The Nature Conservancy is a global organization dedicated to protecting the lands and waters on which the diversity of life depends. We envision a world where forest, grasslands, deserts, rivers and oceans are healthy; where the connection between natural systems and the quality of human life is valued; and where the places that sustain all life endure for future generations. We are one of the world's largest private, non-profit, nongovernmental conservation organizations. Yet we operate at the local level, working in hundreds of communities on the ground to conserve natural diversity and sustain livelihoods.", 3 )
    create_charity( "Philippine Aid Society", "http://www.philippineaid.org", "Philippine Aid Society provides a variety of charitable services in the Philippines focused on eliminating poverty. Our four-fold plan of action includes feeding, caring for, educating, and employing the needy. Young men and women from poor households have the opportunity to pursue higher education and street children have a brighter future through our scholarship programs. Our livelihood programs are designed to take the poor and destitute and give them job skills and the means for self production so they can provide for themselves and for their families. Finally, we respond to natural disasters and immediate needs e.g. medical or nutritional.", 4 )
    create_charity( "Invisible Youth Network", "http://www.invisibleyouthnetwork.net", "Invisible Youth is an expansive online network that provides resources and support for an estimated 1.5 million homeless at risk youth. Our influence is felt every day in cities all across America. Our mission is carried out through both partner organizations and volunteers who take to the streets to find, stabilize and otherwise help homeless and at risk youth to improve their quality of life. Our focus reaches past the streets and lends itself to deterrence, research, resources, seminars, training and workshops that are provided to the communities and via the Internet. Despite our motto \"Advocating for America's Youth,\" our endeavors are focused on all homeless and at risk youth with no regard to their geographic location. Mahatma Gandhi said, \"You must be the change you wish to see in the world,\" and with that in mind, The Invisible Youth Network envisions better lives for children who cannot see it themselves..", 5 )          
    
    
    tbak = UserProfile()
    tbak.user = User.objects.get(username__exact='tbak')    
    tbak.plura_id = "user" + str(tbak.user.id)
    tbak.charity = Charity.objects.get(name__exact='Philippine Aid Society')
    tbak.save()
    
    test_team = Team()
    test_team.name = "Test Team"
    test_team.description = "Test team descritpionsfk lsk "
    test_team.url = "http://foo"
    test_team.owner = User.objects.get(username__exact='tbak')  
    test_team.save()    
    
    print "Done inserting initial rows! Now bugger off!"
