from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.views.decorators.cache import cache_page
cache_seconds = 60 * 60
from superdonate_django.main.views import charity_info

urlpatterns = patterns('',
 
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    
    # LOGIN/ETC
    (r'^login/$', 'django.contrib.auth.views.login', {'template_name':'main/login.html'}, 'login' ),
    (r'^logout/$', 'django.contrib.auth.views.logout_then_login', {'login_url':'/'}, 'logout' ),    
    (r'^register/$', 'superdonate_django.main.views.register', {}, 'register' ),   
    
    # CAN BE SEEN BY ANYONE
    (r'^$', 'superdonate_django.main.views.home', {}, 'home' ),
    (r'^about/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/about.html'}, 'about' ),    
    (r'^how_does_superdonate_work/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/how_does_superdonate_work.html'}, 'how_does_superdonate_work' ),
    (r'^faq/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/faq.html'}, 'faq' ),        
    (r'^privacy/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/privacy.html'}, 'privacy' ),
    (r'^promote/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/promote.html'}, 'promote' ),
    (r'^invite/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/invite.html'}, 'invite' ),    
    (r'^privacy/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/privacy.html'}, 'privacy' ),
    (r'^contact/$', 'superdonate_django.main.views.contact', {}, 'contact' ),
    (r'^contact_thankyou/$', 'superdonate_django.main.views.contact_thankyou' ),
    
    (r'^download/(?P<operating_system>[a-z]+)/$', 'superdonate_django.main.views.download', {}, 'download' ),

    # Inserted into index.html when the user clicks to see the movie
    (r'^ajax_movie/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/ajax_movie.html'}, 'ajax_movie' ),    
    
    (r'^facebook/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/facebook_app.html'}, 'facebook_app' ),

    # Oldercopies of the program refer to this page. In the future, .php should be removed
    (r'^survey_uninstall.php/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/survey_uninstall.html'}, 'survey_uninstall_old' ),    
    (r'^survey_uninstall/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/survey_uninstall.html'}, 'survey_uninstall' ),
    
    (r'^invite_email/$', 'superdonate_django.main.views.invite_email', {}, 'invite_email' ),
    (r'^invite_email_thankyou/$', 'superdonate_django.main.views.invite_email_thankyou', {}, 'invite_email_thankyou' ),
    
    (r'^watch_video/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/watch_video.html'}, 'watch_video' ),
    
    (r'^test/$', 'django.views.generic.simple.direct_to_template', {'template': 'main/test.html'}, 'test' ),    
    
    # Also shows stats? How can you switch between different charities, then?
    # I can't just cache the whole page, because the login/logout header will be different!
    # Any way to just cache the contents of the page (but not the header stuff???)    
    #(r'^charity/info/(?P<charity_id>\d+)$', cache_page(charity_info, cache_seconds), {}, 'charity_info' ),
        
    (r'^charity/info/(?P<charity_id>\d+)$', 'superdonate_django.main.views.charity_info', {}, 'charity_info' ),    
    (r'^charity/list/$', 'superdonate_django.main.views.charity_list', {}, 'charity_list'),
    
    # The charity app_id is not consistent with the charity's primary key ID
    # So the app needs to go to this page to view the correct charity
    (r'^charity/info_app_id/(?P<charity_app_id>\d+)$', 'superdonate_django.main.views.charity_info_app_id', {}, 'charity_info_app_id' ),
           
    # Team info may as well also display stats???
    (r'^team/info/(?P<team_id>\d+)$', 'superdonate_django.main.views.team_info', {}, 'team_info'),    
    (r'^team/list/$', 'superdonate_django.main.views.team_list', {}, 'team_list'),
           
    # REQUIRES AUTHENTICATION
    # Show personal stats, show selected team stats(or just a link), show charity donating to 
    (r'^user/home/$', 'superdonate_django.main.views.user_home', {}, 'user_home' ),    
    (r'^user/join_team/(?P<team_id>\d+)$', 'superdonate_django.main.views.user_join_team', {}, 'user_join_team'),
    
    # This view is also used to create new teams?
    (r'^user/create_team/$', 'superdonate_django.main.views.user_edit_team', {}, 'user_create_team' ),
    (r'^user/edit_team/(?P<team_id>\d+)$', 'superdonate_django.main.views.user_edit_team', {}, 'user_edit_team'),
    (r'^user/edit_profile/$', 'superdonate_django.main.views.user_edit_profile', {}, 'user_edit_profile'),
    # Create team
    # View team
    # Select team (maybe do this in the app instead?)
    
       

    # In: username + encrypted password
    # Returns {result, session key, team name?}    
    (r'^app/login/$', 'superdonate_django.main.views.app_login'),
    (r'^app/ping/$', 'superdonate_django.main.views.app_ping'),
    
    (r'^app/prepare_register/$', 'superdonate_django.main.views.app_prepare_register'),
    (r'^app/register/$', 'superdonate_django.main.views.app_register'),
        
    # In: username + session_key + charity
    # Returns {result}
    #(r'^app/set_charity/$', 'superdonate_django.main.views.app_set_charity'),
    
    # In: username + session_key + mins
    # Returns {result}
    # TB TODO - This wouldn't work if program is running on more than 1 computer.
    #(r'^app/set_total_time/$', 'superdonate_django.main.views.app_set_charity'),
    
    
    (r'^password_reset/$', 'django.contrib.auth.views.password_reset', {'template_name':'main/password_reset_form_new.html', 'email_template_name':'main/password_reset_email_new.html'}, 'password_reset'),
    (r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', {'template_name':'main/password_reset_done_new.html'}),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'template_name':'main/password_reset_confirm_new.html'}, 'password_reset_confirm'),
    (r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', {'template_name':'main/password_reset_complete_new.html'}),
    
    (r'^captcha/', include('captcha.urls')),
    
    # Google charts visualization api datasource (used by user info page)
    (r'^gviz_datasource/$', 'superdonate_django.main.views.gviz_datasource', {}, 'gviz_datasource' ),
            
)
