import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'superdonate_django.settings'
os.environ['DJANGOTEST_SERVER_LOCATION'] = 'local'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

sys.path.append('C:/p2/superdonate/www')
sys.path.append('C:/p2/superdonate/www/superdonate_django')