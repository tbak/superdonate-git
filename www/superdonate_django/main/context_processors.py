def user_os(request):
    """ Return the OS that the user is using.
    0 = win, 1=linux, 2=mac """
     
    os = 0
    
    tests = ( ('win',0),
              ('mac',2),
              ('openbsd',1),
              ('sunos',1),
              ('linux',1),
              ('x11',1),
              ('qnx',1) )
    
    try:
        agent_lower = request.META['HTTP_USER_AGENT'].lower()
    
    except KeyError:
        # Somehow the user agent is missing for some people?
        return {'user_os':0}
    
    for a,b in tests:
        if a in agent_lower:
            os = b
            break
    
    return {'user_os':os} 
    
    