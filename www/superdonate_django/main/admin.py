# Copyright 2009, SuperDonate, Inc.

from django.contrib import admin 
from superdonate_django.main.models import UserProfile, Charity, Team, DonateDay

class UserProfileAdmin(admin.ModelAdmin):    
    pass

class CharityAdmin(admin.ModelAdmin):    
    pass

class TeamAdmin(admin.ModelAdmin):    
    pass

class DonateDayAdmin(admin.ModelAdmin):    
    pass

admin.site.register(Team, TeamAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Charity, CharityAdmin)
admin.site.register(DonateDay, DonateDayAdmin)