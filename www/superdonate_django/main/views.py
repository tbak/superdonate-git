from superdonate_django.main.models import DonateDay, Charity, Team, UserProfile
from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template.context import RequestContext
from django.contrib.auth.models import User, Permission
from django.contrib.auth import authenticate, login, logout
from django.db.models.query import QuerySet
import logging
from django.conf import settings
from django.utils import simplejson
from django.utils import html
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from superdonate_django.captcha.fields import CaptchaField
from django.core.mail import send_mail
from urllib2 import HTTPError
import re
import datetime
import time
import superdonate_django.main.kungfutimefield as kungfutimefield
from django.contrib.sites.models import Site
from django.contrib.auth.decorators import login_required
import string
import random
from django.db.models import Sum, Count
from django.utils.html import strip_tags
from django.views.decorators.vary import vary_on_headers

from pygooglechart import Chart
from pygooglechart import SimpleLineChart
from pygooglechart import Axis
from math import ceil

class CreateUserForm(forms.Form):    
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField(required=True)
    captcha = CaptchaField()
    
    def clean(self):
        super(forms.Form, self).clean()
    
        errors = False
        
        if 'password' in self.cleaned_data and 'password2' in self.cleaned_data and self.cleaned_data['password'] != self.cleaned_data['password2']:              
            self._errors['password'] = self._errors.get( 'password', forms.util.ErrorList() )
            self._errors['password'].append( "Passwords do not match" )            
            errors = True
            
        if( 'username' in self.cleaned_data and User.objects.filter(username=self.cleaned_data['username']).count() != 0 ):
            self._errors['username'] = self._errors.get( 'username', forms.util.ErrorList() )
            self._errors['username'].append( 'Username is already taken' )                        
            errors = True
            
        if ('username' in self.cleaned_data) and (' ' in self.cleaned_data['username']):
            self._errors['username'] = self._errors.get( 'username', forms.util.ErrorList() )
            self._errors['username'].append( 'Spaces are not allowed' )
            errors = True                     
            
        if errors:            
            raise forms.ValidationError([])
        
        return self.cleaned_data
    

class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField( widget=forms.Textarea( attrs={'cols':80, 'rows':14}) )
    captcha = CaptchaField()

class InviteEmailForm(forms.Form):
    name = forms.CharField()
    target0 = forms.EmailField( required=True )
    target1 = forms.EmailField( required=False )
    target2 = forms.EmailField( required=False )
    target3 = forms.EmailField( required=False )
    target4 = forms.EmailField( required=False )
    message = forms.CharField( widget=forms.Textarea( attrs={'cols':80, 'rows':14}) )
    captcha = CaptchaField()


# Right now you can just change your email address...
class EditUserForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

class TeamForm(forms.ModelForm):
        
    class Meta:
        model = Team
        exclude = ['owner', 'create_date', 'is_enabled' ]
    

class BadUserException(Exception):
    """Used to output a generic message box in the HttpResponse if something goes wrong in code -- generally caused by the user doing something bad"""    
    def __init__(self,message):
        # Does this get rid of the warning???
        Exception.__init__(self)              
        
        self.message = message   

class JsonException(Exception):
    """Used to output a JSON error message in the HttpResponse if something goes wrong in code"""    
    def __init__(self,message):
        self.message = message   


def generic_message(request, title, message):
    return render_to_response('main/generic_message.html', {"title":title, "message":message}, context_instance=RequestContext(request) )

# TB TODO - Nothing actually uses this? Should it?
# Maybe it'd be better if I was alerted to errors by email like I do now? (internal server error)
# TB TODO - If DEBUG is False, then catch regular exceptions too and display generic_message + email to me.
class CatchUserException(object):
    """
    Decorator to catches the JsonException and output a json styled error message
    instead of regular html text. 
    """
    def __init__(self,view_func):
        self.view_func = view_func
        
    def __call__(self, request, *args, **kwargs):
        try:
            return self.view_func(request, *args, **kwargs)

        except HTTPError, e:
            return generic_message(request, "HTTP Error", 'The url "' + e.url + '" resulted in an unexpected HTTP error: ' + e.msg )
        
        except BadUserException, e:
            # TB TODO - Return something more friendly?
            return generic_message(request, "Error", e.message)           
            
        except JsonException, e:
            return generic_message(request, "System Error", "Should not be seeing a Json error from CatchUserException decorator...")
         
# This returns the error information as Json data
# The above decorator returns the error information as a general window.
# We can only use 1, depending on how the returned data is expected. 
class CatchUserExceptionJson(object):         
    def __init__(self,view_func):
        self.view_func = view_func
        
    def __call__(self, request, *args, **kwargs):
        try:
            return self.view_func(request, *args, **kwargs)

        except HTTPError, e:
            out = {'result':'error', 'message':'The url "' + e.url + '" resulted in an unexpected HTTP error: ' + e.msg }
            return HttpResponse( simplejson.dumps(out) )
        
        except BadUserException, e:
            out = {'result':'error', 'message':e.message }
            return HttpResponse( simplejson.dumps(out) )           
            
        except JsonException, e:
            out = {'result':'error', 'message':e.message }
            return HttpResponse( simplejson.dumps(out) )
    
         
def get_object_or_baduser_error(klass, *args, **kwargs):
    try:        
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        # Propagate a baduser error
        raise BadUserException('Could not find database object:' + str(klass) )   

def get_object_or_json_error(klass, *args, **kwargs):
    try:        
        return klass.objects.get(*args, **kwargs)
    except klass.DoesNotExist:
        # Propagate a json error
        raise JsonException('Could not find database object:' + str(klass) )   

def get_now_utc_tuple():
    """ Returns a tuple with a bunch of date/time components """
    now = datetime.datetime.now()
    secs = time.mktime(now.timetuple())
    
    # Make the datetime object with the first 6 tuple items: Y M D H M S.
    return time.gmtime(secs)

def get_now_utc_datetime():
    """ Return the current datetime for UTC """
    
    # Make the datetime object with the first 6 tuple items: Y M D H M S.
    return datetime.datetime( *get_now_utc_tuple()[0:6])

def get_now_utc_date():
    # Form the date using the first 3 tuble items: Y M D
    return datetime.date( *get_now_utc_tuple()[0:3]) 

# Vary cache on user-agent since the Download link here depends on the user's OS. All other pages should be safe (they don't care)
@vary_on_headers('User-Agent')
def home(request):
    # TB TODO - Get this (java script disabled tab handling...)
    active_panel = 1
    return render_to_response('main/index.html', {'active_panel':active_panel}, context_instance=RequestContext(request) )  

def download(request, operating_system):
    
    extra_message = None    
    if operating_system == "osx":
        download_os = "Mac OS X"
        # download_file = "dl/superdonate_mac_install-1.1.0.zip"
	download_file = "dl/SuperDonate-1.1.1.dmg"
        extra_message = "<br/><br/><b>Note: A 64-bit Intel Mac with OSX 10.5.2 (Leopard) or higher is required to run SuperDonate on a Mac.</b>"
    elif operating_system == "linux":
        download_os = "Linux"
        download_file = "dl/superdonate_linux-1.1.1.tar.gz"
    else:
        download_os = "Windows"
        download_file = "dl/superdonate_install-1.1.1.exe"
    
    return render_to_response('main/download.html', {'download_os':download_os, 'download_file':download_file, 'extra_message':extra_message}, context_instance=RequestContext(request) )

def contact(request):
    
    if request.method == 'POST': # If the form has been submitted...    
        
        form = ContactForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass                     
                      
            message = 'Message from: ' + form.cleaned_data['name']
            message = message + '\n'
            message = form.cleaned_data['message'] 

            logging.debug(message)

            send_mail('SuperDonate Message', message, form.cleaned_data['email'], ['tom@superdonate.org'], fail_silently=True)

            return HttpResponseRedirect( reverse('superdonate_django.main.views.contact_thankyou' ) )
         
    else:
        form = ContactForm()

    return render_to_response('main/contact.html', {"form":form}, context_instance=RequestContext(request) )    

def contact_thankyou(request):
    return generic_message(request, "Thank You", "Your message has been sent. We will get back to you as soon as possible.")


def invite_email(request):
    if request.method == 'POST': # If the form has been submitted...    
        
        form = InviteEmailForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass                     
                       
            message = form.cleaned_data['message']
            message += "\n\nhttp://www.superdonate.org"
            
            targets = []
            for a in range(5):
                dude = form.cleaned_data['target' + str(a)]
                if len(dude) > 0:
                    targets.append(dude)
                            
            if len(targets) > 0:
            
                subject = form.cleaned_data['name'] + ' has sent you a SuperDonate invitation'
            
                send_mail(subject, message, 'noreply@superdonate.org', targets, fail_silently=True)
                return HttpResponseRedirect( reverse('superdonate_django.main.views.invite_email_thankyou' ) )
         
    else:
        form = InviteEmailForm( initial = {'message':'Hello,\n\nI am using the SuperDonate computer program to help out my favorite charities. Do you want to try it out?'} )

    return render_to_response('main/invite_email.html', {"form":form}, context_instance=RequestContext(request) )    

    
def invite_email_thankyou(request):
    return generic_message(request, "Thank You", "Thank you! Your message has been sent.")    

def create_new_user(request, username, email, password):
    """ Used by the app register and www register. Everything has been verified by now. """
    
    # Create the user!
    user = User.objects.create_user( username, email, password )
    user.save()

    # Automatically log in the user
    # Must call authenticate first
    user = authenticate(username=username, password=password)    
    
    # Attach this user to a profile
    prof = UserProfile()
    prof.user = user        
    prof.charity = Charity.objects.get(app_id__exact=3)
    prof.save()

    #
    # Send email to the address with user info!
    #
    message = 'Thank you for signing up with SuperDonate.\n\n'
    message += 'Your username: ' + username + '\n\n'
    message += 'The SuperDonate application is your key to automatically earning money for your favorite charity. If you haven\'t done so already, be sure to download and install the SuperDonate application. You can download it by visiting http://www.superdonate.org and clicking on the download button for your operating system. Once the application is running on your computer, you will be automatically earning money for your favorite charity.\n\n'
    message += 'If you like SuperDonate, please be sure to tell your friends. You can also help out SuperDonate by visiting http://www.superdonate.org/promote to help spread the word.\n\n'
    message += 'If you have any questions or comments, please let us know. You can reach us by visiting http://www.superdonate.org/contact\n\n'
    message += 'Regards,\nThe SuperDonate Team\nhttp://www.superdonate.org'
    send_mail('Welcome to SuperDonate!', message, 'noreply@superdonate.org', [email], fail_silently=True)
    
    return user
        

def register(request):        
        
    if request.method == 'POST': # If the form has been submitted...
        
        form = CreateUserForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass                     
                      
            user = create_new_user(request, form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'] )
            login(request,user)
            request.user.message_set.create(message='Your new account has been created!')
                        
            return HttpResponseRedirect( reverse('user_home' ) )
         
    else:
        form = CreateUserForm( initial = {'name':'foo!'} )

    return render_to_response('main/register.html', {"form":form}, context_instance=RequestContext(request) )

@login_required()
def user_home(request):    
    profile = request.user.get_profile()        
   
    # This code is similar to get_grouped_stat_data, but personalized for an individual user
    #first_of_month = get_first_of_month()
    
    # Use select_related to also get the charity information for the charity that the user was donating to that day
    #daily_data = DonateDay.objects.filter( user__exact = request.user ).filter(donation_date__gte = first_of_month).select_related('charity')
    
    # Get the last 30 days instead of the first of the month.
    # This way the info won't seem so barren in the first of the month
    now = get_now_utc_date()
    starting_day = now + datetime.timedelta( days=-30 )

    #daily_data = DonateDay.objects.filter( user__exact = request.user ).filter(donation_date__gte = starting_day).select_related('charity')
    daily_data = DonateDay.objects.filter( user__exact = request.user ).filter(donation_date__gte = starting_day).select_related('charity').extra( select = {'total_work_units': '(work_units + fake_work_units)'}, )
       
  
    # Sum of days for a month        
    #monthly_data = DonateDay.objects.filter( user__exact = request.user ).extra(select={'year':"extract(year from donation_date)", 'month':"extract(month from donation_date)" }).values('year','month').annotate(work_units=Sum('work_units')).order_by()
    monthly_data = DonateDay.objects.filter( user__exact = request.user ).extra(select={'year':"extract(year from donation_date)", 'month':"extract(month from donation_date)", 'total_work_units': 'work_units + fake_work_units' }).values('year','month').annotate(work_units=Sum('work_units'), fake_work_units=Sum('fake_work_units')).order_by()        
    
    for m in monthly_data:
        #logging.debug(m.keys())
        m['donation_date'] = datetime.date( m['year'], m['month'], 1)
        
        # This is retarded. I wish I could figure out how to get the sum of the two columns in the SQL call
        m['total_work_units'] = m['work_units'] + m['fake_work_units']

    # Google static charts don't work with SSL :(
    # So use the visualization API isntead which seems cooler anyways...
    #daily_chart_url = get_chart_img_url(daily_data, 600, 300, False, 15, request.user.username )
    #monthly_chart_url = get_chart_img_url(monthly_data, 600, 300, True, 15, request.user.username )
    
    
    # TB TEST - Can I use google charts visualization API?
    #all_data = DonateDay.objects.filter( user__exact = request.user ).select_related('charity').extra( select = {'total_work_units': '(work_units + fake_work_units)'}, )
    
    #return render_to_response('main/user_home.html', {"profile":profile, "monthly_chart_url":monthly_chart_url, "monthly_data":monthly_data, "daily_chart_url":daily_chart_url, "daily_data":daily_data}, context_instance=RequestContext(request) )
    return render_to_response('main/user_home.html', {"profile":profile, "monthly_data":monthly_data, "daily_data":daily_data}, context_instance=RequestContext(request) )

@login_required
def gviz_datasource(request):
    import gviz_api
    
    user_id = request.GET.get('tq', 0)
    #columns = ['date', 'num_workunits']
    
    description= {}
    description['date'] = ('date', 'Date')
    description['total_work_units'] = ('number', 'Work Units Donated')
    
    data_table = gviz_api.DataTable( description )
    
    all_days = DonateDay.objects.filter( user__exact = user_id ).select_related('charity').extra( select = {'total_work_units': '(work_units + fake_work_units)'}, )
    
    for d in all_days:
        data_table.AppendData([ {'date':d.donation_date, 'total_work_units':d.total_work_units}])
        
    return HttpResponse(data_table.ToResponse(tqx=request.GET.get('tqx','')))   

@login_required()
def user_join_team(request, team_id):
    # Make sure the team exists
    team = get_object_or_404(Team, pk=team_id)
    
    profile = request.user.get_profile()
    profile.team = team
    profile.save()
    
    return HttpResponseRedirect( reverse('team_info', args=[team.id]) )
    
    

@login_required()
def user_edit_team(request, team_id = None):
        
    team = None
    if team_id is not None:        
        team = get_object_or_404(Team, pk=team_id)
        
        # Ensure that user is allowed to see this team        
        if request.user != team.owner:
            raise BadUserException('User is not the owner of this script' )
    
    if request.method == 'POST':
        
        # Save the created or edited team using posted data        
        form = TeamForm(request.POST, request.FILES, instance=team)
        if form.is_valid():
                                           
            team = form.save(commit=False)            
            team.owner = request.user                         
            team.save()
            
            # Now, save the many-to-many data for the form.
            # This is required if form.save(commit=False) is called
            # I don't think I need this for a simple team... (code snagged from djangotest)
            # form.save_m2m()
                        
            return HttpResponseRedirect( reverse('team_info', args=[team.id]) )
    
    else:
        # Create/edit a team with database values
        form = TeamForm(instance=team)

    return render_to_response('main/user_edit_team.html', {'form':form}, context_instance=RequestContext(request))   
    

@login_required()
def user_edit_profile(request):
        
    if request.method == 'POST':
        
        # Save the created or edited team using posted data        
        form = EditUserForm(request.POST)
        if form.is_valid():
            
            # Check that password is correct
            try_user = authenticate(username=request.user.username, password=form.cleaned_data['password'])
            if try_user is not None:                                           
                request.user.email = form.cleaned_data['email']
                request.user.save()                                           
                return HttpResponseRedirect( reverse('user_home') )
            
            form.errors['password'] = "Password is incorrect"
    
    else:
        form = EditUserForm( initial = {'email':request.user.email} )

    return render_to_response('main/user_edit_profile.html', {'form':form}, context_instance=RequestContext(request))   
    


def team_list(request):
    
    # distinct=true so that the same user isn't counted as a member for each donateday (he'll only count as 1 member now)
    team_list = Team.objects.filter(is_enabled__exact=True).annotate( work_units=Sum( 'donateday__work_units'), num_members=Count('userprofile', distinct=True) ).order_by('-work_units')
    logging.debug(team_list)    
    return render_to_response('main/team_list.html', {'team_list':team_list}, context_instance=RequestContext(request))

def team_info(request, team_id):
    # To call the annotate function I need to get a queryset with one team, and then get the team.
    # team = get_object_or_baduser_error(Team, id__exact=team_id)
    team = Team.objects.filter(id__exact=team_id).annotate( num_members=Count('userprofile') )[0]
  
    (daily_data, daily_chart_url, monthly_data, monthly_chart_url) = get_grouped_stat_data( team.name, team__exact = team )    

    # The user's team (used for rendering)
    user_team = None
    if request.user.is_authenticated():
        user_team = request.user.get_profile().team
    
    return render_to_response('main/team_info.html', {"user_team":user_team, "team":team, "monthly_chart_url":monthly_chart_url, "monthly_data":monthly_data, "daily_chart_url":daily_chart_url, "daily_data":daily_data}, context_instance=RequestContext(request) )

def get_first_of_month():
    """Get the beginning of the month
    Minus a few days so that when viewing the chart for the first of the month it's not really barrent (it'll show the last month for a few days)
    """
    now = get_now_utc_date()
    now = now + datetime.timedelta( days=-2 )
    first_of_month = datetime.date(now.year, now.month, 1)
    return first_of_month

# Used by the team and charity views to sum all work units and generate relevant charts
# The user stat view can't use this function because it needs to get individual DonateDay records (to extract the charity information) 
def get_grouped_stat_data( pretitle, **kwargs ):
    """Generalized to get stat data for a team or charity
    Returns (daily_data, daily_chart, monthly_data, monthly_chart
    keyword Filters are passed in as kwags (either the charity or team id) """
    #first_of_month = get_first_of_month()
    
    # Get the last 30 days instead of the first of the month.
    # This way the info won't seem so barren in the first of the month
    now = get_now_utc_date()
    starting_day = now + datetime.timedelta( days=-30 )
    ending_day = datetime.date(now.year, now.month, now.day)
        
    daily_data = DonateDay.objects.filter(**kwargs).filter(donation_date__gte = starting_day).filter(donation_date__lt = ending_day).extra(select={'day':"extract(day from donation_date)", 'month':"extract(month from donation_date)", 'year':"extract(year from donation_date)"}).values('day', 'month', 'year').annotate(work_units=Sum('work_units'), fake_work_units=Sum('fake_work_units')).order_by()

    for d in daily_data:
        d['donation_date'] = datetime.date( d['year'], d['month'], d['day'] )
        d['total_work_units'] = d['work_units'] + d['fake_work_units']
  
    # Sum of days for a month        
    monthly_data = DonateDay.objects.filter(**kwargs).extra(select={'year':"extract(year from donation_date)", 'month':"extract(month from donation_date)" }).values('year','month').annotate(work_units=Sum('work_units'), fake_work_units=Sum('fake_work_units')).order_by()
    
    for m in monthly_data:
        #logging.debug(m.keys())
        m['donation_date'] = datetime.date( m['year'], m['month'], 1)
        m['total_work_units'] = m['work_units'] + m['fake_work_units']

    daily_chart_url = get_chart_img_url(daily_data, 600, 300, False, 15, pretitle )
    monthly_chart_url = get_chart_img_url(monthly_data, 600, 300, True, 15, pretitle )

    return (daily_data, daily_chart_url, monthly_data, monthly_chart_url)

def charity_info(request, charity_id):
    
    charity = get_object_or_baduser_error(Charity, id__exact=charity_id)
  
    (daily_data, daily_chart_url, monthly_data, monthly_chart_url) = get_grouped_stat_data( charity.name, charity__exact = charity )
    
    
    # Do I also want the charity list here (selectable with the select control on the bottom?)
    charity_list = Charity.objects.filter(is_enabled__exact=True).order_by('name')       
    
    return render_to_response('main/charity_info.html', {"charity_list":charity_list, "charity":charity, "monthly_chart_url":monthly_chart_url, "monthly_data":monthly_data, "daily_chart_url":daily_chart_url, "daily_data":daily_data}, context_instance=RequestContext(request) )

def charity_info_app_id(request, charity_app_id):
    charity = get_object_or_baduser_error(Charity, app_id__exact=charity_app_id)
    return charity_info(request, charity.id)

def charity_list(request):
    
    # TB TODO - Order them by something?
    charity_list = Charity.objects.filter(is_enabled__exact=True).order_by('name')        
    return render_to_response('main/charity_list.html', {'charity_list':charity_list}, context_instance=RequestContext(request))


def get_chart_img_url( queryset, size_x, size_y, is_monthly, max_labels, pretitle ):          

    if queryset.count() < 2:
        return None

    if is_monthly:
        title = pretitle + ": Donations All Months (Work Units)"
    else:
        title = pretitle + ": Donations last 30 days (Work Units)"
    # TB - Try to fix issue with pygooglechart set_title screwing up with KeyError: u'\xf1'
    title = title.encode('utf-8')

    month_names = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')    
    
    #agg = Payment.objects.filter(operator__exact = operator).aggregate( Sum('num_days_added') )
    #num_days = agg['num_days_added__sum']
    
    charity = Charity.objects.get(id__exact=1)
    #months = DonateDay.objects.filter(charity__exact = charity)
    
    max_y = 100
    
    data = []

    # For daily, this says the day number. For monthly, the month
    small_bottom_labels = []    
    
    # For daily, this says the month (when it changes). For monthly, the year (when it changes)
    big_bottom_labels = []
    last_big_label = None   
           
    stride = int(ceil( queryset.count() / float(max_labels) ))
    index = 0    
    
    for a in queryset:

        # Retarded! If the values() method is used on a query, a dictionary is returned, which must access the values
        # differently. So it's either a.work_units or a['work_units']...
        # total_work_units contains the sum of work_units and fake_work_units
        try:
            #val = a.work_units
            val = a.total_work_units            
        except:            
            #val = a['work_units']
            val = a['total_work_units']
        data.append(val)
        
        if val > max_y:
            max_y = val
            
        try:
            label = a.donation_date
        except:
            label = a['donation_date']
                
        """            
        if is_monthly:
            all_bottom_labels.append( month_names[label.month-1] )
        else:
            all_bottom_labels.append( str(label.day) )
        """
        
        # Only output a bottom label for some items
        if is_monthly:
            possible_label = month_names[label.month-1]
            possible_big_label = str(label.year)
        else:
            possible_label = str(label.day)
            possible_big_label = month_names[label.month-1]
                
        if (index % stride is 0) or (index is 0) or (index is queryset.count()-1):
            small_bottom_labels.append( possible_label )
        else:
            small_bottom_labels.append('')
            
        if possible_big_label != last_big_label:
            big_bottom_labels.append( possible_big_label )
            last_big_label = possible_big_label
        else:
            big_bottom_labels.append('')
                
        index+=1
        



    # Filter out some of the bottom labels so that max_labels are used
    # bottom_labels = all_bottom_labels
    """
    bottom_labels = []
    
    stride = int(ceil( len(all_bottom_labels) / float(max_labels) ))
    
    for i, item in enumerate(all_bottom_labels):
        
        if (i % stride is 0) or (i is 0) or (i is len(all_bottom_labels)-1) :
            bottom_labels.append(item)
        else:
            bottom_labels.append('')
    """
             
    
    #bottom_labels = [ a and (a%2==1) or ' ' for a in all_bottom_labels ]
    
    """
    stride = int(ceil( len(all_bottom_labels) / float(max_labels) ))
    bottom_labels = all_bottom_labels[::stride]
    # Always include the last label (so add it back if it was chopped off)
    if len(all_bottom_labels) % stride is 0:
        bottom_labels.append(all_bottom_labels[-1])

    logging.debug(len(all_bottom_labels))
    logging.debug(bottom_labels)
    """

    max_y = max_y + 100 - max_y%100

    chart = SimpleLineChart(size_x, size_y, y_range=[0, max_y])
    chart.set_title(title) 
    #chart.set_legend( ["username"] )
    chart.add_data(data)
    
    # Set the line colour to blue
    chart.set_colours(['0000FF'])
    
    # Set the vertical stripes
    #chart.fill_linear_stripes(Chart.CHART, 0, 'eeeeee', 0.2, 'FFFFFF', 0.2)
    
    # Set the horizontal dotted lines

    chart.set_grid(0, 25, 5, 5)    
    
    # The Y axis labels contains 0 to 100 skipping every 25, but remove the
    # first number because it's obvious and gets in the way of the first X
    # label.
    #left_axis = range(0, max_y + 1, 25)
    #left_axis[0] = ''
    left_axis = ['', max_y/2, max_y]
    chart.set_axis_labels(Axis.LEFT, left_axis)
    
    # X axis labels
    # chart.set_axis_labels(Axis.BOTTOM, ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'] )
    chart.set_axis_labels(Axis.BOTTOM, small_bottom_labels )    
    chart.set_axis_labels(Axis.BOTTOM, big_bottom_labels )
    
    #chart.download('line-stripes.png')
    return chart.get_url()    

  

def get_new_captcha_id():
    from superdonate_django.captcha.fields import CaptchaTextInput
    
    # Generate the captcha
    # The render function returns <img> and <hidden> tags.
    # We just want to get the ID and return that to the app, which can then manually request that particular image.
    foo = CaptchaTextInput()
    name = 'fooname'
    value = 'foovalue'
    img_string = foo.render(name,value)
    logging.debug(img_string)
    re_extract_id = re.compile(r'value="(.+?)"')
    id = re_extract_id.search(img_string).group(1)
    return id
    

def app_prepare_register(request):
    """Get the captcha ID that the form should use to request the captcha image from the server"""
        
    return HttpResponse(get_new_captcha_id())        
     
def app_register(request):
    
    form = CreateUserForm(request.POST)
    if form.is_valid():
                
        user = create_new_user(request, form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'] )
        return HttpResponse("1")
    
    response = []
    response.append("2")
    response.append( 'username' in form.errors and str( form.errors['username'].as_text() ) or "" )
    response.append( 'email' in form.errors and str( form.errors['email'].as_text() ) or "" )            
    response.append( 'password' in form.errors and str( form.errors['password'].as_text() ) or "" )
    response.append( 'password2' in form.errors and str( form.errors['password2'].as_text() ) or "" )
    response.append( 'captcha' in form.errors and str( form.errors['captcha'].as_text() ) or "" )
    
    # Also return a new captcha ID. The old one will have been invalidated if it was correctly entered,
    # but some other field was hosed. So we need to redo it.
    response.append( get_new_captcha_id() )
    
    #logging.debug(response)
    #logging.debug( dir(form.errors['username']) )
    #logging.debug( form.errors['username'].as_text() )
    response = "\n".join(response)
    
    #return HttpResponse( str(form.errors) )                             
    return HttpResponse( response )
    #return HttpResponse( str(form.errors['username']) )

def app_ping(request):
    # Tell the server that we're still running the program
    # This will reward the user with a few "fake" work units just in case real work units from
    # Plura are not being awarded. This way the user will never see just 0 WU.
    
    if request.method != 'POST':
        return HttpResponse("ERR No post")
    
    # Get the post variables. If one of them is not set, exit gracefully
    try:
        username = request.POST['username'] 
        session_key = request.POST['session_key']        
    except KeyError:
        return HttpResponse("ERR Bad post vars")





    # TB TEMP!!!!! DISABLE SQL PING STUFF!!! Just return and ignore!
    # I think it's trashing the DB
    # Instead I should use redis or something for ping accumulation.
    return HttpResponse("1")






    try:
        user = User.objects.get(username = username)
        
    except User.DoesNotExist:
        return HttpResponse("ERR User does not exist")        

    # TB TODO - Verify that the session key we gave when the dude logged in is what we are
    # getting back.
    # Separate session table???

    # Ensure that it's been at least 30 mins
    profile = user.get_profile()    
    
    now = get_now_utc_datetime()
    earlier = now + datetime.timedelta( minutes=-30 )    
    if profile.last_app_ping_date is not None and profile.last_app_ping_date > earlier:
        return HttpResponse("2") # ping too often -- don't count it
       
    
        
    profile.last_app_ping_date = get_now_utc_datetime()
    profile.save()
    
    # Get the donate_day entry for today
    # If one doesn't exist, create a new one
    # We can safely set the team and charity values as well since we only ever edit the current day
    # The admin tool must be careful not to set the team/charity when updating since it goes
    # multiple days in the past.
    try:
        donate_day = DonateDay.objects.get( user__exact = user, donation_date__exact = now)
        
    except DonateDay.DoesNotExist:
        donate_day = DonateDay()
        donate_day.user = user
        donate_day.donation_date = now
        donate_day.work_units = 0
        donate_day.fake_work_units = 0        
        
    donate_day.team = profile.team
    donate_day.charity = profile.charity
    
    # TB TODO - How much to bump this up?    
    donate_day.fake_work_units += 1
        
    donate_day.save()

    return HttpResponse("1")

def app_login(request):

    if request.method != 'POST':
        return HttpResponse("ERR not post")
        
    # Get the post variables. If one of them is not set, exit gracefully
    try:
        password_enc = request.POST['password_enc'] 
        charity = request.POST['charity']
        username = request.POST['username']
    except KeyError:
        return HttpResponse("ERR missing field")
                
    
    real_password = []
    key = 42
    for letter in password_enc:
        real_password.append( chr( ord(letter) ^ key ) )
        key = key+1
    real_password = "".join(real_password)

    response = []

    user = authenticate(username=username, password=real_password)
    if user is not None:

        # TB TODO - Set the user's desired charity value in the DB
        profile = user.get_profile()            
        profile.charity = Charity.objects.get(app_id__exact = charity)
        profile.last_app_login_date = get_now_utc_datetime()
        profile.save()

        symbols = string.letters + string.punctuation
        session_key = []
        for i in range(32):
            session_key.append( symbols[random.randrange(0,len(symbols))] )
            
        session_key = "".join(session_key)    
       
        
        response.append("1")
        if profile.team is not None:                        
            response.append(profile.team.name)
            response.append( str(profile.team.id) )
        else:
            response.append("***")
            response.append("***")
        response.append(session_key)            
                    
        
    else:
        response.append("2")
        
    response_str = "\n".join(response)
    return HttpResponse( response_str )

