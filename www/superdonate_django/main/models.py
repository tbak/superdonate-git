from django.db import models
from django.contrib.auth.models import User
from stdimage.fields import StdImageField

# I snagged this from http://www.smipple.net/snippet/IanLewis/Django%20Big%20Integer%20Fields
class BigIntegerField(models.IntegerField):
    empty_strings_allowed=False
  
    def get_internal_type(self):
        return "BigIntegerField"
  
    def db_type(self):
        return 'bigint' # Note this won't work with Oracle.    

class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


# TB TODO - Better name?
class Team(models.Model):

    name = models.CharField( max_length=60 )    
    logo = StdImageField( upload_to='upload/team', blank=True, default='', size=(150, 50), thumbnail_size=(75,25), verbose_name='Logo' )
    url = models.CharField( max_length=60, blank=True, default='' )  
    create_date = models.DateField('date created', auto_now_add=True)
    description = models.TextField()
    owner = models.ForeignKey(User)
    is_enabled = models.BooleanField(default=True);
    
    class Meta:        
        ordering = ('name',)
    
    def __unicode__(self):
        return self.name



# Screw this for now. We'll pass a random session key around but it won't actually be verified
# I can always add this later 
"""
class AppSession(models.Model):
    # When an app logs in, a session is created.
    # This is passed back to the app.
    # Other calls require this user + session to work.
    user = models.ForeignKey(User, unique=True)
    session_key = models.CharField( max_length=64 )
    create_date = models.DateField('date created', auto_now_add=True)
"""

class Charity(models.Model):

    # TB TODO - Do I need this? All that's important is DonateDay, and that just
    # refers directly to the charity...
    # But the application needs to refer to the charity by plura id? Can't they just be the same?
    # Every user will have a plura ID string associated with it
    # The anonymous users will have plura ID "charity0" while non-anon will have plura ID "user3"
    # Yes but the charitydata file in the application uses plura ids to identify the charities. We
    # can't really guarantee the IDs otherwise... so it should be here...
    # But we still need to userprofile plura id since that's used for communicating with the server...?
    # May as well return the entire "user0" string instead of just 0 to help with confusion...? 
    # plura_id = models.IntegerField();
    # How about instead of plura_id which now is used for every user, we use a different name?
    # This is the ID that the application uses to identify the charity... That's reasonable, yes?
    app_id = models.IntegerField(); 
     
    name = models.CharField( max_length=60 )
    url = models.CharField( max_length=60 )
    is_enabled = models.BooleanField(default=True);
    description = models.TextField(blank=True)
    
    class Meta:
        ordering = ('name',)
     
    def __unicode__(self):
        return self.name

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    
    team = models.ForeignKey(Team, null=True)    
    charity = models.ForeignKey(Charity)
    last_app_login_date = models.DateTimeField('Date the dude last logged in using the app', null=True)
    last_app_ping_date = models.DateTimeField('Date the dude last pinged the server', null=True)
    
    # This is not needed anymore. The user (NOT userprofile) ID can be used for the plura id, since a 
    # string can be accepted. "user" + id will be the identifier.
    # This will not conflict with the charity plura IDs, since they are "charity" + id
    # The plura ID is just the username...    
    # plura_id = models.CharField( max_length=60 )
    
    # Session key - The program must pass this in with charity_select commands to ensure that a logged in user is doing this.
    # This isn't really necessary since there is only 1 server app command, which both logs in and selects the user ^_^

    def __unicode__(self):
        return self.user.username

class DonateDay(models.Model):
    
    user = models.ForeignKey(User)
    donation_date = models.DateField('date of donation')
    work_units = models.IntegerField()
    team = models.ForeignKey(Team, null=True)
    charity = models.ForeignKey(Charity)
    
    # These are donation amounts made when the user pings the server
    # This will ensure that a few work units are earned even when plura is screwing up
    # and is awarding 0 WU to a particular user.
    fake_work_units = models.IntegerField('work units awarded from server pings (not plura)', default=0)
    
    class Meta:        
        ordering = ('donation_date',)        
        unique_together = (("user", "donation_date"),)
        
    def __unicode__(self):
        return str(self.donation_date)


class History(models.Model):
    user = models.ForeignKey(User)
    create_date = models.DateField('date created', auto_now_add=True)
    
    ACTION_TYPES = [ (0, 'User logged in from app'), ]
    action = models.IntegerField(choices=ACTION_TYPES)
    
    class Meta:        
        ordering = ('create_date',)        
    
    def __unicode__(self):
        return str(self.create_date)     