# Django settings for superdonate project.

# TB -  Used to detect whether we are on the local test machine or the remote production machine (environ variable set in the wsgi files)
import os

ADMINS = (
    ('Tom Bak', 'tom@thomasbak.com'),
)

MANAGERS = ADMINS


DATABASE_ENGINE = 'mysql'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'superdonate'             # Or path to database file if using sqlite3.
DATABASE_USER = 'slartibartfast'             # Not used with sqlite3.
DATABASE_PASSWORD = 'dbx34qp7'         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'


# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

    
SITE_ID = 1

AUTH_PROFILE_MODULE = 'main.UserProfile'

if (not 'DJANGOTEST_SERVER_LOCATION' in os.environ) or (os.environ['DJANGOTEST_SERVER_LOCATION'] != 'remote'):

    #
    # LOCAL / DEVELOPMENT
    #
    
    # Absolute path to the directory that holds media.
    # Example: "/home/media/media.lawrence.com/"
    MEDIA_ROOT = 'C:/p2/superdonate/www/media/'
    
    # URL that handles the media served from MEDIA_ROOT. Make sure to use a
    # trailing slash if there is a path component (optional in other cases).
    # Examples: "http://media.lawrence.com", "http://example.com/media/"
    # MEDIA_URL = 'http://l.media.superdonate.org/'
    # TB - Media URL must be relative to original server or SSL will complain!
    MEDIA_URL = '/media/'
    
    TEMPLATE_DIRS = (
        # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
        'C:/p2/superdonate/www/superdonate_django/templates',
    )

    CACHE_BACKEND = 'dummy:///'    
    #CACHE_BACKEND = 'locmem:///'
    #CACHE_BACKEND = 'file://c:/d/djangocache'
    DEBUG = True
    
else:
    
    #
    # REMOTE / PRODUCTION
    #

    MEDIA_ROOT = '/home/tbak/p/superdonate/www/media/'
    #MEDIA_URL = 'http://media.superdonate.org/'
    MEDIA_URL = '/media/'
    TEMPLATE_DIRS = ( '/home/tbak/p/superdonate/www/superdonate_django/templates' )
    
    # DB backend works... but use dummy for now for quick updates
    # CACHE_BACKEND = 'db://djangocache?timeout=3600'
    # CACHE_BACKEND = 'dummy:///'
    # Try memcached???
    CACHE_BACKEND = 'memcached://127.0.0.1:11211/'  
    DEBUG = False
    # DEBUG = True


TEMPLATE_DEBUG = DEBUG

# On the local machine email is disabled, and emailing myself is retarded.
# This also greatly slows down displaying the page. 
if DEBUG == False and os.environ['DJANGOTEST_SERVER_LOCATION'] == 'remote':
    SEND_BROKEN_LINK_EMAILS = True



# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
# ADMIN_MEDIA_PREFIX = '/media/'
ADMIN_MEDIA_PREFIX = MEDIA_URL + 'admin/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'e&g5!%d87ebjp^=56g!&io!5)o_pzbkZZkqdtq76vt#5dssvc-'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',    
)

if DEBUG:
    MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
        'superdonate_django.debug_middleware.DebugFooter',
        'superdonate_django.djangologging.middleware.LoggingMiddleware',
        #'djangotest.profile_middleware.ProfileMiddleware',
    )   

# Used by djangologging
INTERNAL_IPS = ('127.0.0.1',)
# LOGGING_LOG_SQL = True # Doesn't work. Gives error: 'NoneType' object is unsubscriptable

ROOT_URLCONF = 'superdonate_django.urls'

# Cache all anonymous non-post pages for 1 hour
CACHE_MIDDLEWARE_SECONDS = 60*60
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
CACHE_MIDDLEWARE_KEY_PREFIX = 'superdonate'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'superdonate_django.main',
    'captcha',
    'compress',
    'django_evolution',
    
    # Used to get the intcomma template filter
    'django.contrib.humanize',
)

LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/user/home/'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    
    # This gives me request.path in the context
    # The ones on the top I snagged from django global settings... Is there no way to just append tihs one?
    'django.core.context_processors.request',
    
    # Gives me the user's operating system (used to return the correct download link)
    'main.context_processors.user_os',
)

# CAPTCHA_FONT_PATH = 'fonts/Vera.ttf'
CAPTCHA_FONT_SIZE = 36
CAPTCHA_LETTER_ROTATION = (-15,15)
CAPTCHA_FOREGROUND_COLOR = '#880000'
#CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_arcs', 'captcha.helpers.noise_dots')
CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_dots',)

# Django-compress

COMPRESS = True
COMPRESS_VERSION = True
COMPRESS_CSS_FILTERS = None



COMPRESS_CSS = {
                
    'group_main': {
        'source_filenames': ('superdonate.css', ),
        'output_filename': 'compressed/group_main.?.css',        
    },
    
    'group_datatables': {
        'source_filenames': ('lib/datatables/datatables.css', ),
        'output_filename': 'compressed/group_datatables.?.css',        
    },

}

COMPRESS_JS = {
    
    'group_main': {
        'source_filenames': ('lib/jquery-1.4.min.js', 'lib/jquery.cycle.min.js' ),
        'output_filename': 'compressed/group_main.?.js',
    },
    
    'group_datatables': {
        'source_filenames': ('lib/datatables/jquery.dataTables.min.js', 'lib/datatables/datatables_extra.js' ),
        'output_filename': 'compressed/group_datatables.?.js',
    },

}
