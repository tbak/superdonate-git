﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Drawing;
//using wx;

public class MainForm : wx.Frame
{

    private const int ID_FileOpenDir    = 0;
    private const int ID_FileExit       = 1;

    // Help menu IDs
    private const int ID_HelpAbout      = 2;



    // Golden ratio = 1.618
    public MainForm() : base("Test Application", wxDefaultPosition, new Size(600, 370))
    {
        //wx.BoxSizer
        wx.BoxSizer sizer = new wx.BoxSizer(wx.Orientation.wxVERTICAL);
        wx.TextCtrl foo = new wx.TextCtrl(this, "Foo");
        wx.TextCtrl bar = new wx.TextCtrl(this, "Bar");
        sizer.Add(foo);
        sizer.Add(bar);
        Sizer = sizer;

        BackgroundColour = new wx.Colour(128, 255, 128);



        wx.MenuBar menuBar = new wx.MenuBar();


        // The File menu
        wx.Menu fileMenu = new wx.Menu(); 
        fileMenu.Append(ID_FileExit, "Exit", "Exit this application"); 
        menuBar.Append(fileMenu, "&File");



        // The Help menu
        wx.Menu helpMenu = new wx.Menu(); 
        helpMenu.Append(ID_HelpAbout, "&About...", "About this application"); 
        menuBar.Append(helpMenu, "&Help");


        this.MenuBar = menuBar;

        CreateStatusBar(2);

        // Set the initial status bar text, this will set the text in the 
        // first status bar field.
        StatusText = "Welcome to ImageView!";

        EVT_MENU(ID_FileExit,          new wx.EventListener(OnFileExit));
        EVT_MENU(ID_HelpAbout,          new wx.EventListener(OnHelpAbout));
    }

    protected void OnFileExit(object sender, wx.Event evt)
    {
        // Close the frame.  Since this is the last (only) frame in the
        // application, the application will exit when it is closed.

        Close();
    }


    protected void OnHelpAbout(object sender, wx.Event evt)
    {
        // Message for our about dialog.
        string msg = "ImageView!\n\nAn application for viewing images.";

        // Display a message box.  The message box will have an OK button
        wx.MessageDialog.ShowModal(this, msg, "About ImageView", wx.WindowStyles.DIALOG_OK | wx.WindowStyles.ICON_INFORMATION);
    }
}

class SuperDonateApp : wx.App
{
    public override bool OnInit()
    {
        MainForm mainForm = new MainForm();
        mainForm.Show(true);
        return true;
    }

    public static void Main()
    {
        // Create an instance of our application class
        // ImageViewApp app = new ImageViewApp();

        // Run the application
        // app.Run();
        //System.Console.Ole

        // TB - Without this, I get a "Cannot initialize OLE" error
        wx.Log.IsEnabled = false;

        //System.Console.Out.Write("Hello, world.");
        //Foo();
        SuperDonateApp app = new SuperDonateApp();
        app.Run();

        wx.Log.IsEnabled = true;
    }
}
