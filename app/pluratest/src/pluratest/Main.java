/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pluratest;

import com.pluraprocessing.node.affiliate.desktop.JavaPluraConnector;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 *
 * @author tbak
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        JavaPluraConnector foo = null;

        String affiliate = "ae9da222-577f-574d-144e-f70686e53e8b";
        double cpu = 0.2;
        double bandwidth = 0.2;


        try
        {

            String pluraClientString = "pluratestABC42";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            


            int numCores = 8;

            System.out.println("Connecting...");
            foo = new JavaPluraConnector( affiliate, cpu, bandwidth, pluraClientString, numCores );
            System.out.println("Connected...");
            foo.start();
            System.out.println("Started...");

            while( true ) {

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, -1);
                System.out.println("---------------");

                for( int i = 0; i < 3; i++ ) {

                    int wu = foo.getWorkUnitsCompletedByClient(pluraClientString, cal);
                    System.out.println( sdf.format(cal.getTime()) + "  -- Work units:" + wu );
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }

                Thread.sleep(1000 * 360);                
            }
            
        }
        catch( Exception e ) {
            e.printStackTrace(System.err);
        }

        System.exit(0);

    }

}
