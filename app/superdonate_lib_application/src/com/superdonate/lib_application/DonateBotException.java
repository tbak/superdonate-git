/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_application;

/**
 *
 * @author Administrator
 */
public class DonateBotException extends Exception {

    DonateBotException(String reason) { super(reason); }
}
