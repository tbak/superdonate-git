/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_application;

import com.superdonate.lib_common.OSCheck;

/**
 * Used by DonateBotConnection to determine how long the computer has been idle (no keyboard or mouse activity).
 * The class determines the operating system that the program is running on, and then uses
 * Win32IdleTime or LinuxIdleTime to make the actual JNA system call to query the idle time.
 * If Windows or Linux is not detected, 0 will be returned.
 * @author Administrator
 */
public class IdleTime {

    /**
     * Get how many milliseconds the computer has been idle (no keyboard or mouse activity).
     * @return
     */
    public static int getIdleTimeMillis() {

        int val = 0;

        if( OSCheck.GetOS() == OSCheck.OSType.WINDOWS ) {
            val = Win32IdleTime.getIdleTimeMillis();            
        }
        else if( OSCheck.GetOS() == OSCheck.OSType.MAC ) {            
            val = (int)MacIdleTime.getIdleTimeMillis();
        }
        else if( OSCheck.GetOS() == OSCheck.OSType.LINUX )
        {
            val = (int)LinuxIdleTime.getIdleTimeMillis();
        }
        else
        {
            val = 0;
        }

        
        // DANG. KeyboardUtils doesn't work for Mac... POS!
        /*
        com.sun.jna.examples.KeyboardUtils foo = new com.sun.jna.examples.KeyboardUtils();
        for( int i = 0; i < 100; i++ )
        {
            if( foo.isPressed(i) )
                System.out.println( "" + i + "-" + foo.isPressed(i) );
        }
        */


        return val;

    }

    public static void main(String[] args) {
		
		for (;;) {

			int idleSec = IdleTime.getIdleTimeMillis() / 1000;
			
			System.out.println( idleSec );

			try { Thread.sleep(1000); } catch (Exception ex) {}
		}
	}


}
