/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_application;

import com.superdonate.lib_common.DonateBotConnection;
import com.superdonate.lib_common.InternetChecker;
import com.superdonate.lib_common.OSCheck;
import com.superdonate.update.UpdateApplication;
import java.util.Date;

import java.io.*;
import java.util.Calendar;


// Putting the JavaPluraConnector in a separate thread is apparently safer?
class PluraProcessManager extends Thread {

    volatile private boolean shouldDie = false;
    volatile private boolean shouldStartNewProcess = false;

    private String pluraID;
    private double resourceUsage = 0;    
    private String affiliateID;    

    static private final boolean ultraVerbose = false;

    public boolean isConnected = false;
    public boolean isErrorConnecting = false;
    public boolean isManagingProcess = false;

    // How long to wait for the plura process to respond with a heartbeat.
    // If the process hasn't responded by now, kill it.
    static private final int heartlessTimeLimit = 1000 * 60;

    public PluraProcessManager()
    {
        
    }

    // Kill this thread and the plura process (program is closing down)
    public void signalKillThread()
    {
        shouldDie = true;
    }

    public void signalNewPluraProcess( String pid, double cpu, String affil )
    {
        // A new plura process should start!
        pluraID = pid;
        resourceUsage = cpu;
        affiliateID = affil;
        shouldStartNewProcess = true;
    }   

    private String getSuperDonateWorkerExePath()
    {
        if( OSCheck.GetOS() != OSCheck.OSType.WINDOWS ) {
            return null;
        }

        String filepath = "lib/superdonate_worker.exe";
        File f = new File(filepath);
        if( !f.exists() ) {
            System.out.println("This is a Windows machine but we were unable to locate the worker EXE");
            return null;
        }

        return filepath;
    }

    static public String getSuperDonateWorkerJarPath()
    {
        if( OSCheck.GetOS() == OSCheck.OSType.MAC ) {
            return "/Library/Application Support/SuperDonate/superdonate_worker.jar";
        }

        String filepath = "lib/superdonate_worker.jar";
        File f = new File(filepath);
        if( !f.exists() ) {            
            // Disable absolute path for real-world testing.            
            filepath = "C:/p2/superdonate/app/superdonate_worker/dist/superdonate_worker.jar";
            System.out.println("Using absolute path superdonate_worker.jar");
            f = new File(filepath);
            if( !f.exists() ) {
                // TB TODO
                System.out.println("Unable to locate superdonate_worker.jar -- automatic update required.");
                return null;
            }
            
        }

        return filepath;
    }

    // This looks over the lifetime of a single process
    // This function will end if
    // 1) signalNewPluraProcess() is called (with new cpu/charity info)
    // 2) signalKillThread() is called (program is closing down)
    // 3) The plura process becomes unresponsive (detected within this loop)
    private void startPluraProcess()
    {        
        shouldStartNewProcess = false;

        System.out.println("[PM] StartPluraProcess() -- pluraID:" + pluraID + " CPU:" + Math.round(resourceUsage*100) );
        
        // TB TODO - Get some idea what xmx should be!

        double cpu = resourceUsage;
        double bandwidth = resourceUsage;

        /*
        String filepath = getSuperDonateWorkerJarPath();
        if( filepath == null ) {
            // This will happen if an older user has updated superdonate.jar but has not downloaded
            // the helper JARs yet... The program should schedule an immediate update...
            System.out.println("Unable to locate superdonate_worker.jar -- automatic update required.");
            return;
        }
         */



        ProcessBuilder builder = null;
        String exepath = getSuperDonateWorkerExePath();
        if( exepath != null ) {
            // If we can find superdonate_worker.exe, use that instead since it'll create a nice process name
            // instead of ugly-ass "java.exe"
            String[] command = {exepath, pluraID, Double.toString(cpu), Double.toString(bandwidth), affiliateID };
            System.out.println("Starting worker(EXE)");
            builder = new ProcessBuilder(command);
        }
        else {
            // TB TEMP TEST - Java classpath is already set. We can just run the class directly
            //String[] command = {UpdateApplication.getJavaProcessCommand(), "-Xmx512m", "-Xms512m", "-jar", filepath, pluraID, Double.toString(cpu), Double.toString(bandwidth), affiliateID };
            //String[] command = {UpdateApplication.getJavaProcessCommand(), "-Xmx512m", "-Xms512m", "com.superdonate.worker.PluraWorker", pluraID, Double.toString(cpu), Double.toString(bandwidth), affiliateID };

            // TB NOTE - Does not work with windows.
            String[] command = {UpdateApplication.getJavaProcessCommand(), "-cp" ,"superdonate_application.jar:./lib/superdonate_update.jar:./lib/superdonate_worker.jar", "-Xmx512m", "-Xms512m", "com.superdonate.worker.PluraWorker", pluraID, Double.toString(cpu), Double.toString(bandwidth), affiliateID };
            System.out.println("Starting (JAR)");
            builder = new ProcessBuilder(command);
        }

        System.out.println("Command is: " + builder.toString() );
        

        builder.redirectErrorStream(false);        

        Process process = null;
        long lastWorkerHeartbeatTime = Calendar.getInstance().getTime().getTime();
        
        try {
            process = builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        OutputStream os = process.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;

        InputStream isErr = process.getErrorStream();


        try {

            boolean shouldKillProcess = false;
            //while( !shouldKillProcess  ) {
            while( !shouldKillProcess && !shouldStartNewProcess && !shouldDie ) {

                if( ultraVerbose ) System.out.println("CLIENT LOOP");

                // Check for the client hearbeat
                // Check for connect message as well???

                //while( br.ready() ) {
                if( is.available() > 0 ) {
                    if( ultraVerbose ) System.out.print("Worker says: ");

                    while( is.available() > 0 ) {
                        //char msg = br.read();
                        int msg = is.read();

                        //System.out.print(msg);

                        //System.out.println("Worker says: "  + (char)msg);
                        if( ultraVerbose ) System.out.print((char)msg);
                        switch( msg )
                        {
                        case 0:
                            // Heartbeat
                            if( ultraVerbose ) System.out.println("Client got a worker heartbeat!");
                            lastWorkerHeartbeatTime = Calendar.getInstance().getTime().getTime();
                            //System.out.println("CLKSDFI HEARTBEAT");
                            break;
                        case 1:
                            // Init
                            if( ultraVerbose ) System.out.println("Client got a worker init message!");
                            System.out.println("[PM] Init");

                            // TB TODO - Some kind of error detection?

                            break;
                        case 2:
                            // Connect
                            if( ultraVerbose ) System.out.println("Client got a worker connect message!");
                            isConnected = true;
                            isErrorConnecting = false;
                            System.out.println("[PM] Connect");
                            break;

                        case 3:
                            // Error connecting
                            isErrorConnecting = true;
                            break;

                        default:
                            System.out.print((char)msg);

                        }
                    }

                    if( ultraVerbose ) System.out.println();
                }


                // Output error stream
                if( isErr.available() > 0 ) {
                    System.out.print("[PM] Worker ERROR stream: ");

                    StringBuilder sb = new StringBuilder();

                    while( isErr.available() > 0 ) {
                        //System.out.print((char)isErr.read());
                        sb.append( (char)isErr.read() );
                    }

                    String str = sb.toString();
                    System.out.println( str );

                    // If the message is an exception, it's possible that plura just croaked.
                    // In that case, the worker process should kill itself and restart.
                    if( str.contains("Exception in thread") ) {
                        System.out.println("[PM] Worker threw exception. Restarting process.");
                        shouldKillProcess = true;
                        continue;
                    }
                }

                // Write out a client heartbeat
                bw.write(0);
                bw.flush();

                // Check heartless time
                // Check if we haven't gotten the worker heartbeat for a while.
                // It means that something has screwed up, so we should kill this process and try again.
                long heartlessTime = Calendar.getInstance().getTime().getTime() - lastWorkerHeartbeatTime;
                if( heartlessTime > heartlessTimeLimit ) {
                    System.out.println("[PM] Client hasn't heard from the worker for a while... killing...");
                    shouldKillProcess = true;
                    continue;
                }

                Thread.sleep( 1000 * 1 );

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        isConnected = false;
        
        System.out.println("[PM] startPluraProcess() is finishing up.");
        if( process != null ) {

            // superdonate_worker.exe does not get destroyed by process.destroy()
            // for some reason. So we need to also send a kill message. It won't
            // be as responsive but it'll still work.
            try {
                bw.write(1);
                bw.flush();
            } catch (Exception e) { e.printStackTrace(); }

            System.out.println("[PM] Stopped the worker process.");
            process.destroy();
            process = null;
        }

    }

    @Override
    public void run()
    {
        // Wait for the need to create a plura process
        while( !shouldDie ) {

            // TB TODO - Use a semaphore or something better than this.
            try { Thread.sleep(1000); }
            catch( Exception e ) { }

            if( resourceUsage <= 0 ) {
                //System.out.println("[PM] Resource usage is 0. Not starting a process.");
                continue;
            }
            else {
                // Start a process!
                isManagingProcess = true;
                startPluraProcess();
                isManagingProcess = false;
            }

        }

        System.out.println("[PM] Main Thread about to end.");
        
    }
}




/**
 *
 * The SuperDonate library connection class. Use this class in your Java project
 * to donate food, water, education and rainforest land as people use your
 * Java application.
 *
 * Be sure to call startWork() on the Connection instance to begin communication with the distributed
 * computing system. You can change any of the parameters at any time.
 *
 * Note: This does not work with java applets.
 *
 * @author SuperDonate
 */
public final class DonateBotConnectionApplication extends DonateBotConnection {

    volatile private boolean shouldDie = false;
    private PluraProcessManager pluraProcessManager = null;
    
    /**
     * Create new instance. Also creates a worker thread to handle server requests in the background.<br>
     * You should probably only have one instance of this.
     */
    public DonateBotConnectionApplication()
    {
        // TB TODO - Set to donatebot_lib_application
        // This is set in clientFrame
        //setAffiliateID("ae9da222-577f-574d-144e-f70686e53e8b");

        pluraProcessManager = new PluraProcessManager();
        pluraProcessManager.start();

        enableIdleDetection(true);
    }

    public void applicationIsClosing()
    {
        // Kill this thread and the process manager thread
        // The VM is sometimes crashing in the Windows getIdleTimeMillis() method
        // when the program closes...
        shouldDie = true;
        pluraProcessManager.signalKillThread();

        System.out.println("applicationIsClosing()");
    }

    @Override
    public void run()
    {
        if( PluraProcessManager.getSuperDonateWorkerJarPath() == null ) {
            System.out.println("DonateBotConnectionApplication was unable to locate superdonate_worker!");
            shouldDie = true;
            pluraProcessManager.signalKillThread();
            return;
        }


        // Check every once in a while to see if the plura state has changed,
        // requiring a new plura process to get spawned.
        while( !shouldDie )
        {

            // TB TODO - Use a semaphore or something better than this.
            // I can't really since some settings depend on the time changing, which doesn't have a specific
            // event associated with it.
            try {
                Thread.sleep( 500 );
            }
            catch( Exception e ) {
                // Assume that the thread was killed (program is exiting, applet killed, whatever)
                // So just go away
                if( enableVerbose ) System.out.println("[SuperDonate] Worker thread exiting" );
                return;
                //e.printStackTrace(System.out);
            }

            // Check if the plura process is reporting anything exciting
            if( pluraProcessManager.isConnected ) {
                state = State.RUNNING;
            }
            else if( pluraProcessManager.isErrorConnecting ) {
                state = State.ERROR_CONNECT_PLURA;
            }


            // Chill out for a while longer if we're having trouble connecting
            if( state == State.ERROR_CONNECT_PLURA || getInternetStatus() == InternetChecker.TestStatus.NOT_CONNECTED ) {
                try {
                    Thread.sleep( 2000 );
                }
                catch( Exception e ) {
                    // Assume that the thread was killed (program is exiting, applet killed, whatever)
                    // So just go away
                    if( enableVerbose )
                        System.out.println("[SuperDonate] Worker thread exiting" );
                    return;

                }

            }

            // Calculate the REAL desired usage for right now (factoring in everything) and see if it's different
            // from what it is right now.
            // Check if the just calculated desired usage is different from what it is now (the actual usage)
            boolean computerIsIdle = ( enableIdleDetection && IdleTime.getIdleTimeMillis() > idleRampStartMS );
            float desiredUsageNow = resourceUsageNotIdle;
            if( computerIsIdle ) {
                desiredUsageNow = resourceUsageIdle;
            }

            if( !isAllowedToRun() ) {
                desiredUsageNow = 0;
            }           

            // TB - Screw annonymous connections. You must have a username to connect!
            String pluraIDDesired = serverCommunicator.GetUserPluraString();

            if( pluraIDDesired == null ) {
                desiredUsageNow = 0;
            }

            // Update the resource usage if it has changed!
            // Also if the charity has changed.
            float epsilon = 0.001f;
            if( !pluraIDDesired.contentEquals(pluraIDConnected) ||
                desiredUsageNow + epsilon < resourceUsageActual ||
                desiredUsageNow - epsilon > resourceUsageActual ) {

                System.out.println("updateResourceUsage: " + pluraIDDesired + ", " + desiredUsageNow );

                pluraIDConnected = pluraIDDesired;
                resourceUsageActual = desiredUsageNow;
                pluraProcessManager.signalNewPluraProcess( pluraIDDesired, desiredUsageNow, getAffiliateID() );
            }

            // We are not necessarily connected or anything here...
            // Everything is async


            // Ping the server every once in a while to generate fake work units
            if( pluraProcessManager.isConnected ) {
                Date now = new Date();
                long diff = now.getTime() - lastServerPingTime.getTime();
                if( diff > serverPingFrequencyMS ) {
                    serverCommunicator.PingServerBlocking();
                    lastServerPingTime = now;
                }
            }

        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

    }


}
