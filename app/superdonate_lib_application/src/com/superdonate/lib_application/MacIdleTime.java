/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_application;

import java.awt.*;
import java.util.Date;

/**
 * Note: Only detects mouse activity. Works for Mac. Needs to be called periodically to work correctly.
 * The IdleTime class should be used instead since it can detect your OS and utilize the correct class.
 * Utility method to retrieve the idle time on Windows and sample code to test it.
 * JNA shall be present in your classpath for this to work (and compile).
 * @author ochafik
 */
class MacIdleTime {

    static final int pixelThresholdSq = 15*15;
    static Point lastPos = new Point(0,0);
    static Date lastPosTime = new Date();

	/**
	 * Get the amount of milliseconds that have elapsed since the last input event
	 * (mouse or keyboard)
	 * @return idle time in milliseconds
	 */
	public static int getIdleTimeMillis() {

        try
        {
            PointerInfo pointerInfo = MouseInfo.getPointerInfo();
            Point newPos = pointerInfo.getLocation();

            //System.out.println( newPos );

            if( newPos.distanceSq(lastPos) > pixelThresholdSq )
            {
                // Mouse has moved more than the threshold so we must reset the idle time...
                lastPosTime = new Date();
                lastPos = newPos;
                return 0;
            }
            else
            {
                // Mouse has not moved enough, assume computer is still idle.
                // Update the last pos just for kicks... in case the mouse pointer moves a couple pixels a second(wind, crazy mouse, etc), this will still be idle.
                lastPos = newPos;
                Date now = new Date();
                return( (int)( now.getTime() - lastPosTime.getTime() ) );
            }


        }
        catch( Exception e )
        {
            // do something
            return 0;
        }

	}




}
