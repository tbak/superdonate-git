/*
Copyright 2009, SuperDonate.  All rights reserved.

Test against currentApplicationVersion... e.g. "1.0"
The min version should be ignored "1.0.1" (don't care about them)

*/

package com.superdonate.update;

import com.superdonate.update.*;
import java.io.*;
import java.nio.channels.*;
import java.net.*;
import java.security.MessageDigest;


class MD5Checksum {

    public static byte[] createChecksum(String filename) throws Exception
    {
        InputStream fis =  new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        return complete.digest();
    }

    // see this How-to for a faster way to convert
    // a byte array to a HEX string
    public static String getMD5Checksum(String filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";
        for (int i=0; i < b.length; i++) {
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }
}










public class UpdateApplication extends Thread
{
    // This should be false for production!!!
    public static boolean forceTestUpdate = false;

    public static String processKey = "42";

    private boolean didSuccessfullyDownloadAllNewFiles = false;
    private boolean didError = false;
    private boolean didDownloadOneFile = false;

    public boolean isDone = false;

    public static String filesToUpdate[] = { "superdonate_application.jar", "lib/superdonate_worker.jar", "lib/superdonate_update.jar" };

    private boolean shouldDownloadFromLocalServer = false;
    private final String localFileLocation = "http://l.media.superdonate.org/update/";
    private final String remoteFileLocation = "http://media.superdonate.org/update/";
    private String updateServerLocation = null;

    
    static private final String testLocalFileName = "dist/superdonate_application.jar";


    // Wait 5 mins before checking for an update... to wait for things to settle down?
    private final int initialCheckSleepTime = 1000 * 60 * 5;
    //private final int initialCheckSleepTime = 1000;

    /**
     *
     * @param currentVersion
     * @param args The arguments passed to the program when starting it. (needed if we restart the program)
     */
    public UpdateApplication() {
        
    }

    // The command used to start a new Java process
    public static String getJavaProcessCommand()
    {
        // Try to find the bundled Java VM (Windows)
        String p = System.getProperty("user.dir") + "/jre6/bin/javaw.exe";
        File javaw = new File( p );        
        if( javaw.exists() ) {
            //System.out.println("Running java from bundled JRE");
            return p;
        }
        
        // Uh oh hopefully this'll locate java when executed!
        //System.out.println("Running java from faith");
        return "java";
    }

    private String getTempStorageDir()
    {
        //Get the temporary directory.
        String tmpDir  = System.getProperty("java.io.tmpdir");
        if(!tmpDir.endsWith(File.separatorChar + "")) {
                tmpDir += File.separatorChar;
        }
        tmpDir += "superdonate_update/";
        return tmpDir;
    }

    // Chucks the downloaded file to the temp directory.
    private void downloadFile( String name ) throws IOException, MalformedURLException
    {
        System.out.println("[UpdateApplication] Downloading " + name );

        File tmpDir = new File( getTempStorageDir() );
        if(!tmpDir.exists()) {
            tmpDir.mkdirs();
        }

        File destination = new File(tmpDir, name );
        destination.getParentFile().mkdirs();
        destination.createNewFile();

        // Create a URL that refers to the file on the net.
        URL url = new URL( updateServerLocation + name );

        // Get an input stream for reading
        BufferedInputStream in = new BufferedInputStream( url.openStream() );
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destination));

        int nextByte;
        for(int j=0; (nextByte = in.read()) != -1; j++) {
            out.write(nextByte);
        }

        out.close();
        in.close();

        System.out.println("[UpdateApplication] Done downloading file.");
    }

    private String getChecksumFileContents( String filename ) throws Exception
    {
        // Get an input stream for reading (use the local copy)
        URL url = new URL( "file:" + getTempStorageDir() + filename );

        //BufferedInputStream in = new BufferedInputStream( url.openStream() );
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        String newChecksum = in.readLine();

        in.close();

        return newChecksum;
    }

    private void checkForNewVersion( String filename )
    {
        // Give the other threads a second to settle down...
        // try { Thread.sleep(1000); } catch(Exception e) { }

        boolean isVirginFile = false;

        String MD5FileName = filename + ".md5";

        System.out.println("[UpdateApplication] Checking for new version.");

        try {

            // Get the MD5 for the current JAR we are running
            // If we are running in the IDE the program JAR is in the /dist folder, so we need to check for this.
            // Remote filename does not change... but the local pos might.
            String remoteFilename = filename;
            File f = new File(filename);
            if( !f.exists() ) {
                System.out.println("Update was unable to find file: " + filename + ". Trying /dist" );
                filename = "dist/" + filename;
                f = new File(filename);
                if( !f.exists() ) {
                    // TB TODO - Actually, maybe this shouldn't be an error? It just means that we're downloading
                    // a new file that doesn't exist on this computer... which could happen...
                    System.out.println("Update was STILL unable to find file: " + filename + "!" );
                    System.out.println("Assuming that this is a new file to get");
                    isVirginFile = true;
                }
            }
            
            
            //System.out.println("[UpdateApplication] Current file checksum: " + currentChecksum);


            // We store this in the temp dir next to the downloaded jar so that we
            // can check if a new file is available next update.
            downloadFile(MD5FileName);
            String newChecksum = getChecksumFileContents(MD5FileName);

            boolean shouldDownloadNewFile = false;
            if( isVirginFile ) {
                // Ignore checksums if we don't have this file on our machine yet
                shouldDownloadNewFile = true;
            }
            else {
                String currentChecksum = MD5Checksum.getMD5Checksum( f.getAbsolutePath() );                
                int comp = newChecksum.compareToIgnoreCase(currentChecksum);
                if( comp == 0 ) {
                    // Files are the same
                    shouldDownloadNewFile = false;
                }
                else {
                    shouldDownloadNewFile = true;
                }
            }


            
            if( !shouldDownloadNewFile ) {
                // The files are the same. No need to update!
                System.out.println("[UpdateApplication] The files are the same. No need to update.");
            }
            else {
                System.out.println("[UpdateApplication] New version of " + remoteFilename + " is available for download.");

                // These files are not the same! Download!
                downloadFile(remoteFilename);

                // OK, now we have the Jar. Verify that it's MD5 is correct.
                String downloadedChecksum = MD5Checksum.getMD5Checksum( getTempStorageDir() + remoteFilename );
                //System.out.println(downloadedChecksum);

                int comp = downloadedChecksum.compareToIgnoreCase(newChecksum);
                if( comp == 0 ) {
                    System.out.println("[UpdateApplication] Downloaded successfully. New version will be patched in on program exit.");
                    didDownloadOneFile = true;
                }
                else {
                    System.out.println("[UpdateApplication] Downloaded checksum is inconsistent with what the md5 file said.");
                    didError = true;
                }
            }


        }
        catch( Exception e ) {
            e.printStackTrace();
            didError = true;
        }

    }

    // Use the previously downloaded jar to patch the program.
    // This should be called when the program is exiting (since otherwise we'll have inconsistent JARs/classes)
    public void checkAndPatchDownloadedFile( String filename )
    {
        System.out.println("[UpdateApplication] Check and patch " + filename );

        String MD5FileName = filename + ".md5";

        // See if we have a Jar waiting for us.
        // Then check the actual jar md5 against what the md5 file says
        // If they're different, something got hosed. Delete and return.
        // Check if the program jar md5 is different from this downloaded md5
        // If so, we should update the Jar and restart the program.
        try {
            File programFile = new File(filename);

            // No need to check the current checksum... we've already done this.
            // It's also a hassle in the case that the file we are getting doesn't exist on our machine yet.
            // String currentChecksum = MD5Checksum.getMD5Checksum( programFile.getAbsolutePath() );

            String downloadedChecksum = MD5Checksum.getMD5Checksum( getTempStorageDir() + filename );

            // Check the update.jar.md5 file as well just to be really sure something didn't screw up.
            String updateFileChecksum = getChecksumFileContents(MD5FileName);


            //System.out.println("[UpdateApplication] Current checksum: " + currentChecksum);
            //System.out.println("[UpdateApplication] Downloaded checksum: " + downloadedChecksum);
            //System.out.println("[UpdateApplication] .md5 checksum: " + updateFileChecksum);

            if( downloadedChecksum.compareToIgnoreCase(updateFileChecksum) != 0 ) {
                System.out.println("[UpdateApplication] The update file checksum is not consistent with what was downloaded. Aborting.");
                return;
            }


            System.out.println("[UpdateApplication] Updating " + filename + " to newer version.");

            FileChannel inChannel = new FileInputStream(new File(getTempStorageDir() + filename)).getChannel();
            FileChannel outChannel = new FileOutputStream(programFile).getChannel();
            try {
                inChannel.transferTo(0, inChannel.size(), outChannel);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (inChannel != null) inChannel.close();
                if (outChannel != null) outChannel.close();
            }

            System.out.println("[UpdateApplication] Done updating.");

        }
        catch( FileNotFoundException e ) {
            System.out.println("[UpdateApplication] No update found." );
        }
        catch( Exception e ) {
            System.out.println("[UpdateApplication] EX1: " + e);
        }

    }

    private void deleteDir(File dir)
    {
        if(dir.isDirectory()) {
            String[] children = dir.list();
            for(int i=0; i<children.length; i++) {
                deleteDir(new File(dir, children[i]));
            }
        }
        dir.delete();
    }

    @Override
    public void run()
    {
        if( shouldDownloadFromLocalServer ) {
            System.out.println("************ Downloading updates from local server!");
            updateServerLocation = localFileLocation;
        }
        else {
            updateServerLocation = remoteFileLocation;
        }

        // Del temp files
        deleteDir( new File(getTempStorageDir()) );

        // Skip this stuff if we're testing.
        if( !forceTestUpdate ) {

            File testLocalFile = new File(testLocalFileName);
            if( testLocalFile.exists() ) {
                System.out.println("[UpdateApplication] Running from build. Update aborted.");
                return;
            }

            // Sleep for a little while first to give other net tasks priority.
            try {
                Thread.sleep( initialCheckSleepTime );
            } catch(Exception e) { }

        }

        // Check if there is a newer JAR to download
        for( String filename : filesToUpdate ) {
            checkForNewVersion(filename);

            if( didError ) {
                break;
            }
        }

        // We must download ALL files or the backup is not valid.
        if( didDownloadOneFile && !didError ) {
            didSuccessfullyDownloadAllNewFiles = true;
        }

    }

    public static String getSuperDonateUpdateJarPath()
    {
        String filepath = "lib/superdonate_update.jar";
        File f = new File(filepath);
        if( !f.exists() ) {
            return null;

        }

        return filepath;
    }

    public void startUpdateCopyProcess()
    {
        if( didError ) {
            System.out.println("Not starting update copy process because something went wrong during the tranfer.");
            return;
        }

        // No need to create the process
        if( !didSuccessfullyDownloadAllNewFiles ) {
            System.out.println("No need to start update copy process -- this is the latest version.");
            return;
        }

        System.out.println("Starting update copy process");

        String filepath = getSuperDonateUpdateJarPath();
        if( filepath == null ) {
            // TB TODO - Exit gracefully or pop up a message?
            System.out.println("****** Could not locate superdonate_update.jar!!! Running update from this process instead.");
            for( String filename : filesToUpdate ) {
                checkAndPatchDownloadedFile(filename);
            }
            return;
        }

        String[] command = {getJavaProcessCommand(), "-jar", filepath, processKey };

        ProcessBuilder builder = new ProcessBuilder(command);

        builder.redirectErrorStream(false);

        try {
            builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        if( args.length != 1 || !args[0].contains(processKey) ) {
            System.out.println("To run SuperDonate please use superdonate.exe or superdonate_application.jar");
            System.exit(0);
        }

        // Sleep for a couple seconds to ensure that the main program has closed...
        try { Thread.sleep(6000); }
        catch( Exception e ) { }

        // This actually copies the JAR stuff
        // It runs in a separate process so that it can update the client JAR
        UpdateApplication ua = new UpdateApplication();

        for( String filename : filesToUpdate ) {
            ua.checkAndPatchDownloadedFile(filename);
        }
    }

}
