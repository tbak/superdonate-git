/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

/*
Check if computer is connected to the internet by pinging some websites.
If we are connected to the internet but connecting to plura fails, we know that plura is down.
 */

package com.superdonate.lib_common;

import java.io.*;
import java.net.*;

/**
 *
 * @author COM
 */
public class InternetChecker implements Runnable {    

    // Were we able to connect to the site?
    public enum TestStatus { UNCHECKED, CONNECTED, NOT_CONNECTED };
    private TestStatus testStatus = TestStatus.UNCHECKED;
    private boolean isEnabled = true;

    // Taken from http://www.alexa.com/topsites
    final String siteList[] = new String[] {
        "google.com",
        "yahoo.com",
        "youtube.com",
        "microsoft.com",
        "facebook.com",
        "live.com",
        "bing.com",
        "msn.com",
        "wikipedia.com",
        "blogger.com",
        "myspace.com",
        "rapidshare.com",
        "twitter.com",
        "wordpress.com",
        "ebay.com",
        "craigslist.org",
        "flickr.com",
        "amazon.com",
        "aol.com",
        "hi5.com",
        "doubleclick.com",
        "imdb.com",
        "go.com",
        "ask.com",
        "cnn.com",
        "adobe.com",
        "conduit.com",
        "vmn.net",
        "skyrock.com",
        "imageshack.us",
        "cnet.com",
        "megaupload.com",
        "dailymotion.com",
        "linkedin.com",
        "about.com",
        "mediafire.com",
        "globo.com",
        "files.wordpress.com",
        "nytimes.com",
        "zynga.com",
        "netlog.com",
        "weather.com",
        "synga.com",
        "comcast.net",
        "digg.com",
        "yieldmanager.com",
        "yourfilehost.com",
        "orkut.com",
        "badoo.com",
        "alibaba.com",
        "gamespot.com",
        "metacafe.com",
        "answers.com",
        "mywebsearch.com",
        "imagevenue.com",
        "download.com",
        "dell.com",
        "ning.com",
        "nifty.com",
        "fotolog.net",
        "wikimedia.org",
        "reference.com",
        "depositfiles.com",
        "scribd.com",
        "bebo.com",
        "ezinearticles.com",
        "sourceforge.net",
        "multiply.com",
        "netflix.com",
        "mapquest.com",
        "perfspot.com",
        "rapidlibrary.com",
        "foxnews.com"
    };

    // How many ms to wait between sets of checks
    // This should be pretty high so we don't inadvertently DDOS the server! :)
    private static final int checkFrequencyIfConnected = 1000 * 60 * 5;
    
    // We can check this more frequently since it isn't spamming the web servers
    private static final int checkFrequencyIfNotConnected = 1000 * 5;

    public TestStatus GetStatus()
    {
        return testStatus;
    }

    // Java doesn't natively support ping... It just does port 7 crap
    // that most servers don't listen to anyways... So I guess I'll have to
    // go back to the web checker... :P
    /*
    private boolean CheckSingleSite(String name)
    {
        try {
            InetAddress address = InetAddress.getByName(name);

            boolean isReachable = address.isReachable(3000);
            System.out.println("Reach: " + name + " - " + isReachable );

            return isReachable;
        }
        catch (UnknownHostException e) {
            System.err.println("[InternetChecker] Unable to lookup"+name);
        }
        catch (IOException e) {
            System.err.println("[InternetChecker] Unable to reach"+name);
        }

        return false;
    }
    */

    private boolean CheckSingleSite(String name)
    {
        boolean didConnect = false;
        HttpURLConnection connection = null;

        try {            
            URL serverAddress = new URL(name);

            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(5000);
            connection.connect();

            // At this point we assume success!
            didConnect = true;

        } catch (MalformedURLException e) {
            System.out.println("[InternetChecker] FAILED with MalformedURLException");
            //e.printStackTrace();
        } catch (ProtocolException e) {
            System.out.println("[InternetChecker] FAILED with ProtocolException");
            //e.printStackTrace();
        } catch (IOException e) {
            System.out.println("[InternetChecker] FAILED with IOException");
            //e.printStackTrace();
        }
        finally
        {
            //close the connection, set all objects to null
            connection.disconnect();
        }

        return didConnect;
    }
    

    private void DoCheck()
    {
        
        final int numSitesToTry = 3;

        // Pick a random couple sites to visit
        // Try a few sites until you manage to connect.
        java.util.Random random = new java.util.Random();
        int start = random.nextInt(siteList.length);

        for( int i = 0; i < numSitesToTry; i++ )
        {
            int idx = (start + i) % siteList.length;
            if( CheckSingleSite("http://" + siteList[idx]) )
            {
                if( testStatus != TestStatus.CONNECTED )
                    System.out.println("[InternetChecker] Internet is UP");

                testStatus = TestStatus.CONNECTED;
                return;
            }
        }

        System.out.println("[InternetChecker] Internet is DOWN");
        testStatus = TestStatus.NOT_CONNECTED;
    }

    public void setEnabled( boolean e )
    {
        isEnabled = e;
    }

    private void CheckAllSites()
    {
        System.out.println("****** Checking all sites...");
        for( int i = 0; i < siteList.length; i++ )
        {
            boolean status = CheckSingleSite( "http://" + siteList[i] );
            if( !status )
                System.out.println("SITE DOWN: " + siteList[i] + " = " + status );
        }
        System.out.println("********* Done checking all sites...");
    }

    @Override
    public void run()
    {
        // Screw the internet checker for now... is it freaking people out?
        if( true ) {
            return;
        }

        while(true)
        {
            if( isEnabled ) {
                DoCheck();
            }

            try {
                if( testStatus == TestStatus.CONNECTED )
                    Thread.sleep(checkFrequencyIfConnected);
                else
                    Thread.sleep(checkFrequencyIfNotConnected);
            }
            catch(Exception e) { return; }
        }

    }

    public void StartChecking()
    {
        // Use this to see if a web server is hosed (remove from list?)
        //CheckAllSites();

        Thread workerThread = new Thread(this);
        workerThread.start();
    }

}
