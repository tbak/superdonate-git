/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.superdonate.lib_common;

import java.util.Date;
import java.util.Calendar;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.awt.event.*;
import javax.swing.*;


// The run() loop in DonateBotConnection can stall for many seconds when
// trying to connect to plura. This ensures that the timer value is responsive.
class DonateBotConnectionTimer implements ActionListener {

    DonateBotConnection connection = null;
    private Date lastTotalTimeUpdate = null;
    private float totalSecondsRunning = 0;

    DonateBotConnectionTimer( DonateBotConnection c )
    {
        connection = c;
        lastTotalTimeUpdate = new Date();

        Timer timer = new Timer(500,this);
        timer.setRepeats(true);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {

        if( lastTotalTimeUpdate != null && !connection.isErrorOrNotRunning() ) {

            // Total Time in use
            // TB TODO - Should this be for total time running or just total in general???
            Date now = new Date();
            long diff = now.getTime() - lastTotalTimeUpdate.getTime();
            float inc = diff / 1000.0f;


            // If the time difference is really high, the user probably just
            // changed his system time. In that case, ignore for this update.
            if( inc > 0 && inc < 1 ) {
                totalSecondsRunning += inc;                
            }

        }

        lastTotalTimeUpdate = new Date();
    }

    public int getTotalSecondsRunning()
    {
        return (int)totalSecondsRunning;
    }

    public void setTotalSecondsRunning( int secs )
    {
        totalSecondsRunning = secs;
    }

}

/**
 * Common functions and enums used by both DonateBotConnectionApplication and DonateBotConnectionApplet
 */
public abstract class DonateBotConnection implements Runnable {

    Thread workerThread;

    /**
     * Used to represent the current state of the DonateBot system. Use getState() to retrieve the current value.
     */
    public enum State {

        /**
         * System is starting. Not connected yet.
         */
        INITIALIZING,

        /**
         * SuperDonate is running without any errors. If CPU usage is greater than 0, money will be earned for charity.
         */
        RUNNING,

        /**
         * The system was previously running, but now a new charity has been selected.
         * The system must reconnect to the server to change the charity, which is what is happening during this state.
         */
        SWITCHING_CHARITY,

        /**
         * The system tried to connect to the distributed networking system but failed.
         * The system will automatically retry until it connects.
         * Reasons why a connection might fail: servers are down
         */
        ERROR_CONNECT_PLURA,

        /**
         * Not used
         */
        ERROR
    }

    // If this is not -1, it overrides the pluraID <--> charity connection.
    // In the new system, each registered user has a user ID. This ID is then used in plura
    // to calculate work units. The ServerCommunicator takes care of telling the SuperDonate
    // webserver what charity the user is currently donating for. The admin tool then can check
    // all user plura ids, and then add up the amounts to the charities that were selected.
    protected int pluraUserID = -1;
    public ServerCommunicator serverCommunicator = null;

    // This is now the full client string that is passed to plura
    // It is either "charity0" for old school anonymous charity selection
    // or "user0" for user selection.
    protected String pluraIDConnected = "";
    protected int pluraCharityDesired = -1;

    protected float resourceUsageIdle = 0.90f;
    protected float resourceUsageNotIdle = 0.5f;

    protected boolean enableIdleDetection = true;
    protected int idleRampStartMS = 1 * 60 * 1000; // This gets set in the constructor (setIdleWaitSeconds())    
    
    protected float resourceUsageActual = 0.0f;
    protected boolean hasTriedToConnect = false;

    protected State state = State.INITIALIZING;

    private boolean isEnabled = false;

    protected boolean enableVerbose = false;

    private String affiliateID = "ae9da222-577f-574d-144e-f70686e53e8b";
    

    private DonateBotConnectionTimer connectionTimer;

    protected InternetChecker internetChecker = null;
    public CharityList charityList;

    protected Date lastServerPingTime = null;
    protected final int serverPingFrequencyMS = 1000 * 60 * 55; // About every hour
    //protected final int serverPingFrequencyMS = 1000 * 5; // Every hour

    static final DecimalFormat chartDfNoComma = new DecimalFormat("#");
    static final DecimalFormat chartDf = new DecimalFormat("#,##0");
    static final SimpleDateFormat sdfDay = new SimpleDateFormat("MMM dd, yyyy");

    // Activation time range. Times are represented as mins since midnight
    private boolean activationTimeEnabled;
    private int activationTimeFrom;
    private int activationTimeTo;

    public DonateBotConnection()
    {
        state = State.INITIALIZING;

        charityList = new CharityList();

        serverCommunicator = new ServerCommunicator();
        serverCommunicator.start();

        internetChecker = new InternetChecker();
        internetChecker.StartChecking();

        lastServerPingTime = new Date();
        
        setResourceUsageNotIdle(0.5f);
        enableIdleDetection(true);

        setCharityDesiredToRandom();
        setResourceUsageIdle( 0.90f );
        setIdleWaitSeconds(60 * 1);

        connectionTimer = new DonateBotConnectionTimer(this);
        
        startWorkerThread();
    }

    public void setAffiliateID( String s )
    {
        // This prevents rouge affilate ids from being used
        // Don't worry about this for now since I'm creating test affilates to get my shit together.
        /*
        if( s != "ae9da222-577f-574d-144e-f70686e53e8b" )
            return;
        */

        affiliateID = s;
    }

    
    protected String getAffiliateID()
    {
        return affiliateID;
    }
    
    protected void startWorkerThread()
    {

        if( workerThread == null || !workerThread.isAlive() )
        {
            workerThread = new Thread(this);
            workerThread.start();
        }
        
    }

    public boolean isWorkerThreadAlive()
    {
        return workerThread.isAlive();
    }

    public boolean isErrorOrNotRunning()
    {
        if( !isEnabled() ||
            getInternetStatus() == InternetChecker.TestStatus.NOT_CONNECTED ||
            getConnectionState() == DonateBotConnection.State.ERROR_CONNECT_PLURA ||
            getConnectionState() == DonateBotConnection.State.ERROR )
        {
            return true;
        }

        return false;
    }

    @Override
    abstract public void run();


    /**
     * Set the charity that should receive money earned in exchange for CPU processing power.<br>
     * See setCharityDesired(Charity) for a enum version of this function.
     * 0 = Charity:Water<br>
     * 1 = CARE<br>
     * 2 = Oaktree Foundation<br>
     * 3 = The Nature Conservancy<br>
     * @param c
     */
    public void setCharityDesired( int c )
    {
        if( c < 0 || c >= charityList.GetNumCharities() )
        {
            throw new IllegalArgumentException();
        }

        pluraCharityDesired = c;
        serverCommunicator.SetCharity(c);
    }

    /**
     * Set the charity that should receive money earned in exchange for CPU processing power.<br>
    **/
    /*
    public void setCharityDesired( Charity c )
    {
        setCharityDesired( c.pluraID );
    }
    */

    /**
     * Get the charity that you want to donate to.
     * This value is immediately set when setCharityDesired() is called.
     * getCharityConnected() will first return this value when the connection to the
     * server has actually been made.
     * @return
     */
    public int getCharityDesired()
    {
        return pluraCharityDesired;
    }

    /**
     * Return the charity that is currently being donated for.
     * @return
     */
    /*
    public int getPluraIDConnected()
    {
        return pluraIDConnected;
    }
    */

    /**
     * This starts using the SuperDonate system to exchange CPU processing power for charity.
     * You must call this or no work will be done.
     * @param b Whether to start or stop SuperDonate.
     */
    public void setEnabled( boolean b )
    {
        isEnabled = b;

        // Enable/disable the internet checker as well
        internetChecker.setEnabled(b);
    }

    public boolean isEnabled()
    {
        return isEnabled;
    }

    /*
     * Select a random available charity to donate to.
    */
    public void setCharityDesiredToRandom()
    {
        java.util.Random random = new java.util.Random();
        
        boolean isVisible = false;
        int c=0;
        
        while( !isVisible ) {
            c = random.nextInt(charityList.GetNumCharities());
            if( charityList.GetCharityFromPluraID(c).isVisible ) {
                isVisible = true;
            }
        }

        setCharityDesired(c);
    }

    /**
     * @param pct
     */
    public void setResourceUsageNotIdle( float pct )
    {
        if( pct < 0 || pct > 1.0 ) {
            throw new IllegalArgumentException();
        }

        resourceUsageNotIdle = pct;
    }

    public float getResourceUsageNotIdle()
    {
        return resourceUsageNotIdle;
    }

    public float getResourceUsageIdle()
    {
        return resourceUsageIdle;
    }

    public int getIdleWaitSeconds()
    {
        return idleRampStartMS / 1000;
    }

    /**
     * Set the number of seconds where no keyboard or mouse activity was detected before entering idle mode.
     * When in idle mode, resource usage will be set to the value set by setIdleResourceUsage().
     * The default is 300 seconds (5 minutes).
     * @param seconds
     */
    public void setIdleWaitSeconds( int seconds )
    {
        idleRampStartMS = seconds * 1000;
    }

    /**
     * The amount of CPU resources to use when the computer is idle.
     * Values from 0(0%) to 1(100%) are valid.
     * The computer is determined to be idle if no keyboard or mouse activity has been
     * detected for X seconds (see setIdleWaitSeconds()).
     * The default is 0.9.
     * @param pct
     */
    public void setResourceUsageIdle( float pct )
    {
        if( pct < 0 || pct > 1.0 ) {
            throw new IllegalArgumentException();
        }

        resourceUsageIdle = pct;
    }


    /**
     * Set to true if you want SuperDonate to check if the computer has not been used for a while.
     * If no keyboard or mouse activity has been detected for X seconds (see setIdleWaitSeconds()),
     * the system will change resource usage to the value set by setIdleResourceUsage().
     * Note: Only works in Windows and Linux. On other systems this parameter is ignored.
     * By default it is on.
     * @param enable
     */
    public void enableIdleDetection( boolean enable )
    {
        enableIdleDetection = enable;
    }

    /**
     * @return enabled
     */
    public boolean isIdleDetectionEnabled()
    {
        return enableIdleDetection;
    }


    /**
     * Get the resource usage currently being used by the system. This will either be the value set by
     * setResourceUsage() or setIdleResourceUsage(), depending on whether the computer is idle.
     * @return
     */
    public float getResourceUsageActual()
    {
        return resourceUsageActual;
    }


    /**
     * Turn on or off display of SuperDonate events to System.out.
     * This is useful for quick debugging and ensuring that the system is in fact running.
     * By default it is off.
     * @param enable
     */
    public void enableVerbose( boolean enable )
    {
        enableVerbose = enable;
    }

    /**
     * Get the state of the SuperDonate system. See documentation for the State enum for more info.
     * @return
     */
    public State getConnectionState()
    {
        return state;
    }

    /**
     * Return whether the system has tried to connect to the Plura distributed computing network at least once.
     */
    public boolean getHasTriedToConnect()
    {
        return hasTriedToConnect;
    }

    public int getTotalSecondsRunning()
    {
        return connectionTimer.getTotalSecondsRunning();
    }

    // This is used by the frame to set the value from the previously saved time.
    public void setTotalSecondsRunning(int s)
    {
        connectionTimer.setTotalSecondsRunning(s);
    }

    public InternetChecker.TestStatus getInternetStatus()
    {
        return internetChecker.GetStatus();
    }

    /*
    // TB TODO - This method does not really belong here
    // All charts stuff is now handled by the web server.
    public String getGoogleChartsString( int imageWidth, int imageHeight, String title, String labels, int maxVal, String chartDataValues, String xAxis )
    {
        // Generate the dynamic url before passing it on to the more generic setRemoteImage
        final String startUrl = "http://chart.apis.google.com/chart?";

        String chartDayString = "";
        chartDayString += xAxis + "|";

        String chartURIString;        
        //chartURIString = "chtt=" + title + "&chm=D,0000FF,0,0,2,1&chd=t:";
        chartURIString = "chtt=" + title;

        // Transparent
        chartURIString += "&chf=bg,s,00000000|c,s,00000000";

        // Fill light blue under the line
        chartURIString += "&chm=B,76A4FB,0,0,0";
        chartURIString += "&chd=t:";

        //chartURIString += chartDataValues[0] + "|" + chartDataValues[1] + "|" + chartDataValues[2] + "|" + chartDataValues[3];
        chartURIString += chartDataValues;

        chartURIString += "&chds=0,";
        chartURIString += maxVal;
        //chartURIString += "&chdl=charity: water|CARE|Oaktree Foundation|Nature Conservancy&chco=0000FF,FF0000,CC00CC,00FF00,000000&cht=lc&chxt=x,y,x&chxl=0:|";
        chartURIString += "&chdl=" + labels + "&chco=0000FF,FF0000,CC00CC,00FF00,000000&cht=lc&chxt=x,y,x&chxl=0:|";
        chartURIString += chartDayString;
        chartURIString += "1:|0|";
        chartURIString += chartDf.format(maxVal/2) + "|" + chartDf.format(maxVal);
        chartURIString += "|2:|";
        chartURIString = chartURIString.replace("|", "%7c");
        chartURIString = chartURIString.replace(" ", "%20");

        String url = startUrl + "chs=" + imageWidth + "x" + imageHeight + "&" + chartURIString;
        // System.out.println(url);

        return url;
    }

    // TB TODO - This method does not really belong here    
    public String getPersonalDailyAmountsGoogleChartsString( int imageWidth, int imageHeight, String title )
    {        
        int maxVal = 1;
        int numDays = connectionTimer.personalDailyAmounts.data.size();
        
        // 0=minutes, 1=work units
        String chartDataValues[] = { "", "" };
        
        for( int i = 0; i < numDays; i++ ) {
            int val0, val1;

            val0 = (int)(connectionTimer.personalDailyAmounts.data.get(i).secondsRunning / 60);
            val1 = (int)connectionTimer.personalDailyAmounts.data.get(i).workUnits;

            if( i != 0 ) {
                chartDataValues[0] += ",";
                chartDataValues[1] += ",";
            }

            chartDataValues[0] += chartDfNoComma.format(val0);
            chartDataValues[1] += chartDfNoComma.format(val1);

            if( val0 > maxVal ) {
                maxVal = val0;
            }
            if( val1 > maxVal ) {
                maxVal = val1;
            }
        }

        String xAxis = "";
        xAxis += sdfDay.format( connectionTimer.personalDailyAmounts.startDate.getTime());
        xAxis += "|";
        // TB TODO - Display a date value in the middle?
        xAxis += sdfDay.format( new Date() );        

        return getGoogleChartsString( imageWidth, imageHeight, title, "Minutes|Work Units", maxVal, chartDataValues[0]+"|"+chartDataValues[1], xAxis );
    }
    */

    /* Strip out the day/month/year info, return as # of ms */
    private int getMinsSinceMidnight( Date d )
    {
        // return d.getMinutes() + d.getHours() * 60;

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return c.get(Calendar.MINUTE) + c.get(Calendar.HOUR_OF_DAY) * 60;
    }

    private boolean isTimeBetweenTwoTimes( Date now, int fromtime, int totime ) {

        // Only care about current time, not day
        int nowtime = getMinsSinceMidnight(now);
        
        //System.out.println( "" + nowtime + "   " + fromtime + "   " + totime );

        if( fromtime < totime ) {
            // Does not wrap over midnight
            // ----(from)-------(to)--->
            if( nowtime >= fromtime && nowtime < totime ) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            // This time duration wraps over midnight
            // ----(to)-------(from)--->
            if( nowtime < totime || nowtime >= fromtime ) {
                return true;
            }
            else {
                return false;
            }
        }

    }

    protected boolean isAllowedToRun() {
        
        // If plura is not enabled, then we're done
        if( !isEnabled ) {
            return false;
        }

        // User connected to the superdonate server?
        // This means that anonymous connections are no longer supported...
        if( !serverCommunicator.isLoggedIn ) {
            return false;
        }

        // If we don't care about the time, then it's always OK!
        if( !getActivationTimeEnabled() ) {
            return true;
        }

        // Check if we are within the valid range
        if( isTimeBetweenTwoTimes( new Date(), getActivationTimeFrom(), getActivationTimeTo() ) ) {
            return true;
        }

        // This time is outside of the allowed range, so don't allow it
        return false;
    }

    private void testTimeDuration( Date from, Date to ) {
        Date now = Calendar.getInstance().getTime();

        int fromtime = getMinsSinceMidnight(from);
        int totime = getMinsSinceMidnight(to);

        for( int i = 0; i < 24 * 15; i++ ) {
            boolean result = isTimeBetweenTwoTimes(now, fromtime, totime);
            System.out.println( "" + result + ": " + now + "   " + from + "   " + to );

            // Inc 15 mins
            now.setTime( now.getTime() + 1000*60*15 );
        }
    }


    // from and to are ms times, stripped of day/month/year info
    public void setActivationTime( boolean enabled, int fromMinsSinceMidnight, int toMinsSinceMidnight )
    {
        activationTimeEnabled = enabled;
        activationTimeFrom = fromMinsSinceMidnight;
        activationTimeTo = toMinsSinceMidnight;
    }

    public void setActivationTime( boolean enabled, Date from, Date to )
    {
        setActivationTime( enabled, getMinsSinceMidnight(from), getMinsSinceMidnight(to) );
    }

    public boolean getActivationTimeEnabled() { return activationTimeEnabled; }
    public int getActivationTimeFrom() { return activationTimeFrom; }
    public int getActivationTimeTo() { return activationTimeTo; }

    // Load from the current file pointer position
    public void loadData( DataInputStream dis )
            throws java.io.IOException
    {
        // TB TODO - Fix this up.
        int charity = dis.readInt();
        setCharityDesired(charity);
        
        int totalRunSecs = dis.readInt();
        setTotalSecondsRunning(totalRunSecs);

        setResourceUsageNotIdle( dis.readInt() / 100.0f );
        setResourceUsageIdle( dis.readInt() / 100.0f );
        setIdleWaitSeconds( dis.readInt() );

        setActivationTime(dis.readBoolean(), dis.readInt(), dis.readInt());



        // We save the userPluraString, which is just the username, to indicate that it
        // correctly logged on and can be used instantly when the program starts again.
        // This way we don't start plura with an anonymous plura string, followed by
        // a user plura string once the login stuff finishes.
        String username = dis.readUTF();
        String passwordEnc = dis.readUTF();
        String userPluraString = dis.readUTF();

        serverCommunicator.LoginAsyncPasswordEncrypted(username, passwordEnc, charity);
        
    }

    // Save to the current file pointer position
    public void saveData( DataOutputStream dos )
            throws java.io.IOException
    {
        dos.writeInt( getCharityDesired() );

        dos.writeInt( getTotalSecondsRunning() );

        dos.writeInt( (int)(getResourceUsageNotIdle() * 100) );
        dos.writeInt( (int)(getResourceUsageIdle() * 100) );
        dos.writeInt( getIdleWaitSeconds() );

        dos.writeBoolean( getActivationTimeEnabled() );
        dos.writeInt( getActivationTimeFrom() );
        dos.writeInt( getActivationTimeTo() );



        dos.writeUTF( serverCommunicator.GetUsername() );
        dos.writeUTF( serverCommunicator.GetPasswordEnc() );
        // Even though we can get the plura ID from username and password, it'd be nice to have it
        // instantly available when the program starts. This way we don't start a plura connection with
        // an anonymous ID, only to then immediately start another connection with a user ID.
        dos.writeUTF( serverCommunicator.GetUserPluraString() );
    }



    // Screw all this and just use the servercommunicator
    /*
    // I guess there also needs to be a loginasync?
    // Because otherwise the client frame will have to keep retrying to login, which sucks!
    // FUKKKKKKK
    public void LoginAsync( String u, String p ) {
        // Keep trying to login if there was a server error?
        // So then the donatebotconnection needs to keep track of the login status...?
        // But run() has been snatched already.
        // So the servercommunicator should continue to do that???
    }


    // No need to pass in the charity since we know what that is.
    public int Login( String u, String p ) {
        // Store username, password
        // Try to log in
        // Update pluraUserID if successful. Otherwise set to null (anonymous)
        // Return the status.

        int result = serverCommunicator.LoginBlocking(u, p, getCharityDesired());
        switch( result ) {
        case 1:
            // Set the username, password, pluraID
            break;
        }

        // Propagate the error to the calling methods, which will update their status bars and dialogs and whatnot.
        // This way the client can try to log in once stuff has been loaded (and then update the status bar)
        // And the login dialog can do the same by calling the client code (which will update the status bar), and then update the dialog...
        return result;
    }
    */


}
