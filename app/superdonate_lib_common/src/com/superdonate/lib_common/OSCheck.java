/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_common;

/**
 *
 * @author Administrator
 */
public class OSCheck {
    public enum OSType { WINDOWS, MAC, LINUX };

    public static OSType GetOS()
    {
        String osName = System.getProperty("os.name");
        if( osName.contains("Windows") ) {
            return OSType.WINDOWS;
        }
        else if( osName.contains("Mac") ) {
            return OSType.MAC;
        }
        else if( osName.contains("Linux") )
        {
            return OSType.LINUX;
        }
        else
        {
            // Assume Linux...
            return OSType.LINUX;
        }
    }
}
