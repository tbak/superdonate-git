/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_common;

import java.util.Date;

/**
 *
 * @author COM
 */

public class Charity {
    public int index;
    public String name;
    public int pluraID;
    public String infoText;
    public String websiteURL;
    public Date startDate;
    
    // Can it be seen on the website, application, etc
    public boolean isVisible;
    
    Charity(int idx, String n, int id, String info, String url, boolean vis, Date start )
    {
        index = idx;
        name = n;
        pluraID = id;
        infoText = info;
        websiteURL = url;
        isVisible = vis;
        startDate = start;
    }

    Charity()
    {
        index = 999;
        name = "Initialized charity";
    }
}
