/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_common;

import java.io.*;
import java.nio.channels.*;

// Snagged from http://www.rgagnon.com/javadetails/java-0064.html
public class FileUtils {

    public static void CopyFile(File in, File out)
        throws IOException
    {
        FileChannel inChannel = new
            FileInputStream(in).getChannel();
        FileChannel outChannel = new
            FileOutputStream(out).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(),
                    outChannel);
        }
        catch (IOException e) {
            throw e;
        }
        finally {
            if (inChannel != null) inChannel.close();
            if (outChannel != null) outChannel.close();
        }
    }

}