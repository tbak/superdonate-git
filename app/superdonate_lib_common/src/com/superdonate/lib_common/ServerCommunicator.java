/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.superdonate.lib_common;

import java.io.*;
import java.net.*;
import java.util.concurrent.Semaphore;

public class ServerCommunicator extends Thread
{
    private String webServerAddress = "http://www.superdonate.org/";
    private String appServerAddress = "http://app.superdonate.org/";

    volatile private String sessionKey = null;
    volatile private String username = null;
    volatile private String password_enc = "";

    public boolean isLoggedIn = false;
    public boolean isLogInError = false;
    public boolean isUsernamePasswordError = false;
    volatile private String userPluraString = "";
    public String teamString = "";
    public int teamID = -1;

    volatile private int serverCharity = -1;
    volatile private int desiredCharity = -1;

    public String statusBarText = "";
    public int statusBarCode = 1;

    Semaphore signal = null;

    //private ClientFrame clientFrame;

    public ServerCommunicator()
    {
        SetStatusBarText("User is not logged in", 1);
        signal = new Semaphore(0);
    }

    public void SetWebServerAddress( String s )
    {
        webServerAddress = s;
    }

    public String GetWebServerAddress()
    {
        return webServerAddress;
    }

    public String GetAppServerAddress()
    {
        return appServerAddress;
    }

    public void SetAppServerAddress( String  s )
    {
        appServerAddress = s;
    }

    public static String[] ReadURL( String location, String postParams ) {

        try {
            //System.out.println(postParams);

            byte[] parameterAsBytes = postParams.getBytes();

            // Create connection
            URL url = new URL(location);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-length",String.valueOf (postParams.length()));

            // Create I/O streams
            DataOutputStream outStream = new DataOutputStream(urlConnection.getOutputStream());

            // Send request
            outStream.write(parameterAsBytes);
            outStream.flush();

            BufferedReader reader = new BufferedReader( new InputStreamReader( new DataInputStream(urlConnection.getInputStream()) ));

            int maxLines = 10;
            int numLinesRead = 0;
            String [] lines = new String[maxLines];
            String line = reader.readLine();
            while( line != null && numLinesRead < maxLines ) {

                // TB TEMP
                // System.out.println(line);

                lines[numLinesRead] = line;
                numLinesRead++;
                line = reader.readLine();                
            }


            // Close I/O streams
            reader.close();
            outStream.close();

            //return sb.toString();
            return lines;
        }
        catch(Exception ex) {
            System.out.println("ReadURL() Exception caught:\n"+ ex.toString());
            return null;
        }
    }

    private static String XorEncrypt(String toEnc) {
        // Start at key 42, and then increase by 1 for each letter
        int encKey = 42;
        int t=0;
        String s1="";
        String tog="";
        if(encKey>0) {
            while(t < toEnc.length()) {
                int a=toEnc.charAt(t);
                int c=a ^ encKey;
                char d=(char)c;
                tog=tog+d;
                t++;
                encKey++;
            }

        }
        return tog;
    }

    private void SetStatusBarText(String s, int c ) {
        statusBarText = s;
        statusBarCode = c;
    }

    public void PingServerBlocking() {

        System.out.println("App Server ping");
        String[] result = ReadURL( GetAppServerAddress() + "app/ping/", "username=" + username + "&session_key=" + sessionKey );
        
        // TB TODO - Error checking of some sort?

        // The worker process can't ping the server since it gets started/stopped a bunch.
        // It should be the client frame process.

    }

    public int LoginBlockingPasswordEncrypted( boolean autoRetry ) {

        isLoggedIn = false;

        if( username.contentEquals("") ) {
            SetStatusBarText("Please login", 1);
            return 2;
        }

        String[] result = ReadURL( GetAppServerAddress() + "app/login/", "username=" + username + "&password_enc=" + password_enc + "&charity=" + desiredCharity );

        if( result != null && result[0].charAt(0) == '1' ) {

            isLogInError = false;
            isUsernamePasswordError = false;
            isLoggedIn = true;

            String sb = null;
            if( result[1].contentEquals("***") ) {
                sb = "You are logged in as: " + username;
                teamString = "";
                teamID = -1;
            }
            else {
                teamString = result[1];
                teamID = Integer.decode(result[2]);
                sb = "You are logged in as: " + username + " (" + teamString + ") ";
            }

            sessionKey = result[3];

            // The user plura string is just the username.
            // But if it's set here (and not ""), that means use that instead of the anonymous "charity" lines.
            userPluraString = username;

            serverCharity = desiredCharity;
            SetStatusBarText(sb, 0);

            return 1;
        }
        else if( result != null && result[0].charAt(0) == '2' ) {
            // Bad username/password
            isLogInError = true;
            isUsernamePasswordError = true;
            sessionKey = null;
            userPluraString = "";
            SetStatusBarText("Username/password is incorrect.", 1);

            return 2;
        }
        else {
            // Misc error
            isLogInError = true;
            isUsernamePasswordError = false;
            sessionKey = null;
            if( autoRetry ) {
                SetStatusBarText("Couldn't connect to server. Retrying...", 1);
                try { Thread.sleep( 5000 * 1 ); }
                catch (Exception e) { }
                signal.release();
            }
            else {
                SetStatusBarText("Couldn't connect to server.", 1);
            }

            return 3;
        }

    }

    @Override
    public void run() {

        while(true) {

            try { signal.acquire(); }
            catch (Exception e) { }

            if( username != null && ( (serverCharity != desiredCharity) || (sessionKey == null) )) {
                
                // If we are already logged in but we changed the charity, the program will log in again to
                // set the charity. But there's no need to display this for the user...
                if( sessionKey == null ) {
                    SetStatusBarText("Signing in...", 0);
                }


                //LoginBlocking(username, password_enc, serverCharity);
                LoginBlockingPasswordEncrypted( true );
                

            }

        }
    }

    public void LoginAsyncPasswordEncrypted( String u, String p_enc, int charity ) {
        // Sets a variable to let the communicator thread try to log in.
        // If it fails, it'll keep retrying, unlike LoginBlocking which attempts to login instantly.
        sessionKey = null;
        username = u;
        password_enc = p_enc;
        signal.release();
    }

    public void LoginAsync( String u, String p, int charity ) {
        // Sets a variable to let the communicator thread try to log in.
        // If it fails, it'll keep retrying, unlike LoginBlocking which attempts to login instantly.
        LoginAsyncPasswordEncrypted(u,XorEncrypt(p), charity);
    }

    public int LoginBlocking( String u, String p, int charity ) {

        // TB TODO - Keep track of the log in status.
        // TB TODO - Set the plura ID...
        username = u;
        password_enc = XorEncrypt(p);
        serverCharity = charity;
        return LoginBlockingPasswordEncrypted( false );

    }
    

    public String GetCaptchaIDBlocking()
    {
        String[] result = ReadURL( GetAppServerAddress() + "app/prepare_register/", "" );

        // TB TODO - Better error handling
        if( result != null ) {
            return result[0];
        }
        else {
            return null;
        }

    }

    public String[] RegisterNewAccountBlocking( String username, String email, String password, String password2, String captchaID, String captchaAnswer )
    {
        String[] result = ReadURL( GetAppServerAddress() + "app/register/", "username=" + username + "&email=" + email + "&password=" + password + "&password2=" + password2 + "&captcha_0=" + captchaID + "&captcha_1=" + captchaAnswer );
        
        // TB TODO - Better error handling
        // Or whatever. I guess the dialog can deal with it.
        return result;
    }

    public void Logout() {
        //sessionKey = null;
        //userPluraString = "";
        //username = "";
        //password_enc = "";
        LoginAsync( "", "", desiredCharity );
    }

    public String GetUsername() {
        if( username == null ) {
            return "";
        }
        
        return username;
    }

    public String GetPasswordEnc() {
        return password_enc;
    }

    public String GetUserPluraString() {
        return userPluraString;
    }

    public void SetCharity( int c ) {
        desiredCharity = c;
        signal.release();
    }
}
