/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package com.superdonate.lib_common;
import java.util.List;
import java.util.ArrayList;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author COM
 */

public class CharityList {

    List<Charity> charities = new ArrayList<Charity>();

    public CharityList()
    {
        LoadCharityDataFile();
    }

    // The charity data is now located in the JAR file itself, so it's automatically loaded when an
    // instance of CharityList is created!
    /*
    public boolean LoadCharityDataFile()
    {
        // Load the default one
        return LoadCharityDataFile("client_data/charitylist.dat");
    }
    */

    // The data file format:
    // Version Number
    // Number of charities
    // +++
    // (Charities)
    //   Name (if +++, it means the data file is done)
    //   Plura ID
    //   Text
    //   URL
    //   ---
    // Returns true if success, false if failure
    //private boolean LoadCharityDataFile(String dataFileName)
    private boolean LoadCharityDataFile()
    {   

        /*
        File f;
        try {
            URL url = CharityList.class.getResource("charity_data/charitylist.dat");
            //f = new File(url);
            BufferedInputStream is = new BufferedInputStream(url);
        }
        catch( Exception e )
        {
            System.out.println("LoadCharityDataFile() URL error!");
            return false;
        }
        */
        

        int numExpectedCharities = 0;
        int numActualCharities = 0;
        boolean success = false;

        charities.clear();

        //File f = new File(dataFileName);
        //if( f.exists() )
        //{
            try
            {
                InputStream fis = this.getClass().getResourceAsStream( "charity_data/charitylist.dat" );
                //FileInputStream fis = new FileInputStream(f);


                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);

                int version = Integer.parseInt( br.readLine() );
                numExpectedCharities = Integer.parseInt( br.readLine() );
                String startOfCharityList = br.readLine();
                if( version != 1 || !startOfCharityList.contains("+++") )
                {
                    System.out.println("CharityList: Charity data file version is not 1 or +++ was not found afterwards");
                    return false;
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

                while(true)
                {
                    String name = br.readLine();
                    if( name.contains("+++")) {
                        success = true;
                        break;
                    }

                    if( name.length() <= 1 )
                    {
                        System.out.println("CharityList: Charity name length is less than 1.");
                        return false;
                    }

                    int pluraID = Integer.parseInt( br.readLine() );
                    if( pluraID < 0 || pluraID > 1000 )
                    {
                        System.out.println("CharityList: Bad plura ID");
                    }

                    String infoText = br.readLine();
                    if( infoText.length() <= 1 )
                    {
                        System.out.println("CharityList: Charity info text length is less than 1.");
                        return false;
                    }
                    infoText = infoText.replace("\\n","\n");

                    String webURL = br.readLine();
                    if( webURL.length() <= 1 )
                    {
                        System.out.println("CharityList: Website url text length is less than 1.");
                        return false;
                    }

                    String options = br.readLine();
                    if( !options.contains("VISIBLE") && !options.contains("HIDDEN") )
                    {
                        System.out.println("CharityList: VISIBLE or HIDDEN option was not found.");
                        return false;
                    }

                    String startDateStr = br.readLine();
                    Date startDate = dateFormat.parse(startDateStr);                    

                    String endOfCharity = br.readLine();
                    if( !endOfCharity.contains("---") )
                    {
                        System.out.println("CharityList: The --- charity terminator was not found.");
                        return false;
                    }

                    Charity c = new Charity();
                    c.index = numActualCharities;
                    c.name = name;
                    c.pluraID = pluraID;
                    c.infoText = infoText;
                    c.websiteURL = webURL;
                    c.startDate = startDate;

                    if( options.contains("VISIBLE") )
                        c.isVisible = true;
                    else
                        c.isVisible = false;

                    charities.add(c);

                    numActualCharities++;
                }

                br.close();
                isr.close();
                fis.close();
            }
            catch( Exception e ) { 
                e.printStackTrace(System.out);
            }
        //}
        //else
        //{
        //    System.out.println("CharityList: ERROR: Unable to open charitylist.dat!");
        //    return false;
        //}

        if( numActualCharities == 0 || numExpectedCharities == 0 || numActualCharities != numExpectedCharities )
        {
            System.out.println("CharityList: Num expected/actual charities do not match.");
            return false;
        }

        if( success == false )
        {
            System.out.println("CharityList: Success is false.");
            return false;
        }

        return true;
    }

    public Charity GetCharityFromIndex(int index)
    {
        return charities.get(index);
    }

    public Charity GetCharityFromPluraID(int pID)
    {
        for( int i = 0; i < charities.size(); i++ )
        {
            Charity c = charities.get(i);
            if( c.pluraID == pID )
                return c;
        }

        return null;
    }

    public int GetNumCharities()
    {
        return charities.size();
    }

    public int GetNumVisibleCharities()
    {
        int count = 0;
        for( int i = 0; i < charities.size(); i++ )
        {
            if( charities.get(i).isVisible )
                count++;
        }

        return count;
    }

}
