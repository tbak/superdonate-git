/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

package admin_updatedatabase;

import com.superdonate.lib_common.CharityList;

import java.util.Calendar;
import java.io.*;
import java.text.*;

import com.pluraprocessing.node.affiliate.desktop.JavaPluraConnector;


import java.sql.*;


public class AdminUpdateDatabase {

    final int connectionTimeout = 1000 * 60 * 1;

    boolean doQueryPlura = true;
    //boolean doQueryPlura = false;

    // For some reason on linode 127.0.0.1 will not work...???
    String JdbcConnectionUrl = "jdbc:mysql://127.0.0.1:3306/superdonate";
    //String JdbcConnectionUrl = "jdbc:mysql://69.164.214.209:3306/superdonate";
    String JdbcConnectionUsername = "slartibartfast";
    String JdbcConnectionPassword = "dbx34qp7";

    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");


    private Calendar GetLastDonateDay(Connection con) throws Exception
    {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery ("SELECT donation_date FROM main_donateday ORDER BY donation_date DESC LIMIT 1;");
        rs.next();
        Date thedate = rs.getDate("donation_date");
        Calendar cal = Calendar.getInstance();
        cal.setTime(thedate);
        rs.close ();
        stmt.close ();
        return cal;
    }

    private void InsertUserRow( Connection con, Calendar date, int workUnits, int userID, int charityID, int teamID ) throws Exception
    {
        // Java SQL stuff returns 0 if NULL is returned, so we need to make sure we propage the NULL
        // and not a team ID of 0
        String teamStr = "NULL";
        if( teamID != 0 ) {
            teamStr = "" + teamID;
        }

        // TB TEMP! Get some numbers! fucking internet!
        // workUnits = userID + charityID + teamID + 10 + date.get(Calendar.DAY_OF_MONTH);

        // Don't update the charity_id on duplicate... since the charity that a user set for a previous day should not be smushed by what he may have selected afterwards...
        // The same with teams?
        //String cmd = "INSERT INTO main_donateday (user_id, donation_date, work_units, team_id, charity_id) VALUES (" + userID + " , '" + date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH) + "', " + workUnits + ", " + teamStr + ", " + charityID + ") ON DUPLICATE KEY UPDATE charity_id= " + charityID + ", work_units=" + workUnits + ", team_id=" + teamStr + ";";
        String cmd = "INSERT INTO main_donateday (user_id, donation_date, work_units, fake_work_units, team_id, charity_id) VALUES (" + userID + " , '" + date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH) + "', " + workUnits + ",0, " + teamStr + ", " + charityID + ") ON DUPLICATE KEY UPDATE work_units=" + workUnits + ";";
        //System.out.println(cmd);
        Statement stmt = con.createStatement();
        stmt.execute(cmd);
        stmt.close();
    }

    private int GetUserFakeWorkUnits( Connection con, Calendar date, int userID ) throws Exception
    {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery ("SELECT fake_work_units FROM main_donateday WHERE user_id = " + userID + " and donation_date = '" + date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH) + "';");
        rs.next();
        int theamount = rs.getInt("fake_work_units");
        rs.close ();
        stmt.close ();
        return theamount;
    }

    private void UpdateUser( Connection con, JavaPluraConnector pluraConnector, Calendar startDay, Calendar endDay, int userID, int teamID, String pluraIDString, int charityID ) throws Exception
    {        

        Calendar checkDate = (Calendar) startDay.clone();

        while( checkDate.get(Calendar.DAY_OF_MONTH) != endDay.get(Calendar.DAY_OF_MONTH) ||
                           checkDate.get(Calendar.MONTH) != endDay.get(Calendar.MONTH) ||
                           checkDate.get(Calendar.YEAR) != endDay.get(Calendar.YEAR) )
        {
            // The number of WORK UNITS
            int work = 0;

            if( doQueryPlura )
            {
                // Store everything as plura work units for simplicity
                // Since "minutes" is not really accurate either, we may as well use this!
                //System.out.println("Yo");
                work = pluraConnector.getWorkUnitsCompletedByClient( pluraIDString, checkDate );
                //System.out.println("Qux");

            }
            
            InsertUserRow( con, checkDate, work, userID, charityID, teamID );

            int fake_work = GetUserFakeWorkUnits(con, checkDate, userID);

            //System.out.println(sdf.format(checkDate.getTime()) + ": UserPluraID=" + pluraIDString + " --> " + work + " WU, " + fake_work + " FWU" );


            checkDate.add(Calendar.DAY_OF_MONTH, 1);            
        }
    }


    private void UpdateAmounts() throws Exception
    {
        final String affiliateID = "ae9da222-577f-574d-144e-f70686e53e8b";

        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection( JdbcConnectionUrl, JdbcConnectionUsername, JdbcConnectionPassword);

        // Subtract a couple days in case the numbers changed in plura
        Calendar lastDonateDay = GetLastDonateDay(con);
        lastDonateDay.add( Calendar.DAY_OF_MONTH, -2);

        // Don't get values for today since they don't represent a full day...
        // TB TODO - Maybe get the values anyways? For a fast update feeling?
        Calendar lastUpdateDay = Calendar.getInstance();
        
        // Add a day, since the loop stops as soon as it hits this date... But we want to also include the current day!
        // May as well get this stuff as realtime as possible to make it show up in people's accounts faster...?
        lastUpdateDay.add( Calendar.DAY_OF_MONTH, +1 );

        System.out.println ( "Start update date = " + sdf.format(lastDonateDay.getTime()) );
        System.out.println ( "End update date = " + sdf.format(lastUpdateDay.getTime()) );

        System.out.println("Connecting...");
        JavaPluraConnector pluraConnector = null;
        if( doQueryPlura ) {
             pluraConnector = new JavaPluraConnector(affiliateID, .1, .1, "foo", 1);
        }
        System.out.println("Connected!");

        // Iterate through all active users to see if they contributed anything for the days we're checking
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery ("SELECT auth_user.id, auth_user.username, team_id, charity_id FROM main_userprofile, auth_user WHERE main_userprofile.user_id = auth_user.id AND auth_user.is_active = 1;");

        int count = 0;
        while (rs.next ())
        {

            UpdateUser( con, pluraConnector, lastDonateDay, lastUpdateDay, rs.getInt("auth_user.id"), rs.getInt("team_id"), rs.getString("auth_user.username"), rs.getInt("charity_id") );
            
            count++;
        }
        rs.close ();
        stmt.close ();
        System.out.println (count + " users were updated");

    }

    private int GetAnonCharityUserFromCharityPluraID(Connection con, int pluraid) throws Exception
    {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery ("SELECT id from auth_user where username = 'charity" + pluraid + "';");
        rs.next();
        int idVal = rs.getInt ("id");
        //System.out.println ( "Plura id = " + pluraid + " : anonymous user id = " + idVal );
        rs.close ();
        stmt.close ();
        return idVal;
    }
    
    private int GetCharityFromCharityAppID(Connection con, int appID) throws Exception
    {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery ("SELECT id from main_charity where app_id = " + appID);
        rs.next();
        int idVal = rs.getInt ("id");
        //System.out.println ( "App id = " + appID + " : charity id = " + idVal );
        rs.close ();
        stmt.close ();
        return idVal;
    }

    private void InsertAnonymousRow( Connection con, Calendar date, int pluraID, int workUnits ) throws Exception
    {
        int charityID = GetCharityFromCharityAppID(con, pluraID);
        int userID = GetAnonCharityUserFromCharityPluraID(con, pluraID);

        // Team ID 0 will be converted to NULL when inserted.
        InsertUserRow( con, date, workUnits, userID, charityID, 0 );

        /*
        String cmd = "INSERT INTO main_donateday (user_id, donation_date, work_units, team_id, charity_id) VALUES (" + user_id + " , '" + date.get(Calendar.YEAR) + "-" + (date.get(Calendar.MONTH)+1) + "-" + date.get(Calendar.DAY_OF_MONTH) + "', " + work_units + ", NULL, " + charity_id + ") ON DUPLICATE KEY UPDATE charity_id= " + charity_id + ", work_units=" + work_units + ", team_id=NULL;";
        System.out.println(cmd);
        Statement stmt = con.createStatement();
        stmt.execute(cmd);
        stmt.close();
         */
    }

    // TB TODO - Del me once the database is good to go
    private void LoadLocalAmounts(boolean saveToDB) throws Exception
    {
        // TB DB
        Statement stmt;

        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection( JdbcConnectionUrl, JdbcConnectionUsername, JdbcConnectionPassword);

        //stmt = con.createStatement();
        //stmt.execute("insert into table ");
        //stmt.executeQuery ("SELECT id, username, email from auth_user");
        //stmt.close ();

        // Dammit I gotta get the correct anonymous user_id for this charity...
        //GetAnonCharityUserFromCharityPluraID(con, 0);

        //InsertAnonymousRow(con,Calendar.getInstance(), 0, 42);

        //INSERT INTO main_donateday (user_id, donation_date, work_units, team_id, charity_id) VALUES (1, '2010-01-05', 42, NULL, 1)
	//ON DUPLICATE KEY UPDATE charity_id=1, work_units=43, team_id=NULL;


        final String localAmountsFileName = "localAmounts.dat";
        int amounts[][][];
        int numDays;
        Calendar startDate;
        final String outputFolder = "admin_data/";
        int numAffiliates = 1;

        CharityList charityList = new CharityList();
        File f = new File(outputFolder + localAmountsFileName);


        // Start FEB 14 -- valentine's day why not...
        startDate = Calendar.getInstance();
        startDate.set(2009, Calendar.FEBRUARY, 14);

        // Calculate the number of days we need to store
        Calendar today = Calendar.getInstance();
        Calendar cal = (Calendar) startDate.clone();
        int arraySize = 0;
        arraySize = (int)( ((long)( today.getTimeInMillis() - cal.getTimeInMillis() )) / (1000*60*60*24) + 1 );

        if( !f.exists() )
        {
            throw new RuntimeException("Can't find local amounts file");
        }
        else
        {
            try
            {
                FileInputStream fis = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(fis);
                DataInputStream dis = new DataInputStream(bis);

                // It's possible that new charities have been added. In that case, we want to fill in the original charity values,
                // but accomodate for the new charities in the array + when outputting the file again.
                int numCharitiesInThisFile = dis.readInt();
                //int numCharitiesInThisFile = numCharities;
                int day = dis.readInt();
                int month = dis.readInt();
                int year = dis.readInt();
                numDays = dis.readInt();
                startDate = Calendar.getInstance();
                startDate.set(year, month, day);

                amounts = new int[arraySize][numAffiliates][charityList.GetNumCharities()];

                for( int d = 0; d < numDays; d++ )
                {
                    for( int a = 0; a < numAffiliates; a++ )
                    {
                        for( int c = 0; c < numCharitiesInThisFile; c++ )
                        {
                            amounts[d][a][c] = dis.readInt();

                            if( saveToDB ) {


                                Calendar theDay = (Calendar) startDate.clone();
                                theDay.add(Calendar.DAY_OF_MONTH, d);                                
                                // Store as work units, not minutes.
                                int workUnits = amounts[d][a][c] * 4;
                                InsertAnonymousRow( con, theDay, c, workUnits );


                            }

                        }
                    }
                }

                dis.close();
                bis.close();
                fis.close();
            }
            catch( Exception e )
            {
                e.printStackTrace(System.out);
                System.exit(1);
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("Starting admin_updatedatabase");
        boolean transferLocalAmounts = false;

        AdminUpdateDatabase foo = new AdminUpdateDatabase();

        for (String s: args) {
            if( s.contains("-transfer_local_amounts") )
            {
                System.out.println("COMMAND LINE: Transfer local crap to DB...");
                transferLocalAmounts = true;
            }
        }

        try {

            if( transferLocalAmounts ) {
                foo.LoadLocalAmounts(true);
            }
            else {
                foo.UpdateAmounts();
            }
        }
        catch( Exception e ) {
            e.printStackTrace();
            System.exit(1);
        }


        System.out.println("Done! Have a nice day!");
        
        // 100 = success. Ideally 0 should be succes, but 0 is returned if an exception is not caught, and
        // it looks to nasty to wrap everything in main around a fat try block.
        System.exit(100);
    }

}
