/*
 * Copyright 2008-2010 Plura Processing, LP
 */

//package com.pluraprocessing.node.security;
// package com.superdonate.worker;

import java.net.SocketPermission;
import java.security.AccessControlException;
import java.security.AccessController;


/*
 * This SecurityManager class is necessary because DNS resolution makes the default implementation
 * brutally slow.  We only restrict network communications by port, so DNS resolution isn't
 * necessary for us.
 */
public class PluraSecurityManager extends SecurityManager {
	/*
	 * This override allows for the following permissions:
	 * permission java.net.SocketPermission "*:80", "accept";
  	 * permission java.net.SocketPermission "*:8080", "accept";
  	 * permission java.net.SocketPermission "*:443", "accept";
	 */
	public void checkAccept(String host, int port) {
		try {
			SocketPermission perm = new SocketPermission("*:" + port, "accept");
			AccessController.checkPermission(perm);
		}
		catch (AccessControlException e) {
			super.checkAccept("*", port);
		}
	}

	/*
	 * This override allows for the following permissions:
	 * permission java.net.SocketPermission "*:80", "connect";
  	 * permission java.net.SocketPermission "*:8080", "connect";
  	 * permission java.net.SocketPermission "*:443", "connect";
	 */
	public void checkConnect(String host, int port) {
		try {
			SocketPermission perm = new SocketPermission("*:" + port, "connect");
			AccessController.checkPermission(perm);
		}
		catch (AccessControlException e) {
			super.checkConnect("*", port);
		}
	}
}