/*
 * Print to both the console and a log file
 */

package com.superdonate.application;

import java.io.*;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 *
 * @author tbak
 */
public class LogStream extends java.io.PrintStream {

    private PrintStream outConsole = null;
    private PrintStream outFile = null;
    private File logFilePath = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private int maxFileLength = 100000; // 100K - by then it should be apparent what's going on.
    
    LogStream( File outFilePath ) {
        super( System.out );

        outConsole = System.out;
        logFilePath = outFilePath;

        try {

            logFilePath.delete();

            outFile = new PrintStream(new FileOutputStream( outFilePath.getAbsolutePath(), true ));
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void println(String x) {

        outConsole.println(x);

        if( logFilePath.length() < maxFileLength ) {
            Calendar cal = Calendar.getInstance();
            outFile.println( sdf.format(cal.getTime()) + " - " + x);
        }
        
    }
    
    @Override
    public void print(String x) {
        outConsole.print(x);

        if( logFilePath.length() < maxFileLength ) {
            outFile.print(x);
        }
        
    }

    @Override
    public void print(char x) {
        outConsole.print(x);

        if( logFilePath.length() < maxFileLength ) {
            outFile.print(x);
        }

    }

}
