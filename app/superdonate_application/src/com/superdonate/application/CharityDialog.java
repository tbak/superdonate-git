/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * OptionsCharityPanel.java
 *
 * Created on Jun 25, 2009, 10:29:27 AM
 */

package com.superdonate.application;
import javax.swing.border.EmptyBorder;
import com.superdonate.lib_common.Charity;
import java.awt.*;
import javax.swing.*;
import com.superdonate.common.BareBonesBrowserLaunch;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;

/**
 *
 * @author tbak
 */
public class CharityDialog extends javax.swing.JDialog {

    ClientFrame clientFrame;

    // This maps from the index in the list of charities to the actual plura ID that this charity represents.
    int charityListIndexToPluraIDMap[];

    final static boolean useCharityFilter = false;

    /** Creates new form OptionsCharityPanel */
    public CharityDialog( Frame owner ) {
        super(owner);
        
        initComponents();

        if( !useCharityFilter ) {
            jTextFieldFilter.setVisible(false);
            jLabelFilter.setVisible(false);
        }

        jScrollPane2.setBorder(new EmptyBorder(0,0,0,0));

        jListCharity.requestFocusInWindow();
        getRootPane().setDefaultButton(jButtonOK);
        
    }

    private void buildCharityList()
    {
        // ListModel??? This is retarded amounts of code to do simple crap...
        DefaultListModel listModel = new DefaultListModel();

        int listIndex = 0;
        jListCharity.removeAll();
        for( int i = 0; i < clientFrame.pluraConnection.charityList.GetNumCharities(); i++ )
        {
            Charity charity = clientFrame.pluraConnection.charityList.GetCharityFromIndex(i);

            boolean shouldShow = charity.isVisible;
            if( useCharityFilter ) {
                shouldShow = shouldShow && charity.name.toLowerCase().contains( jTextFieldFilter.getText().toLowerCase() );
            }

            if( shouldShow )
            {
                listModel.add( listIndex, charity.name );
                charityListIndexToPluraIDMap[listIndex] = charity.pluraID;
                listIndex++;
            }

        }
        jListCharity.setModel(listModel);

        selectCurrentlyDesiredCharity();
    }

    public void setClientFrame( ClientFrame cf )
    {
        clientFrame = cf;

        charityListIndexToPluraIDMap = new int[clientFrame.pluraConnection.charityList.GetNumCharities()];     

        buildCharityList();

        jButtonCharityLink.setCursor( Cursor.getPredefinedCursor(Cursor.HAND_CURSOR) );        

        // TB - Snagged from http://www.velocityreviews.com/forums/t136479-jtextfield-cant-listen-when-the-text-is-changed.html
        // If I use the textfield events directly then the contents of the field don't contain the currently typed letter!
        jTextFieldFilter.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {                
                buildCharityList();                
            }
            public void removeUpdate(DocumentEvent e) {
                buildCharityList();
            }
            public void insertUpdate(DocumentEvent e) {
                buildCharityList();
            }
        });
    }

    private void updateSingleCharityDisplay()
    {
        if( jListCharity.getSelectedIndex() == -1 ) {
            // Ignore if nothing is selected
            // TB TODO - I guess nothing should be displayed???
            charityLogoPanel1.setBlankImage();
            jTextAreaCharityInfo.setText( null );
            jLabelCharityName.setText("No charity matches this filter");
            jButtonCharityLink.setText(null);
            return;
        }

        //Charity ch = clientFrame.pluraConnection.charityList.GetCharityFromIndex(jListCharity.getSelectedIndex());
        Charity ch = clientFrame.pluraConnection.charityList.GetCharityFromPluraID( charityListIndexToPluraIDMap[jListCharity.getSelectedIndex()] );

        charityLogoPanel1.setCharityImage( ch.pluraID );

        //jTextAreaCharityInfo.setText( "<html><font color=blue size=6>" + ch.name + "</font>" + ch.infoText + "</html>");
        jTextAreaCharityInfo.setText( ch.infoText );
        jTextAreaCharityInfo.setCaretPosition(0);

        //jScrollPane2.setBor
        //System.exit(0);

        jLabelCharityName.setText(ch.name);
        jButtonCharityLink.setText(ch.websiteURL);
    }


    public void selectCurrentlyDesiredCharity()
    {
        // Locate the selected index that has this plura ID
        // jListCharity.setSelectedIndex( clientFrame.pluraConnection.charityList.GetCharityFromPluraID(clientFrame.pluraConnection.getCharityDesired()).index );
        int selectedIndex = 0;
        for( int i = 0; i < jListCharity.getModel().getSize(); i++ )
        {
            if( charityListIndexToPluraIDMap[i] == clientFrame.pluraConnection.getCharityDesired() )
            {
                selectedIndex = i;
            }
        }
        jListCharity.setSelectedIndex(selectedIndex);

        updateSingleCharityDisplay();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListCharity = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        charityLogoPanel1 = new com.superdonate.common.ImagePanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaCharityInfo = new javax.swing.JTextArea();
        jButtonCharityLink = new javax.swing.JButton();
        jLabelCharityName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldFilter = new javax.swing.JTextField();
        jLabelFilter = new javax.swing.JLabel();
        jButtonOK = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SuperDonate - Select Charity");
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(300, 230));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jPanel2.setMinimumSize(new java.awt.Dimension(315, 230));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jSplitPane1.setDividerLocation(160);
        jSplitPane1.setDividerSize(15);
        jSplitPane1.setMinimumSize(new java.awt.Dimension(300, 102));

        jScrollPane1.setBorder(null);
        jScrollPane1.setMinimumSize(new java.awt.Dimension(130, 22));

        jListCharity.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jListCharity.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListCharity.setToolTipText("");
        jListCharity.setMaximumSize(new java.awt.Dimension(150, 80));
        jListCharity.setMinimumSize(new java.awt.Dimension(150, 80));
        jListCharity.setPreferredSize(new java.awt.Dimension(150, 80));
        jListCharity.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListCharityValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListCharity);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(400, 100));
        jPanel1.setPreferredSize(new java.awt.Dimension(570, 300));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        javax.swing.GroupLayout charityLogoPanel1Layout = new javax.swing.GroupLayout(charityLogoPanel1);
        charityLogoPanel1.setLayout(charityLogoPanel1Layout);
        charityLogoPanel1Layout.setHorizontalGroup(
            charityLogoPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        charityLogoPanel1Layout.setVerticalGroup(
            charityLogoPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanel1.add(charityLogoPanel1, gridBagConstraints);

        jTextAreaCharityInfo.setColumns(20);
        jTextAreaCharityInfo.setEditable(false);
        jTextAreaCharityInfo.setFont(new java.awt.Font("Tahoma", 0, 13));
        jTextAreaCharityInfo.setLineWrap(true);
        jTextAreaCharityInfo.setRows(5);
        jTextAreaCharityInfo.setText("Charity info");
        jTextAreaCharityInfo.setWrapStyleWord(true);
        jTextAreaCharityInfo.setAutoscrolls(false);
        jTextAreaCharityInfo.setBorder(new EmptyBorder(5,0,5,5));
        jTextAreaCharityInfo.setFocusable(false);
        jScrollPane2.setViewportView(jTextAreaCharityInfo);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 10, 10);
        jPanel1.add(jScrollPane2, gridBagConstraints);

        jButtonCharityLink.setForeground(new java.awt.Color(0, 0, 255));
        jButtonCharityLink.setText("www.care.org");
        jButtonCharityLink.setBorder(null);
        jButtonCharityLink.setBorderPainted(false);
        jButtonCharityLink.setContentAreaFilled(false);
        jButtonCharityLink.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jButtonCharityLink.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCharityLinkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 10);
        jPanel1.add(jButtonCharityLink, gridBagConstraints);

        jLabelCharityName.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabelCharityName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelCharityName.setText("Charity Name");
        jLabelCharityName.setAlignmentX(0.5F);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanel1.add(jLabelCharityName, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanel1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 0);
        jPanel2.add(jSplitPane1, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel1.setText("Select the charity that you want to donate to from the list on the left. Then click OK.");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(jLabel1, gridBagConstraints);

        jTextFieldFilter.setPreferredSize(new java.awt.Dimension(159, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 0);
        jPanel2.add(jTextFieldFilter, gridBagConstraints);

        jLabelFilter.setText("Filter charity list:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 10, 5);
        jPanel2.add(jLabelFilter, gridBagConstraints);

        jButtonOK.setText("OK");
        jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOKActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        jPanel2.add(jButtonOK, gridBagConstraints);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel2.add(jButtonCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(jPanel2, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jListCharityValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListCharityValueChanged

        // If we're rebuiding the model (filter has changed) it's possible that the state has
        // gone from something selected to nothing selected... in which case we should just chill out.
        if( jListCharity.getSelectedIndex() == -1 ) {
            return;
        }

        updateSingleCharityDisplay();
}//GEN-LAST:event_jListCharityValueChanged

    private void jButtonCharityLinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCharityLinkActionPerformed
        Charity ch = clientFrame.pluraConnection.charityList.GetCharityFromPluraID( charityListIndexToPluraIDMap[jListCharity.getSelectedIndex()] );
        BareBonesBrowserLaunch.openURL("http://" + ch.websiteURL );
    }//GEN-LAST:event_jButtonCharityLinkActionPerformed

    private void jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOKActionPerformed
        if( jListCharity.getSelectedIndex() == -1 ) {
            // Ignore if nothing is selected
            return;
        }

        // TB TODO - Get rid of plura/charity connection!
        // You need your own ID from when you log in...

        int charityID = charityListIndexToPluraIDMap[jListCharity.getSelectedIndex()];
        clientFrame.pluraConnection.setCharityDesired( charityID );
        clientFrame.OnCharityDialogOK();
        dispose();
    }//GEN-LAST:event_jButtonOKActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.superdonate.common.ImagePanel charityLogoPanel1;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonCharityLink;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCharityName;
    private javax.swing.JLabel jLabelFilter;
    private javax.swing.JList jListCharity;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextArea jTextAreaCharityInfo;
    private javax.swing.JTextField jTextFieldFilter;
    // End of variables declaration//GEN-END:variables

}
