/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * RegisterDialog.java
 *
 * Created on Feb 8, 2010, 4:45:34 PM
 */

package com.superdonate.application;

import com.superdonate.common.BareBonesBrowserLaunch;
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author tbak
 */
public class RegisterDialog extends javax.swing.JDialog {

    ClientFrame clientFrame;
    String captchaID = null;

    /** Creates new form RegisterDialog */
    public RegisterDialog( Frame owner ) {
        super(owner);

        initComponents();
        jLabelStatus.setText(" ");
        jLabelErrorUsername.setText("");
        jLabelErrorEmail.setText("");
        jLabelErrorPassword.setText("");
        jLabelErrorPassword2.setText("");
        jLabelErrorCaptcha.setText("");

        jButtonTerms.setCursor( Cursor.getPredefinedCursor(Cursor.HAND_CURSOR) );

        jTextFieldUsername.requestFocusInWindow();
        getRootPane().setDefaultButton(jButtonCreateAccount);


        // The captcha swingworker is throwing a nullpointer exception in Linux.
        // Maybe this will fix it???
        try {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    GetCaptcha();
                }

            });
        } catch(Exception e) { }

        
    }

    private void SetCaptchaImage( String id )
    {
        captchaID = id;
        imagePanelCaptchaTest.setRemoteImage( clientFrame.pluraConnection.serverCommunicator.GetAppServerAddress() + "captcha/image/" + id + "/");
    }

    private void GetCaptcha()
    {
        new SwingWorker<String,Void>() {

            @Override
            protected String doInBackground() throws Exception {

                // Call the blocking servercommunicator command to get the captcha
                return clientFrame.pluraConnection.serverCommunicator.GetCaptchaIDBlocking();
            }

            @Override
            protected void done() {

                try {
                    // TB TODO - Error processing!
                    String id = get();
                    SetCaptchaImage( id );
                }
                catch(Exception e) {
                    e.printStackTrace();
                }

            }

        }.execute();

    }

    private void TryRegister()
    {
        new SwingWorker<String[],Void>() {

            @Override
            protected String[] doInBackground() throws Exception {

                jLabelErrorUsername.setText("");
                jLabelErrorEmail.setText("");
                jLabelErrorPassword.setText("");
                jLabelErrorPassword2.setText("");
                jLabelErrorCaptcha.setText("");

                jLabelStatus.setForeground(Color.blue);
                jLabelStatus.setText("Creating new account...");

                return clientFrame.pluraConnection.serverCommunicator.RegisterNewAccountBlocking( jTextFieldUsername.getText(), jTextFieldEmail.getText(), new String(jTextFieldPassword1.getPassword()), new String(jTextFieldPassword2.getPassword()), captchaID, jTextFieldCaptcha.getText() );
            }

            @Override
            protected void done() {

                try {
                    String[] result = get();
                    jLabelStatus.setText(" ");

                    if( result == null ) {
                        jLabelStatus.setForeground(Color.red);
                        jLabelStatus.setText("Unable to connect to server.");
                    }
                    else {
                        if( result[0].contentEquals("1") ) {

                            // Display success message, wait 1 second, then close dialog.
                            // Log in as well!
                            new SwingWorker<Void,Void>() {
                                @Override
                                protected Void doInBackground() throws Exception {
                                    jLabelStatus.setForeground( new Color(0, 128, 0) );
                                    jLabelStatus.setText("New account created!");
                                    // TB TODO - Technically, the user could have changed someting between the time the stuff was passed in and when the server responds... screw it?
                                    clientFrame.pluraConnection.serverCommunicator.LoginAsync( jTextFieldUsername.getText(), new String(jTextFieldPassword1.getPassword()), clientFrame.pluraConnection.getCharityDesired() );
                                    Thread.sleep(1000);
                                    return null;
                                }
                                @Override
                                protected void done() {
                                    dispose();
                                }
                            }.execute();

                        }
                        else {
                            // Display the errors
                            jLabelErrorUsername.setText(result[1]);
                            jLabelErrorEmail.setText(result[2]);
                            jLabelErrorPassword.setText(result[3]);
                            jLabelErrorPassword2.setText(result[4]);
                            jLabelErrorCaptcha.setText(result[5]);

                            // Need another captcha -- the old one could have been invalidated...
                            jTextFieldCaptcha.setText("");
                            SetCaptchaImage( result[6] );
                            
                        }
                    }
                    
                    // If success, then call the servercommunicator async login function
                    // Then display success, wait a second, and then close the dialog.
                    // Maybe pop up a window?
                    // If failure, update the status label.

                }
                catch(Exception e) {
                    e.printStackTrace();
                    jLabelStatus.setText("Unable to connect to server.");
                    jLabelStatus.setForeground(Color.red);
                }

            }

        }.execute();

    }

    public void setClientFrame( ClientFrame cf )
    {
        clientFrame = cf;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jButtonCreateAccount = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jButtonTerms = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabelErrorPassword2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabelErrorEmail = new javax.swing.JLabel();
        jTextFieldEmail = new javax.swing.JTextField();
        jLabelPassword2 = new javax.swing.JLabel();
        jTextFieldCaptcha = new javax.swing.JTextField();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldUsername = new javax.swing.JTextField();
        jLabelErrorCaptcha = new javax.swing.JLabel();
        jLabelErrorUsername = new javax.swing.JLabel();
        jLabelStatus = new javax.swing.JLabel();
        imagePanelCaptchaTest = new com.superdonate.common.ImagePanel();
        jLabelCaptcha = new javax.swing.JLabel();
        jLabelUsername = new javax.swing.JLabel();
        jLabelPassword1 = new javax.swing.JLabel();
        jLabelErrorPassword = new javax.swing.JLabel();
        jLabelLogo = new javax.swing.JLabel();
        jTextFieldPassword1 = new javax.swing.JPasswordField();
        jTextFieldPassword2 = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SuperDonate - Create New Account");
        setAlwaysOnTop(true);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jButtonCreateAccount.setText("Create Account");
        jButtonCreateAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateAccountActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 10);
        jPanel1.add(jButtonCreateAccount, gridBagConstraints);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel1.add(jButtonCancel, gridBagConstraints);

        jButtonTerms.setForeground(java.awt.Color.blue);
        jButtonTerms.setText("Terms and conditions");
        jButtonTerms.setBorderPainted(false);
        jButtonTerms.setContentAreaFilled(false);
        jButtonTerms.setFocusPainted(false);
        jButtonTerms.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTermsActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel1.add(jButtonTerms, gridBagConstraints);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabelErrorPassword2.setForeground(java.awt.Color.red);
        jLabelErrorPassword2.setText("Error password2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel2.add(jLabelErrorPassword2, gridBagConstraints);

        jLabel4.setText("email:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel2.add(jLabel4, gridBagConstraints);

        jLabelErrorEmail.setForeground(java.awt.Color.red);
        jLabelErrorEmail.setText("Error Email");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel2.add(jLabelErrorEmail, gridBagConstraints);

        jTextFieldEmail.setMinimumSize(new java.awt.Dimension(140, 20));
        jTextFieldEmail.setPreferredSize(new java.awt.Dimension(140, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jTextFieldEmail, gridBagConstraints);

        jLabelPassword2.setText("password (again):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        jPanel2.add(jLabelPassword2, gridBagConstraints);

        jTextFieldCaptcha.setMinimumSize(new java.awt.Dimension(120, 20));
        jTextFieldCaptcha.setPreferredSize(new java.awt.Dimension(120, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jTextFieldCaptcha, gridBagConstraints);

        jTextArea1.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        jTextArea1.setColumns(20);
        jTextArea1.setEditable(false);
        jTextArea1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("Welcome to SuperDonate! Before you can start earning money for charity, you must create a user account. It only takes a few seconds. Once your account has been created, SuperDonate will automatically log you in whenever the program starts.");
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setMaximumSize(new java.awt.Dimension(200, 2147483647));
        jTextArea1.setMinimumSize(new java.awt.Dimension(4, 124));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        jPanel2.add(jTextArea1, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24));
        jLabel2.setText("Create New Account");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jLabel2, gridBagConstraints);

        jTextFieldUsername.setMinimumSize(new java.awt.Dimension(140, 20));
        jTextFieldUsername.setPreferredSize(new java.awt.Dimension(140, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jTextFieldUsername, gridBagConstraints);

        jLabelErrorCaptcha.setForeground(java.awt.Color.red);
        jLabelErrorCaptcha.setText("Error captcha");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 22;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel2.add(jLabelErrorCaptcha, gridBagConstraints);

        jLabelErrorUsername.setForeground(java.awt.Color.red);
        jLabelErrorUsername.setText("Error Username");
        jLabelErrorUsername.setPreferredSize(new java.awt.Dimension(150, 14));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 0);
        jPanel2.add(jLabelErrorUsername, gridBagConstraints);

        jLabelStatus.setForeground(java.awt.Color.blue);
        jLabelStatus.setText("Creating new user...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(20, 0, 0, 0);
        jPanel2.add(jLabelStatus, gridBagConstraints);

        imagePanelCaptchaTest.setMinimumSize(new java.awt.Dimension(120, 50));
        imagePanelCaptchaTest.setPreferredSize(new java.awt.Dimension(120, 50));

        javax.swing.GroupLayout imagePanelCaptchaTestLayout = new javax.swing.GroupLayout(imagePanelCaptchaTest);
        imagePanelCaptchaTest.setLayout(imagePanelCaptchaTestLayout);
        imagePanelCaptchaTestLayout.setHorizontalGroup(
            imagePanelCaptchaTestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );
        imagePanelCaptchaTestLayout.setVerticalGroup(
            imagePanelCaptchaTestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 21;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanel2.add(imagePanelCaptchaTest, gridBagConstraints);

        jLabelCaptcha.setText("Please enter the letters in this box (spam prevention):");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 20;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 0, 0);
        jPanel2.add(jLabelCaptcha, gridBagConstraints);

        jLabelUsername.setText("username:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 5);
        jPanel2.add(jLabelUsername, gridBagConstraints);

        jLabelPassword1.setText("password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 5);
        jPanel2.add(jLabelPassword1, gridBagConstraints);

        jLabelErrorPassword.setForeground(java.awt.Color.red);
        jLabelErrorPassword.setText("Error password");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel2.add(jLabelErrorPassword, gridBagConstraints);

        jLabelLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/icon128.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        jPanel2.add(jLabelLogo, gridBagConstraints);

        jTextFieldPassword1.setMinimumSize(new java.awt.Dimension(140, 20));
        jTextFieldPassword1.setPreferredSize(new java.awt.Dimension(140, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jTextFieldPassword1, gridBagConstraints);

        jTextFieldPassword2.setMinimumSize(new java.awt.Dimension(140, 20));
        jTextFieldPassword2.setPreferredSize(new java.awt.Dimension(140, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel2.add(jTextFieldPassword2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        jPanel1.add(jPanel2, gridBagConstraints);

        getContentPane().add(jPanel1, new java.awt.GridBagConstraints());

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCreateAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateAccountActionPerformed
        this.TryRegister();
}//GEN-LAST:event_jButtonCreateAccountActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.dispose();
}//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonTermsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTermsActionPerformed
        BareBonesBrowserLaunch.openURL( clientFrame.pluraConnection.serverCommunicator.GetWebServerAddress() + "privacy/" );
    }//GEN-LAST:event_jButtonTermsActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterDialog(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.superdonate.common.ImagePanel imagePanelCaptchaTest;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonCreateAccount;
    private javax.swing.JButton jButtonTerms;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelCaptcha;
    private javax.swing.JLabel jLabelErrorCaptcha;
    private javax.swing.JLabel jLabelErrorEmail;
    private javax.swing.JLabel jLabelErrorPassword;
    private javax.swing.JLabel jLabelErrorPassword2;
    private javax.swing.JLabel jLabelErrorUsername;
    private javax.swing.JLabel jLabelLogo;
    private javax.swing.JLabel jLabelPassword1;
    private javax.swing.JLabel jLabelPassword2;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelUsername;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldCaptcha;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JPasswordField jTextFieldPassword1;
    private javax.swing.JPasswordField jTextFieldPassword2;
    private javax.swing.JTextField jTextFieldUsername;
    // End of variables declaration//GEN-END:variables

}
