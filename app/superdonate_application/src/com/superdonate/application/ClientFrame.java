/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

/*
 * ClientFrame.java
 *
 * Created on Dec 9, 2008, 3:11:14 PM
 *
 *
 * This no longer seems to be working? The Plura threads kill themselves? I was using an old policy/manager file. GRR!
 * But I think in the distributed application I'll just not include this...
 * -Djava.security.manager="com.donatebot.application.PluraSecurityManager" -Djava.security.policy="C:\p\ezdonate\donatebot_application\nsis\java.policy"
 *
 */

package com.superdonate.application;

import com.superdonate.common.ClientPanel;
import com.superdonate.common.BareBonesBrowserLaunch;
import com.superdonate.lib_application.DonateBotConnectionApplication;
import com.superdonate.lib_common.CharityList;
import com.superdonate.lib_common.OSCheck;
import com.superdonate.update.UpdateApplication;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Calendar;
import java.io.*;
import java.net.*;
import javax.swing.*;
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;
//import com.lokorin.jupdater.*;
//import com.sun.jna.examples.WindowUtils;
//import java.security.*;
//import javax.crypto.*;



/**
 *
 * @author Administrator
 */
public class ClientFrame extends javax.swing.JFrame implements ActionListener
{
    private static final String applicationName = "SuperDonate";

    public DonateBotConnectionApplication pluraConnection = null;
    
    //static final private DecimalFormat amountDonatedFormat = new DecimalFormat("#,##0.0");
    static final private DecimalFormat timeFormat = new DecimalFormat("00");
    static final private DecimalFormat commaFormat = new DecimalFormat("#,##0");
    private Date lastDataFileSaveTime;

    private final String dataFileName = new String("superdonate.dat");

    static final private String applicationVersion = "1.1.1";

    private final int currentFileVersion = 16;

    // After 1,000 units donated, a popup will come up and an auto redirect to the invite page.
    // This should only ever happen once...
    // This is now set to 5 days running.
    private boolean didPopupInviteMessage = false;
    private final int popupInviteMessageRequiredSeconds = 86400 * 7;
    private final int periodicSaveTime = 1000*60*3;

    // Check every 1 day. If this ever thrashes my server I can make this bigger.
    private final int numMSToWaitForNewApplicationVersionCheck = 1 * 24 * 60 * 60  * 1000;
    // Actually, always perform the test when starting up. That'll ensure that a new version is instanly updated if available.
    // private final int numMSToWaitForNewApplicationVersionCheck = 0;

    UpdateApplication updateApplicationThread = null;
    private Date lastNewApplicationVersionCheck = new Date();
    String programArgs[];
    public boolean enableAutoUpdate = true;
    public boolean wasApplicationPatched = false;

    private TrayIcon trayIcon = null;

    public boolean windowStartMinimized = false;
    public boolean windowShouldFrameBeVisible = true;

    // First time running the program?
    private boolean isRunningProgramFirstTime = false;

    //private OptionsFrame optionsFrame = null;
    //private OptionsFrame2 optionsFrame;
    private AboutDialog aboutDialog = null;
    private CharityDialog charityDialog = null;
    private ResourcesDialog resourcesDialog = null;
    private LoginDialog loginDialog = null;
    private RegisterDialog registerDialog = null;

    private boolean useTestAffiliate = false;
    private int forceCharity = -1;
    
    public final float defaultResourceUsagePct = 0;
    public final float defaultIdleResourceUsagePct = 0.80f;
    public final int defaultIdleWaitSeconds = 60;
    public final boolean defaultIsEnabled = true;
    public final boolean defaultEnableAutoUpdate = true;
    
    // Default Activation time range = disabled, 6pm - 8am
    public boolean defaultActivationTimeEnabled = false;
    public int defaultActivationTimeFromHour = 18;
    public int defaultActivationTimeToHour = 8;


    /** Creates new form ClientFrame */
    public ClientFrame( final String args[] ) {
        
        // Get the application version from the manifest. Nah just get it from here!
        // TB TODO - It's kind of weird that this thread class does this, but oh well...
        //applicationVersion = UpdateApplicationThread.GetImplementationVersion();

       
        //setTitle("Starting...");
        setTitle("SuperDonate - www.superdonate.org");

        // Remember how we started the program in case we need to restart (after an update)
        programArgs = args;

        
        try {
            if( OSCheck.GetOS() == OSCheck.OSType.LINUX ) {
                // The combo box apparently causes gtk errors with the system look and feel...
                // gtk-CRITICAL **: gtk_paint_box: assertion 'style-depth == gdk_drawable_get_depth(window)' failed.
                // Attempting to add a widget with type GtkButton to a GtkComboBoxEntry (need an instance of GtkEntry or of a subclass)
                // UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            }
            else {
                UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
            }
            
        }
        catch (Exception e) { }
        


        
        setIconImage( GetFrameIconImage() );

        initComponents();

        




        // TB TEST - Mac/Linux status bar font too bloody big.
        if( OSCheck.GetOS() != OSCheck.OSType.WINDOWS ) {
            Font oldFont = jLabelStatus.getFont();
            oldFont.getName();
            jLabelStatus.setFont( new Font(oldFont.getName(), oldFont.getStyle(), 10));

            jLabelTotalRunTime.setFont( new Font(oldFont.getName(), oldFont.getStyle(), 10));

            // Also increase the client panel size because otherwise it may not fit...?
            Dimension ms = clientPanel.getMinimumSize();
            ms.width += 20;
            clientPanel.setMinimumSize(ms);

            ms = clientPanel.getPreferredSize();
            ms.width += 20;
            clientPanel.setPreferredSize(ms);

        }



        pack();


        // Position the window in the top right corner
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        setLocation( screenSize.width - this.getSize().width - 20, 0 + 40 );


        

        CheckJavaVersion();

        boolean useLocalWebServer = false;

        for (String s: args) {
            if( s.contains("-l") )
            {
                System.out.println("Command line: Using local web server");
                useLocalWebServer = true;
            }
            else if( s.contains("-m") )
            {
                System.out.println("Command line: Starting minimized");
                windowShouldFrameBeVisible = false;
            }
            else if( s.contains("-testaffiliate") )
            {
                System.out.println("Command line: FORCE TEST AFFILIATE!!!");
                useTestAffiliate = true;
            }
            else if( s.contains("-forceupdate") )
            {
                System.out.println("Command line: FORCE UPDATE!!!");
                UpdateApplication.forceTestUpdate = true;
            }
            else if( s.contains("-charity") )
            {
                // Strip "-charity" from the string
                String valStr = s.substring(8);                

                try {
                    int val = Integer.parseInt(valStr);
                    System.out.println("Command line: Setting starting charity to " + val );
                    forceCharity = val;
                }
                catch( NumberFormatException e ) {
                    System.out.println("Command line '-charity' does not have a number immediately following it.");
                }

            }
            else
            {
                System.out.println("Command line: Ignoring unknown command line argument: " + s);
            }

        }

        // Just show the window right off the bat?
        /*
        if( windowShouldFrameBeVisible ) {
            setExtendedState(Frame.ICONIFIED);
        }
        */
        setVisible(windowShouldFrameBeVisible);

        if( OSCheck.GetOS() == OSCheck.OSType.LINUX )
        {
            // Linux fucks up if the window is never made visible -- the task tray will not restore the window...
            setVisible(true);
        }




        // Initial constructor call. Do this on application start, before the GUI has kicked in, since
        // otherwise there wil be a bit of a delay as the classpath is modified to load the plura code.
        // This is not working. The window pops up with nothing showing...
        pluraConnection = new DonateBotConnectionApplication();        
        pluraConnection.setAffiliateID("ae9da222-577f-574d-144e-f70686e53e8b");

        if( useLocalWebServer ) {
            pluraConnection.serverCommunicator.SetWebServerAddress("http://l.www.superdonate.org/");
            pluraConnection.serverCommunicator.SetAppServerAddress("http://l.app.superdonate.org/");
        }

        if( useTestAffiliate ) {
            pluraConnection.setAffiliateID("66753031-3dc3-4442-314d-2ae3169f32ca");
        }

        pluraConnection.enableVerbose(true);

        //pluraConnection.setCharityDesired(0);
        pluraConnection.setCharityDesiredToRandom();

        isRunningProgramFirstTime = true;

        // Diaper pattern to make sure a bad file doesn't hose the program
        try {
            LoadDataFile();
        }
        catch( Exception e ) {
            e.printStackTrace();
        }

        CheckForNewVersion();
        




        // TB - Moved from the data file load function
        pluraConnection.setEnabled(true);

        StartMainDisplay();

        /*
        if( windowShouldFrameBeVisible ) {
            setExtendedState( windowStartMinimized ? Frame.ICONIFIED : Frame.NORMAL );
        }        
        setVisible(windowShouldFrameBeVisible);
        */


    }


    public void StartMainDisplay() {

        //optionsFrame = new OptionsFrame2();
        //optionsFrame.setClientFrame(this);
        //optionsFrame.setVisible(false);

        getContentPane().setBackground( jToggleButton1.getBackground() );              

        //jLabelFiller.setText("");
        
        clientPanel.SetPluraConnection( pluraConnection );
        clientPanel.SetCharity( pluraConnection.getCharityDesired() );

        SetupTrayIcon();
        AddToSystemTray();

        // Notifications
        if( isRunningProgramFirstTime ) {
        
            // This doesn't display properly on a mac. it just shows an small window with text, not necesarrily close to the icon.
            if( OSCheck.GetOS() != OSCheck.OSType.MAC ) {
                trayIcon.displayMessage("SuperDonate", "You can access the SuperDonate application from this icon.", TrayIcon.MessageType.INFO );
            }
        }
        else if( wasApplicationPatched ) {
            trayIcon.displayMessage("SuperDonate", "SuperDonate has been updated to the latest version.", TrayIcon.MessageType.INFO );
        }
        

        lastDataFileSaveTime = new Date();


        // This will initially draw stuff for us, before the timer kicks in...
        actionPerformed(null);
        Timer timer = new Timer(500,this);
        timer.setRepeats(true);
        timer.start();


        paintAll(getGraphics());
        invalidate();
        repaint();


        if( isRunningProgramFirstTime ) {
            // This thing is kind of annoying???
            //ShowFirstTimeDialog();
        }
    }

    private void CheckForNewVersion()
    {
        // Check if enough days have passed to warrent checking for an update.
        // TB TODO - Would there ever be a reason to check this while the program is running? Or screw it?
        boolean shouldCheckNewVersion = true;
        Date now = new Date();
        long diff = now.getTime() - lastNewApplicationVersionCheck.getTime();
        if( diff < numMSToWaitForNewApplicationVersionCheck )
        {
            shouldCheckNewVersion = false;
            System.out.println("Not checking for new version because enough time has not passed.");
        }

        if( OSCheck.GetOS() == OSCheck.OSType.MAC )
        {
            shouldCheckNewVersion = false;
            System.out.println("Not checking for new version because this is a Mac.");
            return;
        }
        
        if( UpdateApplication.forceTestUpdate == true ) {
            shouldCheckNewVersion = true;
            System.out.println("********** FORCE TEST UPDATE IS TRUE! (UpdateApplication.java)");
        }

        if( !enableAutoUpdate )
        {
            shouldCheckNewVersion = false;
            System.out.println("Not checking for new version because option disabled by user.");
        }

        if( shouldCheckNewVersion )
        {
            updateApplicationThread = new UpdateApplication();
            updateApplicationThread.start();
        }
    }

    static public Image GetFrameIconImage()
    {
        URL url;
        if( OSCheck.GetOS() == OSCheck.OSType.WINDOWS ) {
            //url = getClass().getResource("trayicon_16x16.png");
            url = ClientFrame.class.getResource("trayicon_16x16.png");
        }
        else {
            //url = getClass().getResource("trayicon_48x48.png");
            url = ClientFrame.class.getResource("trayicon_48x48.png");
        }

        Image icon = Toolkit.getDefaultToolkit().getImage(url);
        return icon;
    }


    /**
     * Returns the appropriate working directory for storing application data. The result of this method is platform
     * dependant: On linux, it will return ~/applicationName, on windows, the working directory will be located in the
     * user's application data folder. For Mac OS systems, the working directory will be placed in the proper location
     * in "Library/Application Support".
     * <p/>
     * This method will also make sure that the working directory exists. When invoked, the directory and all required
     * subfolders will be created.
     *
     * @param applicationName Name of the application, used to determine the working directory.
     * @return the appropriate working directory for storing application data.
     *
     * NOTE: Taken from http://forums.sun.com/thread.jspa?threadID=5386564
     *
     * NOTE: This gets the My Documents folder, if that ever is needed:
     *              JFileChooser filechooser = new JFileChooser();
     *              base = filechooser.getFileSystemView().getDefaultDirectory().getAbsolutePath();
     */
    static public File GetDataFileFolder()
    {
        final String userHome = System.getProperty("user.home", ".");
        final File workingDirectory;
        switch( OSCheck.GetOS() ) {
        case LINUX:
            workingDirectory = new File(userHome, '.' + applicationName );
            break;

        case WINDOWS:
            final String applicationData = System.getenv("APPDATA");
            if (applicationData != null) {
                workingDirectory = new File(applicationData, applicationName );
            }
            else {
                workingDirectory = new File(userHome, applicationName );
            }
            break;

        case MAC:
            workingDirectory = new File(userHome, "Library/Application Support/" + applicationName);
            break;
            
        default:
            return new File(".");
        }

        return workingDirectory;
    }

    private File GetDataFile()
    {
        return new File( GetDataFileFolder(), dataFileName );
    }

    public void ShowFirstTimeDialog()
    {
        String iconName = "icon128.png";
        URL imgURL = getClass().getResource(iconName);
        ImageIcon icon = new ImageIcon(imgURL, "foo");

        String title = "Welcome to SuperDonate!";
        String message = "<html>" +
                "<font color=blue size=6>Welcome to SuperDonate!</font><br/><br/>" +
                "SuperDonate automatically generates money for charity while it's running.<br/><br/>" +
                "To use SuperDonate, click \"options\" to select the charity you want to donate to.<br/>Then minimize the program and you're done!<br/><br/>" +
                "<font color=red>Keep SuperDonate running to donate to charity. <br/>It automatically adjusts your CPU usage so you won't notice that it's on.</font><br/><br/>" +
                "<font color=green size=4>Thank you for using SuperDonate.</font><br/><br/>" +
                "</html>";

        // Saved file does not exist -- assume this is the first time the user has run the program...
        JOptionPane.showMessageDialog(this,
            message,
            title,
            JOptionPane.INFORMATION_MESSAGE,
            icon);

        // Last param = icon???
    }

    private void AddTrayMenuItem( String str, ActionListener listener, boolean isEnabled, PopupMenu popup )
    {
        MenuItem menuItem = new MenuItem(str);
        menuItem.addActionListener(listener);
        menuItem.setEnabled(isEnabled);
        popup.add(menuItem);
    }

    private void SetupTrayIcon()
    {

        final String strRestore = "Restore SuperDonate";
        final String strWeb = "SuperDonate Website";
        final String strExit = "Exit";        

        if( SystemTray.isSupported() )
        {
                     
            // http://weblogs.java.net/blog/ixmal/archive/2006/05/using_jpopupmen.html

            // create a action listener to listen for default action executed on the tray icon
            ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // execute default action of the application
                    // ...
                    //System.out.println(e.toString());

                    String actionCmd = e.getActionCommand();
                    if( actionCmd == null || actionCmd.equals(strRestore) )
                    {

                        if( OSCheck.GetOS() == OSCheck.OSType.LINUX )
                        {
                            // All of this insanity will work around a linux bug where minimized+hidden windows will not restore.
                            // If I just do setVisible(true) + setExtendedState(JFrame.NORMAL) it still remains minimized and will not restore.
                            // TB - I don't even think this does anything anymore???
                            /*
                            setVisible(true);
                            ClientFrame.this.setExtendedState(JFrame.ICONIFIED);
                            setVisible(false);
                            setVisible(true);
                            ClientFrame.this.setExtendedState(JFrame.NORMAL);
                            setVisible(false);
                            setVisible(true);
                            */
                            showWindow();
                        }
                        else
                        {
                            showWindow();
                        }
                                                
                    }
                    else if( actionCmd.equals(strWeb))
                    {
                        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() );
                    }
                    else if( actionCmd.equals(strExit) )
                    {
                        ExitApplication();
                    }
                                        
                }
            };

            // create a popup menu
            PopupMenu popup = new PopupMenu();

            AddTrayMenuItem( "Please keep SuperDonate", null, false, popup );
            AddTrayMenuItem( "running to donate to charity!", null, false, popup );
            popup.addSeparator();

            AddTrayMenuItem( strRestore, listener, true, popup );
            AddTrayMenuItem( strWeb, listener, true, popup );

            popup.addSeparator();

            AddTrayMenuItem( strExit, listener, true, popup );

            Image image = GetTrayIconImage();

            // ... add other items
            // construct a TrayIcon
            trayIcon = new TrayIcon(image, "SuperDonate", popup);
            trayIcon.setImageAutoSize(true);
                       
            // set the TrayIcon properties
            trayIcon.addActionListener(listener);



            UpdateTrayIcon();
        
        }
    }

    private Image GetTrayIconImage()
    {
        String iconName = "";

        Dimension iconSize = SystemTray.getSystemTray().getTrayIconSize();

        //System.out.println("Tray icon size:" + iconSize );
        //System.out.println("W=" + iconSize.width + " H=" + iconSize.height );

        // Note: Linux does not display transparent system tray icons properly (the transparent pixels come out opaque gray)
        // So the art must fill the entire icon area.
        if( iconSize.width == 16 && iconSize.height == 16 )
        {
            // Windows XP
            iconName = "trayicon_16x16.png";

            if( !pluraConnection.isEnabled() ) {
                iconName = "trayicon_16x16_disabled.png";
            }
        }
        else if( iconSize.width == 32 && iconSize.height == 32 )
        {
            iconName = "trayicon_32x32.png";
        }
        else if( iconSize.width == 24 && iconSize.height == 24 )
        {
            // TB TODO - Ubuntu: Why is there an ugly gray backgrond where there should be transparency???
            iconName = "trayicon_24x24.png";
        }
        else
        {
            iconName = "trayicon_48x48.png";
        }

        URL url = getClass().getResource(iconName);
        Image image = Toolkit.getDefaultToolkit().getImage(url);

        return image;
    }

    // The tray icon can change if SuperDonate is disabled (use gray icon)
    private void UpdateTrayIcon()
    {
        trayIcon.setImage( GetTrayIconImage() );
    }

    
    private void CheckJavaVersion()
    {
        if( !ClientPanel.IsJavaVersionOK() )
        {
            JOptionPane.showMessageDialog(
                null,
                "Please install the latest version of Java in order to use SuperDonate.\n",
                "SuperDonate",
                JOptionPane.INFORMATION_MESSAGE);

            BareBonesBrowserLaunch.openURL("http://www.java.com");

            System.exit(1);
        }
        
    }

    public void OnResourcesDialogOK()
    {
        UpdateTrayIcon();
        SaveDataFile();
    }

    public void OnCharityDialogOK()
    {
        clientPanel.SetCharity( pluraConnection.getCharityDesired() );

        // The call to pluraconnectino.setcharitydesired will take care of server communicatins.
        //serverCommunicator.SetCharity(pluraConnection.getCharityDesired());

        SaveDataFile();
    }

    private void LoadDataFile()
    {
        // Default values (if config file not found)        
        pluraConnection.setResourceUsageNotIdle( defaultResourceUsagePct );
        pluraConnection.setResourceUsageIdle( defaultIdleResourceUsagePct );
        pluraConnection.setIdleWaitSeconds( defaultIdleWaitSeconds );        
        pluraConnection.enableIdleDetection(true);

        pluraConnection.setActivationTime( defaultActivationTimeEnabled, defaultActivationTimeFromHour * 60, defaultActivationTimeToHour * 60 );

        String username = "";
        String passwordEnc = "";
        int userPluraID = 0;

        // Load saved all-time donated stats (if it exists)
        File f = GetDataFile();
        if( f.exists() )
        {

            isRunningProgramFirstTime = false;

            try
            {
                FileInputStream fis = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(fis);
                DataInputStream dis = new DataInputStream(bis);
                // If the file version is not the same, abandon loading the file... Sorry pal!
                int version = dis.readInt();
                if( version == currentFileVersion )
                {

                    didPopupInviteMessage = dis.readBoolean();                    

                    lastNewApplicationVersionCheck.setTime( dis.readLong() );

                    enableAutoUpdate = dis.readBoolean();

                    String oldProgramVersion = dis.readUTF();
                    
                    if( FullApplicationVersionString().compareTo(oldProgramVersion) != 0 ) {
                        // The application version is not the same as what it was before.
                        // Thus, we can assume it was patched to a newer version last time we exited.
                        System.out.println("Application was patched when exiting last time.");
                        wasApplicationPatched = true;
                    }

                    pluraConnection.loadData(dis);
                    
                }
                dis.close();
                bis.close();
                fis.close();
            }
            catch( Exception e ) { e.printStackTrace(System.out); }
        }

        // Command line charity set
        if( forceCharity != -1 )
        {
            if( pluraConnection.charityList.GetCharityFromPluraID(forceCharity) != null ) {
                pluraConnection.setCharityDesired( forceCharity );
            }
        }


    }

    private void SaveDataFile()
    {
        File folder = GetDataFileFolder();
        if( !folder.exists() ) {
            folder.mkdirs();
        }
                
        File file = GetDataFile();
        try
        {
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            DataOutputStream dos = new DataOutputStream(bos);
            dos.writeInt(currentFileVersion);


            dos.writeBoolean(didPopupInviteMessage);           

            dos.writeLong( lastNewApplicationVersionCheck.getTime() );

            dos.writeBoolean( enableAutoUpdate );

            dos.writeUTF( FullApplicationVersionString() );

            pluraConnection.saveData(dos);

            dos.close();
            bos.close();
            fos.close();
        }
        catch( Exception e ) { e.printStackTrace(System.out); }

        lastDataFileSaveTime = new Date();
    }

    public void actionPerformed(ActionEvent evt)
    {               
        int totalSecs = pluraConnection.getTotalSecondsRunning();
        int secs = totalSecs % 60;
        int mins = (totalSecs / 60)%60;
        int hours = (totalSecs / 3600)%24;
        int days = (totalSecs / 86400);
        StringBuilder prettyBuilder = new StringBuilder();
        //prettyBuilder.append("<html>Total Time: ");
        prettyBuilder.append("<html>");
        prettyBuilder.append(days);
        prettyBuilder.append("d : ");
        prettyBuilder.append(timeFormat.format(hours));
        prettyBuilder.append("h : ");
        prettyBuilder.append(timeFormat.format(mins));
        prettyBuilder.append("m : ");
        prettyBuilder.append(timeFormat.format(secs));
        prettyBuilder.append("s");
        prettyBuilder.append("</html>");
        jLabelTotalRunTime.setText( prettyBuilder.toString() );
     

        // Should the popup message with the invite message come up?
        /*
        if( !didPopupInviteMessage && totalSecs >= popupInviteMessageRequiredSeconds )
        {
            didPopupInviteMessage = true;

            String message = "You have now run SuperDonate for 7 days. \nPlease consider inviting your friends to try out SuperDonate.";

            JOptionPane.showMessageDialog(
                null,
                message,
                "SuperDonate",
                JOptionPane.INFORMATION_MESSAGE);

            BareBonesBrowserLaunch.openURL("http://www.superdonate.org/invite.php");
        }
        */

        // TB TODO - This is retarded. It should just set this one time when something changes!
        SetStatusBarText(pluraConnection.serverCommunicator.statusBarText, pluraConnection.serverCommunicator.statusBarCode);
        boolean enable = true;
        if( !pluraConnection.serverCommunicator.isLoggedIn ) {
            enable = false;
        }
        jMenuItemEditAccount.setEnabled(enable);
        jMenuItemStatsCharity.setEnabled(enable);
        jMenuItemStatsPersonal.setEnabled(enable);
        jMenuItemStatsTeam.setEnabled(enable);
        jMenuItemLogout.setEnabled(enable);
        jMenuItemLogin.setEnabled(!enable);


        // Save to disk after a certain amount of time has passed
        // We'll also save to disk when the user exists the application
        // Unfortunately this isn't a platform independent way of detecting when the user shuts down his PC,
        // so this will have to do.
        //
        if( (new Date()).getTime() - lastDataFileSaveTime.getTime() > periodicSaveTime )
        {
            SaveDataFile();
        }

    }

    public void AddToSystemTray()
    {
        if( SystemTray.isSupported() )
        {
            // Minimize to a system tray icon instead of the task bar
            SystemTray tray = SystemTray.getSystemTray();
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
            
        }
    }

    public void RemoveFromSystemTray()
    {
        if( SystemTray.isSupported() )
        {
            // Minimize to a system tray icon instead of the task bar
            SystemTray tray = SystemTray.getSystemTray();
            tray.remove(trayIcon);
        }
    }

    private void ExitApplication()
    {
        System.out.println("Exiting application.");

        if( loginDialog != null ) loginDialog.dispose();
        if( registerDialog != null ) registerDialog.dispose();
        if( charityDialog != null ) charityDialog.dispose();
        if( resourcesDialog != null ) resourcesDialog.dispose();
        if( aboutDialog != null ) aboutDialog.dispose();


        pluraConnection.applicationIsClosing();

        if( SystemTray.isSupported() )
        {
            RemoveFromSystemTray();
        }

        SaveDataFile();
        this.setVisible(false);
        pluraConnection.setEnabled(false);
        this.dispose();

        // Patch the program!
        if( enableAutoUpdate && updateApplicationThread != null ) {
            updateApplicationThread.startUpdateCopyProcess();
        }


        // Sleep for 1.5 secs to ensure that the java process closes
        // it's not the end of the world if it doesn't (it'll kill itself eventually)
        // but this is cleaner.
        // This sleep also ensures that the update copy process does not crash the VM.
        // I think calling System.exit() right after creating a process is doing bad things.
        try { Thread.sleep( 1500 * 1 ); }
        catch (Exception e) { }

        System.exit(0);
    }

    static public String FullApplicationVersionString()
    {
        return applicationVersion;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        clientPanel = new com.superdonate.common.ClientPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabelStatus = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabelTotalRunTime = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemLogin = new javax.swing.JMenuItem();
        jMenuItemEditAccount = new javax.swing.JMenuItem();
        jMenuItemCreateAccount = new javax.swing.JMenuItem();
        jMenuItemLogout = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        jMenuItemCharity = new javax.swing.JMenuItem();
        jMenuItemOptions = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItemMinimize = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItemStatsPersonal = new javax.swing.JMenuItem();
        jMenuItemStatsTeam = new javax.swing.JMenuItem();
        jMenuItemStatsCharity = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItemHelp = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        jMenuItemHomePage = new javax.swing.JMenuItem();
        jMenuItemPromote = new javax.swing.JMenuItem();
        jMenuItemAbout = new javax.swing.JMenuItem();

        jToggleButton1.setText("jToggleButton1");

        jToggleButton2.setText("jToggleButton2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setForeground(java.awt.Color.white);
        setMinimumSize(new java.awt.Dimension(100, 50));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowDeiconified(java.awt.event.WindowEvent evt) {
                formWindowDeiconified(evt);
            }
            public void windowIconified(java.awt.event.WindowEvent evt) {
                formWindowIconified(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        clientPanel.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(160, 160, 160)));
        clientPanel.setMinimumSize(new java.awt.Dimension(450, 60));
        clientPanel.setPreferredSize(new java.awt.Dimension(511, 60));
        clientPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clientPanelMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(clientPanel, gridBagConstraints);

        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(255, 255, 255)));
        jPanel1.setMinimumSize(new java.awt.Dimension(204, 16));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabelStatus.setText("Initializing...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 0);
        jPanel1.add(jLabelStatus, gridBagConstraints);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/trayicon_16x16.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanel1.add(jLabel2, gridBagConstraints);

        jLabelTotalRunTime.setText("5:4:3:2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 5);
        jPanel1.add(jLabelTotalRunTime, gridBagConstraints);

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanel1.add(jSeparator4, gridBagConstraints);

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(2, 5, 2, 5);
        jPanel1.add(jSeparator5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints);

        jMenu1.setMnemonic('f');
        jMenu1.setText("File");

        jMenuItemLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_key.png"))); // NOI18N
        jMenuItemLogin.setMnemonic('l');
        jMenuItemLogin.setText("Login");
        jMenuItemLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLoginActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemLogin);

        jMenuItemEditAccount.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_page_edit.png"))); // NOI18N
        jMenuItemEditAccount.setMnemonic('e');
        jMenuItemEditAccount.setText("Edit Account");
        jMenuItemEditAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEditAccountActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemEditAccount);

        jMenuItemCreateAccount.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_user_add.png"))); // NOI18N
        jMenuItemCreateAccount.setMnemonic('c');
        jMenuItemCreateAccount.setText("Create New Account");
        jMenuItemCreateAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCreateAccountActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemCreateAccount);

        jMenuItemLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_key_delete.png"))); // NOI18N
        jMenuItemLogout.setText("Logout");
        jMenuItemLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLogoutActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemLogout);
        jMenu1.add(jSeparator3);

        jMenuItemCharity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_world.png"))); // NOI18N
        jMenuItemCharity.setMnemonic('s');
        jMenuItemCharity.setText("Select Charity");
        jMenuItemCharity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCharityActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemCharity);

        jMenuItemOptions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_wrench.png"))); // NOI18N
        jMenuItemOptions.setMnemonic('o');
        jMenuItemOptions.setText("Options");
        jMenuItemOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOptionsActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemOptions);
        jMenu1.add(jSeparator1);

        jMenuItemMinimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_arrow_down.png"))); // NOI18N
        jMenuItemMinimize.setMnemonic('m');
        jMenuItemMinimize.setText("Minimize");
        jMenuItemMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemMinimizeActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemMinimize);

        jMenuBar1.add(jMenu1);

        jMenu4.setMnemonic('s');
        jMenu4.setText("Stats");

        jMenuItemStatsPersonal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_user.png"))); // NOI18N
        jMenuItemStatsPersonal.setMnemonic('p');
        jMenuItemStatsPersonal.setText("Personal");
        jMenuItemStatsPersonal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemStatsPersonalActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemStatsPersonal);

        jMenuItemStatsTeam.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_group.png"))); // NOI18N
        jMenuItemStatsTeam.setMnemonic('t');
        jMenuItemStatsTeam.setText("Team");
        jMenuItemStatsTeam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemStatsTeamActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemStatsTeam);

        jMenuItemStatsCharity.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_world_link.png"))); // NOI18N
        jMenuItemStatsCharity.setMnemonic('c');
        jMenuItemStatsCharity.setText("Charity");
        jMenuItemStatsCharity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemStatsCharityActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemStatsCharity);

        jMenuBar1.add(jMenu4);

        jMenu2.setMnemonic('h');
        jMenu2.setText("Help");

        jMenuItemHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_help.png"))); // NOI18N
        jMenuItemHelp.setMnemonic('h');
        jMenuItemHelp.setText("Help");
        jMenuItemHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHelpActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemHelp);
        jMenu2.add(jSeparator2);

        jMenuItemHomePage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_page_world.png"))); // NOI18N
        jMenuItemHomePage.setMnemonic('s');
        jMenuItemHomePage.setText("SuperDonate Website");
        jMenuItemHomePage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHomePageActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemHomePage);

        jMenuItemPromote.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/menuicon_heart.png"))); // NOI18N
        jMenuItemPromote.setMnemonic('p');
        jMenuItemPromote.setText("Promote SuperDonate");
        jMenuItemPromote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPromoteActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemPromote);

        jMenuItemAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/application/trayicon_16x16.png"))); // NOI18N
        jMenuItemAbout.setMnemonic('a');
        jMenuItemAbout.setText("About");
        jMenuItemAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAboutActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemAbout);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        if( SystemTray.isSupported() )
        {            
            setVisible(false);
        }
        else
        {
            ExitApplication();
        }
    }//GEN-LAST:event_formWindowClosing

    private void showWindow()
    {
        if( !isVisible() ) {

            // Update the client panel so that is says the right then when
            // the screen is visible. (otherwise it'll flash something old since
            // the display does not update while the frame is invisible).
            // Hmmm this isn't working... The panel still shows incorrect info for a split second.
            clientPanel.UpdateDisplay(true);
         
            setVisible(true);
            
            setExtendedState(JFrame.NORMAL);
            

        }

        // This will bring the window to the front -- it's already visible + restored
        toFront();

    }

    private void hideWindow()
    {
        if( isVisible() ) {
            setVisible(false);            
        }
    }


    private void formWindowIconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowIconified
        // TB TEST - Linux
        // If linux, we can't mess with the iconified button because it'll screw up the
        // window state and the window will never restore itself again. This is apparently
        // fixed in Java 7.
        if( OSCheck.GetOS() == OSCheck.OSType.LINUX ) {
            return;
        }

        hideWindow();
    }//GEN-LAST:event_formWindowIconified


    private void formWindowDeiconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeiconified
        // TB TEST - Linux
        // If linux, we can't mess with the iconified button because it'll screw up the
        // window state and the window will never restore itself again. This is apparently
        // fixed in Java 7.
        if( OSCheck.GetOS() == OSCheck.OSType.LINUX ) {
            return;
        }

        showWindow();
    }//GEN-LAST:event_formWindowDeiconified

    private void clientPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientPanelMouseClicked
        // People may think that clicking on the donate panel will let you change the charity... so do it!
        // If you're not logged in, I guess it should take you to the login dialog instead.
        if( pluraConnection.serverCommunicator.isLoggedIn ) {
            jMenuItemCharityActionPerformed(null);
        }
        else {
            jMenuItemLoginActionPerformed(null);
        }
    }//GEN-LAST:event_clientPanelMouseClicked

    private void prepareDialog(final JDialog dlg)
    {
        dlg.setIconImage( ClientFrame.GetFrameIconImage() );

        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        dlg.setLocation( screenSize.width/2 - dlg.getSize().width/2, screenSize.height/2 - dlg.getSize().height/2 );
        dlg.pack();

        // Make escape cancel the dialog
        JRootPane dlgRootPane = dlg.getRootPane();
        KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        Action actionListener = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                dlg.setVisible(false);
            }
        };
        InputMap inputMap = dlgRootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(stroke, "ESCAPE");
        dlgRootPane.getActionMap().put("ESCAPE", actionListener);


        dlg.setVisible(true);
    }

    private void jMenuItemAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAboutActionPerformed

        if( aboutDialog == null || !aboutDialog.isVisible() ) {
            aboutDialog = new AboutDialog(this);
        }
        prepareDialog(aboutDialog);

    }//GEN-LAST:event_jMenuItemAboutActionPerformed

    private void jMenuItemCharityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCharityActionPerformed
        if( charityDialog == null || !charityDialog.isVisible() ) {
            charityDialog = new CharityDialog(this);
            charityDialog.setClientFrame(this);            
        }
        prepareDialog(charityDialog);
    }//GEN-LAST:event_jMenuItemCharityActionPerformed

    private void jMenuItemOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOptionsActionPerformed
        
        if( resourcesDialog == null || !resourcesDialog.isVisible() ) {
            resourcesDialog = new ResourcesDialog(this);
            resourcesDialog.setClientFrame(this);
        }
        prepareDialog(resourcesDialog);
        
    }//GEN-LAST:event_jMenuItemOptionsActionPerformed

    private void jMenuItemLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLoginActionPerformed
        if( loginDialog == null || !loginDialog.isVisible() ) {
            loginDialog = new LoginDialog(this);
            loginDialog.setClientFrame(this);
        }
        prepareDialog(loginDialog);
    }//GEN-LAST:event_jMenuItemLoginActionPerformed

    private void jMenuItemEditAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEditAccountActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "login");
    }//GEN-LAST:event_jMenuItemEditAccountActionPerformed

    private void jMenuItemHomePageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHomePageActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() );
    }//GEN-LAST:event_jMenuItemHomePageActionPerformed

    // This needs to be a public method since the register dialog needs to be able to
    // open the login dialog.
    public void ShowRegisterDialog() {
        // Also redo the dialog if it was previously closed since that'll create a new captcha...
        if( registerDialog == null || !registerDialog.isVisible() ) {
            registerDialog = new RegisterDialog(this);
            registerDialog.setClientFrame(this);
        }
        prepareDialog(registerDialog);
    }

    private void jMenuItemCreateAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCreateAccountActionPerformed
        ShowRegisterDialog();
    }//GEN-LAST:event_jMenuItemCreateAccountActionPerformed

    private void jMenuItemStatsPersonalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemStatsPersonalActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "login");
    }//GEN-LAST:event_jMenuItemStatsPersonalActionPerformed

    private void jMenuItemStatsTeamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemStatsTeamActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "team/info/" + pluraConnection.serverCommunicator.teamID );
    }//GEN-LAST:event_jMenuItemStatsTeamActionPerformed

    private void jMenuItemStatsCharityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemStatsCharityActionPerformed
        // We must use the info_app_id function since we are passing in the app_id, not the charity's foreign key.
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "charity/info_app_id/" + pluraConnection.getCharityDesired() );
    }//GEN-LAST:event_jMenuItemStatsCharityActionPerformed

    private void jMenuItemHelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHelpActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "faq" );
    }//GEN-LAST:event_jMenuItemHelpActionPerformed

    private void jMenuItemPromoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPromoteActionPerformed
        BareBonesBrowserLaunch.openURL( pluraConnection.serverCommunicator.GetWebServerAddress() + "promote" );
    }//GEN-LAST:event_jMenuItemPromoteActionPerformed

    private void jMenuItemMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMinimizeActionPerformed
        hideWindow();
    }//GEN-LAST:event_jMenuItemMinimizeActionPerformed

    private void jMenuItemLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLogoutActionPerformed
        pluraConnection.serverCommunicator.Logout();
    }//GEN-LAST:event_jMenuItemLogoutActionPerformed

    public void SetStatusBarText(String t, int icon)
    {               
        String iconName = null;
        if( icon == 0 ) {
            jLabelStatus.setForeground(Color.black);
            iconName = "/com/superdonate/application/trayicon_16x16.png";
        }
        else if(icon == 1) {
            jLabelStatus.setForeground( new Color(156, 0, 0) );
            
            // TB TODO - This icon is ugly!
            //iconName = "/com/superdonate/application/trayicon_16x16_disabled.png";
            iconName = "/com/superdonate/application/trayicon_16x16.png";
        }
        jLabelStatus.setText(t);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(iconName)));
        
    }
    /**
    * @param args the command line arguments
    */
    public static void main(final String args[]) {

        // Mac can't output to the same folder since it'll make the user's Application directory look ugly
        // So put it in the app support folder
        File outFile = new File( "superdonate_log_out.txt" );
        File errFile = new File( "superdonate_log_err.txt" );

        if( OSCheck.GetOS() == OSCheck.OSType.MAC ) {            
            GetDataFileFolder().mkdirs();

            outFile = new File( GetDataFileFolder(), "superdonate_log_out.txt" );
            errFile = new File( GetDataFileFolder(), "superdonate_log_err.txt" );
        }

        LogStream logStreamOut = new LogStream( outFile );
        LogStream logStreamErr = new LogStream( errFile );

        System.setOut(logStreamOut);
        System.setErr(logStreamErr);

        System.out.println("Starting SuperDonate desktop application " + ClientFrame.FullApplicationVersionString() );

        // Test start process
        //testStartProcess();







        /*
        String regex = "[\\d]+\\.[\\d]+.*";
        System.out.println("Test 1: " + "1.0".matches(regex) );
        System.out.println("Test 2: " + "5.1".matches(regex) );
        System.out.println("Test 3: " + "<htm5.1".matches(regex) );
        System.out.println("Test 4: " + "a".matches(regex) );
        System.out.println("Test 5: " + "1.0.1".matches(regex) );
        */

        try {
        //java.awt.EventQueue.invokeAndWait(new Runnable() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                /*
                //System.setProperty("sun.java2d.noddraw", "true");
                //JFrame.setDefaultLookAndFeelDecorated(true);
                //try {
                //    UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
                //} catch( Exception e ) { }
                Window foo = new JFrame("yo");
                foo.setLayout(new FlowLayout());
		foo.add(new JButton("test"));
		foo.add(new JCheckBox("test"));
		foo.add(new JRadioButton("test"));
		foo.add(new JProgressBar(0, 100));
                foo.setSize( new Dimension(400,300) );
                foo.setLocationRelativeTo(null);
                //foo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                foo.setVisible(true);
                //com.sun.jna.examples.WindowUtils.
                com.sun.awt.AWTUtilities.setWindowOpacity(foo, 0.5f);
                */                

                // This quickly starts up and makes a minimized window that says Starting...
                final ClientFrame clientFrame = new ClientFrame(args);
            }

        });
        } catch(Exception e) { }


    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.superdonate.common.ClientPanel clientPanel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelStatus;
    private javax.swing.JLabel jLabelTotalRunTime;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItemAbout;
    private javax.swing.JMenuItem jMenuItemCharity;
    private javax.swing.JMenuItem jMenuItemCreateAccount;
    private javax.swing.JMenuItem jMenuItemEditAccount;
    private javax.swing.JMenuItem jMenuItemHelp;
    private javax.swing.JMenuItem jMenuItemHomePage;
    private javax.swing.JMenuItem jMenuItemLogin;
    private javax.swing.JMenuItem jMenuItemLogout;
    private javax.swing.JMenuItem jMenuItemMinimize;
    private javax.swing.JMenuItem jMenuItemOptions;
    private javax.swing.JMenuItem jMenuItemPromote;
    private javax.swing.JMenuItem jMenuItemStatsCharity;
    private javax.swing.JMenuItem jMenuItemStatsPersonal;
    private javax.swing.JMenuItem jMenuItemStatsTeam;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    // End of variables declaration//GEN-END:variables

}
