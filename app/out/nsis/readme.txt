﻿SUPERDONATE 1.1
http://www.superdonate.org


1. Introduction

Thank you for downloading SuperDonate! SuperDonate is the easiest way to donate to charity -- for free!

Many computers sit idle for hours or days at a time. SuperDonate lets you share this unused processing power with organizations that need to solve complicated problems, such as analyzing the galaxies or performing mathematical calculations. In exchange for a little CPU processing power, you will donate to the charity of your choice.

You will never notice that SuperDonate is running since it only activates when you are away from your computer.

SuperDonate is 100% safe. The distributed computing system runs in a secure Java sandbox that has no access to your files.

This application contains absolutely no spyware and no adware. You will not be harassed with nasty popup ads when using this application. 


2.1 Windows Installation
To install SuperDonate, simply run the superdonate_install.exe file. Once installed, the application will automatically start every time you turn on your computer. You'll know it's running if you see the green SuperDonate leaf icon in your system tray.

 
2.2 Mac Installation
UnZip superdonate_mac_install.zip (double click on it). Then run the superdonate_mac_install package located inside the newly created superdonate_mac_install folder. This will install SuperDonate to your /Applications folder.

Note: You must have Java SE6 or later installed to use SuperDonate. You can do this by going to the apple menu, selecting "Software Update..." and making sure you have the latest OSX updates.

Note: An Intel Mac with OSX 10.5.2 (Leopard) or higher is required to run SuperDonate on a Mac.

2.3 Linux Installation
Extract superdonate_linux.tar.gz to any directory. Then run the run_superdonate_linux.sh shell script to start SuperDonate. Note: You must have Java 1.6 or later installed to use SuperDonate on Linux. To install Java, go to http://www.java.com.


3. Using SuperDonate
If the SuperDonate window is not visible, double-click on the system tray icon to restore it.

Before you can start earning money for charity, you must create a SuperDonate account. You can do this by choosing "Create New Account" from the File menu. If you already have a SuperDonate account, you can choose "Login" from the File menu. Once you are logged in,
the program will remember your information and will automatically log you in the next time the program is run.

To select the charity you want to donate to, choose "Select Charity" from the File menu.

Once you have configured everything, you're done! You can now minimize SuperDonate and your donations to your selected charity will start.

The SuperDonate program may use internet ports 80, 8080 and 443 when solving computational problems. These are the standard ports used by basic web sites. No other ports will be opened by the program.

4. Special Thanks

Mark James for his Silk icon set, http://www.famfamfam.com/lab/icons/silk/

---

For questions or comments, please visit the SuperDonate homepage at http://www.superdonate.org