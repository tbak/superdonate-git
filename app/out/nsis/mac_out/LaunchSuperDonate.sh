#!/bin/sh

# The following files are bundled:
#
# 'lib'
# 'superdonate_application.jar'
#
#

java -Xdock:name="SuperDonate" -cp superdonate_application.jar:./lib/superdonate_update.jar:lib/superdonate_worker.jar com.superdonate.application.ClientFrame