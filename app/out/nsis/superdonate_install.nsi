;NSIS Modern User Interface
;Welcome/Finish Page Example Script
;Written by Joost Verburg
;Modified by Tom Bak for SuperDonate

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"


;--------------------------------
;General

  ;Name and file
  Name "SuperDonate"
  OutFile "superdonate_install.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\SuperDonate"

  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\SuperDonate" ""

  icon "icon.ico"
  SetCompressor /SOLID lzma

  ;Request application privileges for Windows Vista
  ; RequestExecutionLevel user
  ; TB - These scripts are better that just setting RequestExecutionLevel???

  !define MULTIUSER_EXECUTIONLEVEL Admin
  ;!define MULTIUSER_NOUNINSTALL ;Uncomment if no uninstaller is created
  !include MultiUser.nsh

  Function .onInit
    !insertmacro MULTIUSER_INIT
  FunctionEnd

  Function un.onInit
    !insertmacro MULTIUSER_UNINIT
  FunctionEnd
  
  Function .onInstSuccess
	ExecShell "Open" "http://www.superdonate.org/promote"
  FunctionEnd

;--------------------------------
;Interface Settings

  !define MUI_HEADERIMAGE
  ;!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\nsis.bmp" ; optional
  !define MUI_HEADERIMAGE_BITMAP "nsis_header.bmp" ; optional
  !define MUI_ABORTWARNING
  
  !define MUI_ICON "icon.ico"

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  ; !insertmacro MUI_PAGE_LICENSE "${NSISDIR}\Docs\Modern UI\License.txt"
  !insertmacro MUI_PAGE_LICENSE "license.txt"
  ; !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES

# These indented statements modify settings for MUI_PAGE_FINISH
    !define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
;    !define MUI_FINISHPAGE_RUN_NOTCHECKED
    !define MUI_FINISHPAGE_RUN_TEXT "Start SuperDonate now"
    !define MUI_FINISHPAGE_RUN_FUNCTION "StartSuperDonate"
    !define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
    !define MUI_FINISHPAGE_SHOWREADME $INSTDIR\readme.txt
  !insertmacro MUI_PAGE_FINISH


  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH


;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections


;--------------------------------
;Descriptions

  ;Language strings
;  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
;  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
;    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
;  !insertmacro MUI_FUNCTION_DESCRIPTION_END


!macro DeleteOldFiles

	Delete "$INSTDIR\Uninstall.exe"

	Delete "$INSTDIR\*.*"
	RMDir /r "$INSTDIR"

	# Remove the link from the start menu
	delete "$SMPROGRAMS\SuperDonate\Uninstall.lnk"
	delete "$SMPROGRAMS\SuperDonate\SuperDonate.lnk"
	delete "$SMPROGRAMS\SuperDonate\ReadMe.lnk"
	delete "$SMSTARTUP\SuperDonate.lnk"
	RMDir  "$SMPROGRAMS\SuperDonate"

	DeleteRegKey /ifempty HKCU "Software\SuperDonate"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate"

!macroend

!macro DeleteUserAppData

	# TB - Ensure it gets deleted from current user and all user folders...?
	SetShellVarContext current
	Delete "$APPDATA\SuperDonate\*.*"
	RMDir  "$APPDATA\SuperDonate"
	SetShellVarContext all
	Delete "$APPDATA\SuperDonate\*.*"
	RMDir  "$APPDATA\SuperDonate"

!macroend


# start default section
Section

	# Kill old process
	# Otherwise the install will fail when copying over the java DLL files
        # Using this plugin: http://nsis.sourceforge.net/FCT_plug-in
        fct::fct /WTP "SuperDonate" /TIMEOUT 2000
	fct::fct /WTP "DonateBot" /TIMEOUT 2000

	# Is there a better way to get rid of old stuff before installing new program?
	# If the files aren't deleted, the install fails when trying to write over a read only file.
	# This happens if the user didn't uninstall the previous version...
	!insertmacro DeleteOldFiles

	# set the installation directory as the destination for the following actions
	setOutPath $INSTDIR
 
	file superdonate.exe
	file superdonate_application.jar
	file readme.txt
	file java.policy
	file "icon.ico"
	file "license.txt"
	file /r lib

	# The launch4j-tmp folder is deleted in makensis.bat, so we don't need to worry about it.
	# BUT MAKE SURE SUPERDONATE ISN'T RUNNING OR THE FILES CAN'T BE DELETED!!!
	# Apparently some guy still got that folder on his machine, so make sure SuperDonate isn't running!
	# setOutPath $INSTDIR\jre6
	# file /r jre6\bin
	# file /r jre6\lib
	file /r jre6


    # create the uninstaller
    writeUninstaller "$INSTDIR\uninstall.exe"
 
    ; Shortcuts should be for all users
    SetShellVarContext all

    # create a shortcut named "new shortcut" in the start menu programs directory
    # point the new shortcut at the program uninstaller
	CreateDirectory "$SMPROGRAMS\SuperDonate"
	createShortCut 	"$SMPROGRAMS\SuperDonate\SuperDonate.lnk" "$INSTDIR\superdonate.exe"
	createShortCut 	"$SMPROGRAMS\SuperDonate\ReadMe.lnk" "$INSTDIR\readme.txt"
    createShortCut 	"$SMPROGRAMS\SuperDonate\Uninstall.lnk" "$INSTDIR\uninstall.exe"
    CreateShortCut  "$SMSTARTUP\SuperDonate.lnk" "$INSTDIR\superdonate.exe" "-m"


  ;Store installation folder
WriteRegStr HKCU "Software\SuperDonate" "" $INSTDIR

; Windows add/remove programs
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "DisplayName" "SuperDonate"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "UninstallString" "$INSTDIR\uninstall.exe"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "Publisher" "Superdonate, Inc."
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "URLInfoAbout" "http://www.superdonate.org"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "NoModify" 1
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "NoRepair" 1
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SuperDonate" \
                 "DisplayIcon" "$INSTDIR\icon.ico"


SectionEnd


Function StartSuperDonate
  ExecShell "" "$INSTDIR\superdonate.exe"
FunctionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

	!insertmacro DeleteOldFiles
	!insertmacro DeleteUserAppData

	ExecShell "Open" "http://www.superdonate.org/survey_uninstall"

SectionEnd
