/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.superdonate.worker;

import com.pluraprocessing.node.affiliate.desktop.JavaPluraConnector;

/**
 *
 * @author tbak
 */
public class SimpleTest {

    public static void main(String[] args) {

        JavaPluraConnector foo = null;
        try {
            String affiliate = "ae9da222-577f-574d-144e-f70686e53e8b";
            double cpu = 0.5;
            double bandwidth = 0.5;

            // These work
            // String pluraClientString = "AAAAAAA";
            // String pluraClientString = "Jenesta2";
            // String pluraClientString = "tbak";
            // String pluraClientString = "Jenes";
            // String pluraClientString = "jenesta";
            // String pluraClientString = "Aaaaaaa";
            // String pluraClientString = "JENESTA";
            // String pluraClientString = "Jenestq";
            // String pluraClientString = "Flea";

            // These don't work:                        
            // String pluraClientString = "Jenest";
            // String pluraClientString = "Zenesta";
            // String pluraClientString = "Zenest2";
            // String pluraClientString = "eeliottheking";
            String pluraClientString = "Jenesta";

            int numCores = 8;

            System.out.println("Connecting...");
            foo = new JavaPluraConnector( affiliate, cpu, bandwidth, pluraClientString, numCores );
            System.out.println("Connected...");
            foo.start();
            System.out.println("Started...");
            Thread.sleep(1000 * 360);
            System.out.println("Done...");

            System.exit(0);
        }
        catch( Exception e ) {
            e.printStackTrace(System.err);
        }

    }

}
