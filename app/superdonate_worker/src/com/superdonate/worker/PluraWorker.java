/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.superdonate.worker;

import com.pluraprocessing.node.affiliate.desktop.JavaPluraConnector;
import java.io.*;
import java.util.Calendar;

/**
 *
 * @author tbak
 */
public class PluraWorker {

    static private boolean ultraVerbose = false;

    public static boolean startPlura( String pluraClientString, double cpu, double bandwidth, String affiliate )
    {
        JavaPluraConnector foo = null;
        try {
            int numCores = 8;
            foo = new JavaPluraConnector( affiliate, cpu, bandwidth, pluraClientString, numCores );
            foo.start();
            return true;
        }
        catch( Exception e ) {
            e.printStackTrace(System.err);
        }

        return false;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int heartlessTimeLimit = 1000 * 10;

        // Worker message: init

        // This sends 1 (message)
        System.out.print( (char)1 );
        
        // This sends '1' (note usefull -- it'll print out when the client receives it)
        // System.out.print( 1 );

        if( args.length != 4 ) {
            System.err.println("Please run SuperDonate using superdonate.exe or donatebot_application.jar");
            System.exit(0);
        }

        // Check params
        // 0=PLURACLIENT 1=CPU_PCT 2=BANDWIDTH_PCT 3=AFFILIATE
        String pluraClientString = args[0];
        double cpu = Double.parseDouble(args[1]);
        double bandwidth = Double.parseDouble(args[2]);
        String affiliate = args[3];

        if( ultraVerbose ) {
            System.err.println("Plura Client = " + pluraClientString);
            System.err.println("cpu = " + cpu);
            System.err.println("bandwidth = " + bandwidth);
            System.err.println("affiliate = " + affiliate);
        }


        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);


        long lastClientHeartbeatTime = Calendar.getInstance().getTime().getTime();
        boolean isConnected = false;

        boolean shouldDie = false;
        while( !shouldDie ) {

            // Loop forever until something screws up or we are killed by the frame

            if( !isConnected ) {
                isConnected = startPlura( pluraClientString, cpu, bandwidth, affiliate );
                if( isConnected ) {
                    if( ultraVerbose ) System.err.println("WORKER CONNECT");
                    
                    // Worker message: connect
                    System.out.print( (char)2);
                }
                else {
                    if( ultraVerbose ) System.err.println("WORKER ERROR CONNECT");

                    // Worker message: ERROR connect
                    System.out.print( (char)3);
                }
            }

            if( ultraVerbose ) System.err.println("WORKER HEARTBEAT");

            // Worker message: heartbeat
            System.out.print( (char)0 );


            // Check for the client hearbeat
            try {
                while( br.ready() ) {
                    int msg = br.read();

                    if( ultraVerbose ) System.err.println("Client says: " + msg );

                    if( msg == 0 ) {
                        // Client heatbeat
                        if( ultraVerbose ) System.err.println("Worker got a client heartbeat!");
                        lastClientHeartbeatTime = Calendar.getInstance().getTime().getTime();
                    }
                    else if( msg == 1 ) {
                        // Kill message
                        if( ultraVerbose ) System.err.println("Worker got a kill message!");
                        System.exit(0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace(System.err);
            }

            // Check if we haven't gotten the client heartbeat for a while and should die.
            long heartlessTime = Calendar.getInstance().getTime().getTime() - lastClientHeartbeatTime;
            if( heartlessTime > heartlessTimeLimit ) {
                // It's been too long
                System.exit(0);
            }

            try { Thread.sleep( 1000 ); }
            catch( Exception e ) { }

        }


    }

}
