/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

/*
 * CharityLogoPanel.java
 *
 * Created on May 10, 2009, 4:53:02 PM
 */

package com.superdonate.common;

import java.awt.image.BufferedImage;
import javax.swing.SwingWorker;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.util.Date;
import com.superdonate.lib_common.CharityList;
import java.text.DecimalFormat;

/**
 *
 * @author COM
 */
public class ImagePanel extends javax.swing.JPanel {

    private Image imageData;
    private BufferedImage bimg;

    private Date lastRemoteImageFetchTime = new Date(0);
    private String lastRemoteImageLocation = null;

    // Wait 5 minute before reloading a remote image again (from setRemoteImage())
    // Otherwise just use the same image...
    final private int remoteImageRefetchWaitTime = 1000 * 60 * 5;

    public static final int imageWidth = 650;
    public static final int imageHeight = 300;
    public static final String startUrl = "http://chart.apis.google.com/chart?";

    public boolean isScaled = false;

    /** Creates new form CharityLogoPanel */
    public ImagePanel() {
        initComponents();
        
        //imageData = GetImageFromFilesystem("client_data/charitylogo_0.png", this);
        imageData = null;
    }

    // Should the image be scaled up and down to fit the size of this panel?
    // The ratio will stay consistent, so it might not fill all the space.
    public void setScaled( boolean s )
    {
        isScaled = s;
    }

    public void setBlankImage()
    {
        imageData = null;
        repaint();
    }

    public void setJARImage( String name, Class resourceClass )
    {
        imageData = GetImageFromJAR(name, this, resourceClass);
        repaint();
    }

    public void setCharityImage( int charity )
    {
        // TB - We now put the charity data in the JAR
        // This way we can use JUpdater to update the JAR and charity info automatically for us!
        //String name = "client_data/charitylogo_" + charity + ".png";
        //imageData = GetImageFromFilesystem( name, this );

        //String name = "charitylogo_" + charity + ".png";
        String name = "charity_data/charitylogo_" + charity + ".png";
        imageData = GetImageFromJAR(name, this, CharityList.class);

        repaint();
    }

    public void setSpinnerImage()
    {
        imageData = GetImageFromJAR("active.gif", this, ClientPanel.class);
        repaint();
    }

    // Set the image using a URL location
    // The image is loaded in the background and automatically set
    public void setRemoteImage( final String location )
    {

        // For the personal stats, the URL changes every second.
        // I think I can just check the time for remote images since I am consistent about what to display
        // Actually, the single charity view chart image panel now changes the data, so I need to check if the URL has changed... Ah well...
        if( location.equals(lastRemoteImageLocation) ) {
            // Can we ignore this request? We already have the image loaded, so if it hasn't been too
            // long, don't bother loading it again.
            Date now = new Date();
            if( now.getTime() < lastRemoteImageFetchTime.getTime() + remoteImageRefetchWaitTime ) {
                return;
            }
        }
        
        // Set loading image
        setSpinnerImage();

        final Component caller = this;
        SwingWorker<Image,Image> loadimages = new SwingWorker<Image,Image>() {

            @Override
            protected Image doInBackground() throws Exception {

                Image img = null;

                try {
                    URL url = new URL( location );

                    //imageData = Toolkit.getDefaultToolkit().createImage(url);
                    img = Toolkit.getDefaultToolkit().createImage(url);

                    MediaTracker tracker = new MediaTracker(caller);
                    tracker.addImage(img, 0);
                    tracker.waitForID(0);
                }
                catch( Exception e ) {
                    System.err.println(e.toString());
                }

                return img;
            }

            @Override
            protected void done() {
                try
                {
                    imageData = get();

                    // Prevent a remote image load of the same image for a period of time.
                    lastRemoteImageFetchTime = new Date();
                    lastRemoteImageLocation = location;
                }
                catch( Exception e ) {
                    System.err.println(e.toString());
                }
                
            }
            
        };
        loadimages.execute();

    }

    // This one stalls until the image is loaded...
    private Image GetImageFromURL( String location, Component caller )
    {
        Image img = null;

        try {
            URL url = new URL( location );

            //imageData = Toolkit.getDefaultToolkit().createImage(url);
            img = Toolkit.getDefaultToolkit().createImage(url);

            MediaTracker tracker = new MediaTracker(caller);
            tracker.addImage(img, 0);
            tracker.waitForID(0);
        }
        catch( Exception e )
        {
            System.out.println("GetImageFromURL exception: " + e.toString() );
        }

        return img;
    }

    // Located in the JAR file itself
    public Image GetImageFromJAR( String name, Component caller, Class resourceClass )
    {
        Image img = null;

        try {
            // URL url = ClientPanel.class.getResource(name);
            URL url = resourceClass.getResource(name);

            img = caller.getToolkit().getImage(url);

            //MediaTracker tracker = new MediaTracker(this);
            MediaTracker tracker = new MediaTracker(caller);
            tracker.addImage(img, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
            System.out.println("GetImageFromJAR exception: " + e.toString() );
        }
        return img;
    }

    // This gets the image from the filesystem
    /*
    public Image GetImageFromFilesystem( String name, Component caller )
    {
        Image img = null;

        try {

            File foo = new File(name);
            if( foo.exists() )
            {
                img = Toolkit.getDefaultToolkit().getImage(name);
            }
            else
            {
                img = Toolkit.getDefaultToolkit().getImage("client_data/charitylogo_default.png");
            }

            MediaTracker tracker = new MediaTracker(caller);
            tracker.addImage(img, 0);
            tracker.waitForID(0);
        } catch (Exception e) {
            System.out.println("GetImageFromFilesystem exception: " + e.toString() );
        }

        return img;
    }
    */

    public Graphics2D createGraphics2D(int w, int h) {
        Graphics2D g2 = null;
        if (bimg == null || bimg.getWidth() != w || bimg.getHeight() != h) {
            bimg = (BufferedImage) createImage(w, h);
        }
        g2 = bimg.createGraphics();
        g2.setBackground(getBackground());
        g2.setRenderingHint(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
        g2.clearRect(0, 0, w, h);
        return g2;
    }

    @Override
    public void paint(Graphics g) {

        if( imageData == null ) {
            super.paint(g);
        }

        if( imageData == null || imageData.getWidth(this) <= 5 || imageData.getHeight(this) <= 5 )
        {
            return;
        }

        super.paint(g);

        if( !isScaled ) {
            Graphics2D g2 = createGraphics2D( imageData.getWidth(this), imageData.getHeight(this) );
            g2.drawImage(imageData, 0, 0, imageData.getWidth(this), imageData.getHeight(this), this);

            g2.dispose();

            // This centers the drawing horizontally and vertically
            g.drawImage(bimg, (getWidth() - imageData.getWidth(this))/2, (getHeight() - imageData.getHeight(this))/2, this);
            
        }
        else {
            
            float xExpand = (float)getWidth() / (float)imageData.getWidth(this);
            float yExpand = (float)getHeight() / (float)imageData.getHeight(this);
            
            int newX, newY;

            float expandAmount;
            if( xExpand > yExpand ) {
                expandAmount = yExpand;
            }
            else {
                expandAmount = xExpand;
            }

            newX = (int) ( imageData.getWidth(this) * expandAmount );
            newY = (int) ( imageData.getHeight(this) * expandAmount );


            Graphics2D g2 = createGraphics2D( newX, newY );
            g2.drawImage(imageData, 0, 0, newX, newY, this);      
            g2.dispose();

            g.drawImage(bimg, 0, 0, this);
        }

    }


    // overrides imageUpdate to control the animated gif's animation
    @Override
    public boolean imageUpdate(Image img, int infoflags,
                int x, int y, int width, int height)
    {

        if (isShowing() && (infoflags & ALLBITS) != 0)
            repaint();
        if (isShowing() && (infoflags & FRAMEBITS) != 0)
            repaint();
        return isShowing();

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(150, 50));
        setMinimumSize(new java.awt.Dimension(150, 50));
        setPreferredSize(new java.awt.Dimension(150, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
