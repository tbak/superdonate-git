/*
Copyright 2009, SuperDonate.  All rights reserved.
*/

/*
 * ClientPanel.java
 *
 * Created on Dec 8, 2005, 12:49:47 PM
 */

package com.superdonate.common;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.event.*;
import javax.swing.Timer;
import javax.swing.SwingUtilities;
import com.superdonate.lib_common.DonateBotConnection;
import com.superdonate.lib_common.InternetChecker;

/**
 *
 * @author Administrator
 */
public class ClientPanel extends javax.swing.JPanel implements ImageObserver, ActionListener {

    public int selectedCharity = -1;    
    private DonateBotConnection pluraConnection = null;
        
    public ClientPanel() {
        initComponents();

        jLabelInfo.setText("Starting SuperDonate...");

        setBackground(Color.white);

        selectedCharity = -1;        

        Timer timer = new Timer(500, this);
        timer.setRepeats(true);
        timer.start();
    }

    public void SetPluraConnection( DonateBotConnection c )
    {
        pluraConnection = c;
        //progressAnimPanel1.SetPluraConnection( c );
    }            

    public void actionPerformed(ActionEvent evt) {
        UpdateDisplay(false);
    }

    public void SetCharity(int c)
    {       

        if( c != selectedCharity )
        {            
            selectedCharity = c;            

            charityLogoPanel.setCharityImage(c);

            // This will quickly change the display so that clicking the mouse will be responsive.
            // Otherwise we'd have to wait for 2 sleeping threads to wake up before drawing.
            UpdateDisplay(false);
        }
        
    }

    private void SetLeafIcon( boolean isEverythingOK )
    {
        if( isEverythingOK ) {
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/common/trayicon_32x48.png")));
        }
        else {
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/common/trayicon_error_32x48.png")));
        }
    }


    public void UpdateDisplay(boolean forceUpdate)
    {
        // Do not update anything if the display is not visible.
        // Unless we are forcing it (display about to become visible, contents should be updated beforehand)
        // TB TODO - This is causing the display to be incorrect for a split second
        // when the window is restored.
        if( !forceUpdate && (!isVisible() || !SwingUtilities.getRoot(this).isVisible()) )
            return;

        DonateBotConnection.State state = pluraConnection.getConnectionState();

        /*
        if( state == DonateBotConnection.State.RUNNING && selectedCharity != pluraConnection.getCharityConnected() )
            state = DonateBotConnection.State.SWITCHING_CHARITY;
        */

        if( !pluraConnection.isEnabled() )
        {
            jLabelInfo.setText( "<html><font size=5 color=black>SuperDonate is disabled</font><br/><font size=3 color=green>Click OPTIONS to enable again.</font></html>");
            SetLeafIcon( false );
            return;
        }

        if( pluraConnection.serverCommunicator.isLoggedIn == false && pluraConnection.serverCommunicator.isLogInError == false && !pluraConnection.serverCommunicator.GetUsername().contentEquals("") ) {
            jLabelInfo.setText( "<html><font size=5>Connecting...</font></html>" );
            SetLeafIcon( true );
            return;
        }

        if( pluraConnection.serverCommunicator.isLoggedIn == false && pluraConnection.serverCommunicator.GetUsername().contentEquals("") ) {
            jLabelInfo.setText( "<html><font size=5>Please login to your account</font><br/><font size=4 color=green>Select \"login\" from the menu.</font></html>" );
            SetLeafIcon( false );
            return;
        }

        if( pluraConnection.serverCommunicator.isLogInError ) {

            if( pluraConnection.serverCommunicator.isUsernamePasswordError ) {
                jLabelInfo.setText( "<html><font size=5>Error connecting to your account<br/><font size=4>Please verify your login information</font></html>" );
            }
            else {
                jLabelInfo.setText( "<html><font size=5>Unable to connect<br/><font size=4>Retrying...</font></html>" );
            }
            SetLeafIcon(false);
            return;
        }

        // Check if having internet trouble
        // Assume that if internet checker is not done, but we were unable to connect to plura, it's probably an internet problem
        // and the internet checker has stalled when trying to open a website (even though the $#%*(#@( timer should prevent this...)
        // Since the internet checker is now disabled, its status is always UNCHECKED... so if we can't connect to
        // plura, then the best we can do is say that we can't connect to plura...
        /*
        if( pluraConnection.getInternetStatus() == InternetChecker.TestStatus.NOT_CONNECTED ||
            (pluraConnection.getInternetStatus() == InternetChecker.TestStatus.UNCHECKED && state == DonateBotConnection.State.ERROR_CONNECT_PLURA) )
        {
        */
        if( pluraConnection.getInternetStatus() == InternetChecker.TestStatus.NOT_CONNECTED ) {
            jLabelInfo.setText( "<html><font size=4>Unable to connect to the internet</font><br/><font size=3 color=green>SuperDonate will retry automatically</font></html>" );
            SetLeafIcon( false );
            return;
        }

        switch( state )
        {
        case INITIALIZING:

            // Why not just display the happy donate message right off the bat, and then worry about errors
            // when they occur? This "starting/initializing" message does nothing for the user except confuse/worry him.

        case SWITCHING_CHARITY:
        case RUNNING:

            String charityName = pluraConnection.charityList.GetCharityFromPluraID(pluraConnection.getCharityDesired()).name;
            jLabelInfo.setText( "<html><font size=5 color=black>You are now earning money for</font><br/><font size=5 color=green>" + charityName + "</font></html>");
            SetLeafIcon( true );
            break;

        case ERROR_CONNECT_PLURA:
            jLabelInfo.setText( "<html><font size=4>SuperDonate is down for maintentance</font><br/><font size=3 color=green>SuperDonate will reconnect automatically</font></html>" );
            SetLeafIcon( false );
            break;

        case ERROR:
            jLabelInfo.setText( "Error: Generic." );
            SetLeafIcon( false );
            break;

        }
        
    }

    static public boolean IsJavaVersionOK()
    {
        final boolean forceJavaFail = false;

        String version = System.getProperty("java.version");
        System.out.println("Detected Java version: " + version);

        String split[] = null;
        split = version.split("\\.");
        int major = Integer.valueOf(split[0]);
        int minor = Integer.valueOf(split[1]);
        //System.out.println("Major version: " + major );
        //System.out.println("Minor version: " + minor );

        if( forceJavaFail )
        {
            return false;
        }

        if( major > 1 || (major == 1 && minor >= 6) )
        {
            return true;
        }

        return false;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        charityLogoPanel = new com.superdonate.common.ImagePanel();
        jLabelInfo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(900, 60));
        setMinimumSize(new java.awt.Dimension(420, 52));
        setPreferredSize(new java.awt.Dimension(510, 60));
        setLayout(new java.awt.GridBagLayout());

        javax.swing.GroupLayout charityLogoPanelLayout = new javax.swing.GroupLayout(charityLogoPanel);
        charityLogoPanel.setLayout(charityLogoPanelLayout);
        charityLogoPanelLayout.setHorizontalGroup(
            charityLogoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );
        charityLogoPanelLayout.setVerticalGroup(
            charityLogoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 10);
        add(charityLogoPanel, gridBagConstraints);

        jLabelInfo.setFont(new java.awt.Font("Times New Roman", 0, 18));
        jLabelInfo.setText("<html>You are now donfoo</html>");
        jLabelInfo.setPreferredSize(new java.awt.Dimension(290, 44));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 3, 0);
        add(jLabelInfo, gridBagConstraints);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/superdonate/common/trayicon_32x48.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        add(jLabel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.superdonate.common.ImagePanel charityLogoPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelInfo;
    // End of variables declaration//GEN-END:variables

}
