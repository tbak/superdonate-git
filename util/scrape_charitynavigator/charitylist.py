from BeautifulSoup import BeautifulSoup
import urllib2,sys
import re

regex_orgid = re.compile('orgid\=([0-9]+)')

charity_list_filename = 'C:\p\ezdonate\util\scrape_charitynavigator\charitylist.txt'


def get_charity_url(from_rec):
    return 'http://www.charitynavigator.org/index.cfm?bay=search.results&overallrtg=4&FromRec=%d' % from_rec

def get_page_html(from_rec, use_local=False):
    if use_local:
        print 'Using test file'
        in_file = open('C:/p/ezdonate/util/scrape_charitynavigator/testsummary.html', 'r')
        html = in_file.read()
        in_file.close()
    else:
        url = get_charity_url(from_rec)
        print 'Scraping page: ' + url    
        html = urllib2.urlopen(url).read()
        
    return html

def process_page(from_rec, use_local):

    output = ''

    print '*' * 20
    
    html = get_page_html(from_rec, use_local)
    
    # print html
    
    # soup = BeautifulSoup(html)
    
    for match in re.finditer(regex_orgid, html):
        print 'ID: ' + match.group(1)
        output += match.group(1) + '\n'
       
    return output
    
             
if __name__ == '__main__':
    
    print 'Four star charity list scraper!'
        
    use_local = False
    
    output = ''
    
    if use_local:
        output += process_page(0,True)
    else:
        # Loop it!
        # This goes in increments of 25
        # pages = range(0,31)
        pages = [ a * 25 for a in range(32,71) ]                
        for page in pages:
            output += process_page(page,False)
    
    out_file = open('C:\p\ezdonate\util\scrape_charitynavigator\charitylist.txt', 'w')        
    out_file.write(output)
    out_file.close()   
            
    print 'Done! Now bugger off!'