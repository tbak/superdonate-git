Hello***NAME***,

I've created a computer program that can earn money for your organization, ***CHARITY***.

It's a small program that silently runs in the background to generate money for charity. It doesn't use advertisements or spyware and doesn't affect the user's computer performance or privacy in any way. Every person that runs the program will earn up to $25/year for your organization. I believe that in the near future all computers will be running software like this.

I am simply looking for new charities to add to the program.

Please let me know if you're interested! I would be more than happy to explain in greater detail how it works.

Regards,

Tom Bak
tom@superdonate.org
(415)287-0484
