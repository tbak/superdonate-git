from BeautifulSoup import BeautifulSoup
import urllib2,sys
import re
import time
import random

regex_email = re.compile('"mailto:([\w\s@\.]+)"')
regex_tel = re.compile('tel:\s([\(\)\w\s@\.\-]+)')
regex_website = re.compile('a\shref\=\"http://([\w\.]+)')

charity_list_filename = 'charitylist_1star.txt'
output_csv_filename = 'output_1star.csv'

def get_revenue(soup):
    # Get the revenue    
    rating_div = soup.findAll('div', attrs={"class" : "rating"})[2]
    revenue_td = rating_div.findAll('td', align='right')[0]
    revenue = revenue_td.string[1:]
    revenue = [ r for r in revenue if r != ',' ]
    revenue = int( ''.join(revenue) )
    
    print 'Revenue: ' + str(revenue)
    return revenue

def get_charity_name(soup):
    charity_element = soup.find('h1', attrs={"class" : "charityname"})
    
    if charity_element == None:
        print 'There is no charity with this ID!'
        return None   
    
    name = charity_element.string.replace(',',' ')
    
    print 'Charity name is: ' + name
    
    return name


def is_html_valid(html):   
    
    soup = BeautifulSoup(html)
    
    if get_charity_name(soup) == None:
        print 'Could not find name'
        return False
    
    print 'This ID is OK!'
    
    return True

def get_charity_url(org_id):
    return 'http://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=%d' % org_id

def get_page_html(org_id, use_local=False):
    if use_local:
        print 'Using test file'
        in_file = open('C:/p/ezdonate/util/scrape_charitynavigator/testpage.html', 'r')
        html = in_file.read()
        in_file.close()
    else:
        url = get_charity_url(org_id)
        print 'Scraping page: ' + url    
        html = urllib2.urlopen(url).read()
        
    return html

def is_org_id_valid(org_id, use_local=False):
        
    html = get_page_html(org_id, use_local)        
    return is_html_valid(html)            
    
def process_address(org_id, use_local):

    output = ''

    print '*' * 20
    
    html = get_page_html(org_id, use_local)

    if not is_html_valid(html):
        print 'Bad charity'
        return ''
    
    soup = BeautifulSoup(html)
    
    charity_element = soup.find('h1', attrs={"class" : "charityname"})
    
    if charity_element == None:
        print 'There is no charity with this ID!'
        return ''
    
    # Output the org id just in case we need it for something
    # I guess this could be copy-pasted to the charity list file...?
    output += str(org_id) + ','
        
    output += get_charity_name(soup) + ','    
    
               
    rating_div = soup.findAll('div', attrs={"class" : "rating"})[0]
    rating_text = str(rating_div)

    foo = str(rating_div.find('p')).replace(',',';').replace('\r','').replace('\n','').replace('\t','').replace('&nbsp;',' ').strip()
    foo = foo.split('<br />')
    start_tel = 0
    for i in range(1,6):
        start_tel = i
        if foo[i].find('tel:') != -1:
            break
    
    address = ' / '.join(foo[1:start_tel])     

    # print rating_text

    match = regex_website.search(rating_text)
    if match is not None:
        print 'Website is: ' + match.group(1)
        output += match.group(1) + ','
    else:
        print 'Unable to locate website'
        return ''
    
    match = regex_email.search(rating_text)
    if match is not None:
        print 'Email address is: ' + match.group(1)
        output += match.group(1) + ','
    else:
        print 'Unable to locate email address'
        return output + '\n'
    
    match = regex_tel.search(rating_text)
    if match is not None:
        print 'Telephone number is: ' + match.group(1)
        output += match.group(1) + ','
    else:
        print 'Unable to locate telephone number'
        return output + '\n'

    output += address + ','

    output += get_charity_url(org_id) + ','
    
    output += str(get_revenue(soup)) + ','
    
    return output + '\n'
    
             
if __name__ == '__main__':
    
    print 'Charity scraper!'
    
    use_charity_list_file = True
    use_local = False
    # file_mode = 'w'
    file_mode = 'a'
        
    out_file = open(output_csv_filename, file_mode)   

    # Get list of charities from a file or just loop em all? 
    if use_charity_list_file:    
        charity_list = open(charity_list_filename, 'r')        

        lines = charity_list.readlines()
        # Continue from where it screwed up...
        # lines = lines[197:]
        #lines = lines[:197]
                                
        for line in lines:
            
            try:
                output = process_address( int(line), use_local )                      
                out_file.write(output)
                out_file.flush()
            
            except:
                # Sometimes a page will throw a UnicodeDecodeError. Oh well.
                pass
            
            # Sleep for a little while to avoid thrashing the website
            # Screw it! If my IP is blocked I can use my VPS to finish up.
            # time.sleep( random.choice(range(15,60) ))
        
        charity_list.close()
        
    else:
                
        org_ids = [9375]        
        
        for org_id in org_ids:
            output = process_address( org_id, use_local )
            out_file.write(output)
            out_file.flush()
                
    
    out_file.close()   
            
    print 'Done! Have a nice day!'
    