import os
import smtplib
import mimetypes
import re
import sys
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.MIMEAudio import MIMEAudio
from email.MIMEImage import MIMEImage
from email.Encoders import encode_base64

regex_email = re.compile(r'[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}')
# regex_email = re.compile(r'[a-zA-Z0-9._%+-]+@')

def sendMail(subject, recipient_email, html, dynamic_text, *attachmentFilePaths ):
    fromName = 'tom@superdonate.com'
    gmailUser = 'tom@thomasbak.com'
    gmailPassword = 'fq772m'    
    msg = MIMEMultipart()
    msg['From'] = fromName
    msg['To'] = recipient_email
    msg['Subject'] = subject
    
    # copy the message so that we don't trash it when replacing with dynamic text
    html = html[:]
    
    # Replace newlines with <br/> so that it looks right in html
    html = re.sub(r'\n', '<br/>', html )    
    
    # Replace dymamic stuff
    for key, value in dynamic_text.items():
        html = re.sub(key, value, html )
        
    # Strip out the html stuff for the plaintext message
    text = re.sub(r'<br[\w]?/?>', '\n', html )
    text = re.sub('<[^>]*>', '', text )        
    
    match = regex_email.search(recipient_email)
    if match is not None:
        print recipient_email + " is a valid email address!"        
    else:
        print recipient_email + " is NOT a valid email address!"
        return    
    
    #print "subject: " + subject
    #print "text: " + text
    print "html: " + html
    #print 'Sending to %s' % recipient_email
    # return
    
    # msg.attach(MIMEText(text))
    msgAlternative = MIMEMultipart('alternative')
    msg.attach(msgAlternative)
    msgText = MIMEText(text)
    msgAlternative.attach(msgText)
    msgText = MIMEText(html, 'html')
    msgAlternative.attach(msgText)    

    print( "Sending to %s..." % recipient_email )    
    for attachmentFilePath in attachmentFilePaths:
        msg.attach(getAttachment(attachmentFilePath))
    mailServer = smtplib.SMTP('smtp.gmail.com', 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(gmailUser, gmailPassword)
    mailServer.sendmail(gmailUser, recipient_email, msg.as_string())
    mailServer.close()
    print('Sent email to %s' % recipient_email)
    
def getAttachment(attachmentFilePath):
    contentType, encoding = mimetypes.guess_type(attachmentFilePath)
    if contentType is None or encoding is not None:
        contentType = 'application/octet-stream'
    mainType, subType = contentType.split('/', 1)
    file = open(attachmentFilePath, 'rb')
    fileText = file.read()
    if mainType == 'text':
        attachment = MIMEText(fileText)
    elif mainType == 'message':
        attachment = email.message_from_file(file)
    elif mainType == 'image':
        attachment = MIMEImage(fileText,_subType=subType)
    elif mainType == 'audio':
        attachment = MIMEAudio(fileText,_subType=subType)
    else:
        attachment = MIMEBase(mainType, subType)
    attachment.set_payload(fileText)
    encode_base64(attachment)
    file.close()
    attachment.add_header('Content-Disposition', 'attachment',   filename=os.path.basename(attachmentFilePath))
    return attachment


if __name__ == '__main__':
    range_from = int(sys.argv[1])
    range_to = int(sys.argv[2])
    charities = open('output.csv', 'r').readlines()
    charities = charities[range_from:range_to]
    
    message = open('email.txt', 'r').read()
    
    for charity in charities:
        l = charity.split(',')
        
        recipient_name = ''
        recipient_email = l[3]        
        
        # Check for specific name + email address, use that instead...
        # Use first name only
        if( l[8] is not '' and l[9] is not '' ):
            recipient_email = l[9].strip()
            recipient_name = ' ' + l[8].split(' ')[0]               
        
        # TB TODO - Send the individual emails here!
        dynamic_text = { '\*\*\*CHARITY\*\*\*': l[1], '\*\*\*NAME\*\*\*': recipient_name }
        subject = l[1] + " invitation to SuperDonate"
        # message = 'Hi***NAME***,<br/>Normal. <a href="foo.com">***CHARITY***</a> <br/><br/>Bye!'
        
        sendMail(subject, recipient_email, message, dynamic_text, 'C:/p/ezdonate/docs/superdonate charity information.pdf' )
        
    exit()
        