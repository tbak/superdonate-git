#!/bin/sh

cd /home/tbak/p/superdonate/app/admin_updatedatabase/dist

for a in 1 2 3 4 
do
	java -jar admin_updatedatabase.jar > log

	if [ "$?" = 100 ]
	then
		mail -s "Success! Linode SuperDonate" tom@thomasbak.com < log
		exit 0
	fi

	echo "FAILED... WAITING 5 MINUTES"
	sleep 300

done

mail -s "FAILURE: Linode SuperDonate" tom@thomasbak.com < log

